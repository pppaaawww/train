ALTER TABLE `train`.`sys_user_post`
    ADD COLUMN `post_level_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '岗位等级' AFTER `post_id`;

INSERT INTO `train`.`sys_dict_type` (`dict_id`, `dict_name`, `dict_type`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (100, '岗位等级', 'post_level', '0', 'admin', '2024-03-25 20:53:11', '', NULL, '岗位等级');

INSERT INTO `train`.`sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (100, 0, '岗前', '0', 'post_level', NULL, 'default', 'N', '0', 'admin', '2024-03-25 20:56:02', '', NULL, '岗前');
INSERT INTO `train`.`sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (101, 1, '一级', '1', 'post_level', NULL, 'default', 'N', '0', 'admin', '2024-03-25 20:56:25', '', NULL, '一级');
INSERT INTO `train`.`sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (102, 2, '二级', '2', 'post_level', NULL, 'default', 'N', '0', 'admin', '2024-03-25 20:56:38', '', NULL, '二级');
INSERT INTO `train`.`sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (103, 3, '三级', '3', 'post_level', NULL, 'default', 'N', '0', 'admin', '2024-03-25 20:56:57', '', NULL, '三级');


/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3306
 Source Schema         : train

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 25/03/2024 20:40:48
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for exam_plan
-- ----------------------------
DROP TABLE IF EXISTS `exam_plan`;
CREATE TABLE `exam_plan`  (
                              `id` bigint NOT NULL COMMENT '主键',
                              `name` char(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '考试名称',
                              `exam_test_paper_id` bigint NOT NULL COMMENT '考试试卷id',
                              `dept_id` bigint NULL DEFAULT NULL COMMENT '考试部门id',
                              `post_id` bigint NULL DEFAULT NULL COMMENT '考试岗位id',
                              `post_level_code` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '考试岗位级别',
                              `exam_type` int NOT NULL COMMENT '考试类型：0-固定 1-周期',
                              `frequency` int NOT NULL COMMENT '当为周期时的频次（次/天）',
                              `exam_start_time` datetime NOT NULL COMMENT '考试计划开始时间',
                              `exam_end_time` datetime NOT NULL COMMENT '考试计划结束时间',
                              `whether_can_resit` int NOT NULL COMMENT '是否可以补考：0-不可用 1-可以',
                              `Exam_duration` double NOT NULL COMMENT '考试时长（分钟）',
                              `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                              `is_del` int NOT NULL DEFAULT 0 COMMENT '是否删除：0-否 1-是',
                              `del_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '删除者',
                              `del_time` datetime NULL DEFAULT NULL COMMENT '删除',
                              `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '更新者',
                              `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                              `remark` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
                              PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '考试计划' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of exam_plan
-- ----------------------------

-- ----------------------------
-- Table structure for exam_plan_venue
-- ----------------------------
DROP TABLE IF EXISTS `exam_plan_venue`;
CREATE TABLE `exam_plan_venue`  (
                                    `id` bigint NOT NULL COMMENT '主键',
                                    `exam_plan_id` bigint NOT NULL COMMENT '考试计划id',
                                    `exam_start_time` datetime NOT NULL COMMENT '考试计划开始时间',
                                    `exam_end_time` datetime NOT NULL COMMENT '考试计划结束时间',
                                    `exam_place` char(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '考试地点',
                                    `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                    `is_del` int NOT NULL DEFAULT 0 COMMENT '是否删除：0-否 1-是',
                                    `del_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '删除者',
                                    `del_time` datetime NULL DEFAULT NULL COMMENT '删除',
                                    `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '更新者',
                                    `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                    `remark` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
                                    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '考试计划场次' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of exam_plan_venue
-- ----------------------------

-- ----------------------------
-- Table structure for exam_plan_venue_user
-- ----------------------------
DROP TABLE IF EXISTS `exam_plan_venue_user`;
CREATE TABLE `exam_plan_venue_user`  (
                                         `id` bigint NOT NULL COMMENT '主键',
                                         `exam_plan_id` bigint NOT NULL COMMENT '考试计划id',
                                         `exam_plan_venue_id` bigint NOT NULL COMMENT '考试场次id',
                                         `user_id` bigint NOT NULL COMMENT '考试人员id',
                                         `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                         `is_del` int NOT NULL DEFAULT 0 COMMENT '是否删除：0-否 1-是',
                                         `del_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '删除者',
                                         `del_time` datetime NULL DEFAULT NULL COMMENT '删除',
                                         `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '更新者',
                                         `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                         `remark` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
                                         PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '考试场次人员' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of exam_plan_venue_user
-- ----------------------------

-- ----------------------------
-- Table structure for exam_question_bank
-- ----------------------------
DROP TABLE IF EXISTS `exam_question_bank`;
CREATE TABLE `exam_question_bank`  (
                                       `id` bigint NOT NULL COMMENT '主键',
                                       `dept_id` bigint NOT NULL COMMENT '该考题适合部门id',
                                       `syllabus_code` char(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '大纲：来源于数据字典',
                                       `post_id` bigint NOT NULL COMMENT '该考题适合的岗位id',
                                       `subject_id` bigint NOT NULL COMMENT '该考题所属科目',
                                       `knowledge_points_id` bigint NOT NULL COMMENT '该考题所属知识点id',
                                       `question_type` int NOT NULL COMMENT '考题类型：1-判断题 2-填空题 3-问答题 4-论述题 5-选择题',
                                       `content` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '题干内容',
                                       `right_key` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '正确答案',
                                       `post_level_code` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '适合的能力级别，来源于数据字典',
                                       `analysis` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '试题解析',
                                       `create_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '创建者',
                                       `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                       `is_del` int NOT NULL DEFAULT 0 COMMENT '是否删除：0-否 1-是',
                                       `del_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '删除者',
                                       `del_time` datetime NULL DEFAULT NULL COMMENT '删除',
                                       `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '更新者',
                                       `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                       `remark` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
                                       PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '题库管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of exam_question_bank
-- ----------------------------

-- ----------------------------
-- Table structure for exam_question_bank_option
-- ----------------------------
DROP TABLE IF EXISTS `exam_question_bank_option`;
CREATE TABLE `exam_question_bank_option`  (
                                              `id` bigint NOT NULL COMMENT '主键',
                                              `question_id` bigint NOT NULL COMMENT '考题id',
                                              `sort_code` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '序号',
                                              `sort` int NOT NULL COMMENT '排序',
                                              `content` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '内容',
                                              `create_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '创建者',
                                              `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                              `is_del` int NOT NULL DEFAULT 0 COMMENT '是否删除：0-否 1-是',
                                              `del_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '删除者',
                                              `del_time` datetime NULL DEFAULT NULL COMMENT '删除',
                                              `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '更新者',
                                              `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                              `remark` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
                                              PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '考题选项 适用于填空、选择题' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of exam_question_bank_option
-- ----------------------------

-- ----------------------------
-- Table structure for exam_subject_knowledge_points
-- ----------------------------
DROP TABLE IF EXISTS `exam_subject_knowledge_points`;
CREATE TABLE `exam_subject_knowledge_points`  (
                                                  `id` bigint NOT NULL COMMENT '主键',
                                                  `name` char(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '科目/知识点名称',
                                                  `type` int NOT NULL COMMENT '0-科目；1-知识点',
                                                  `subject_id` bigint NULL DEFAULT NULL COMMENT '当type=1时存在，并为某个科目的id',
                                                  `post_id` bigint NULL DEFAULT NULL COMMENT '当type=0时存在，并为某个岗位的id',
                                                  `create_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '创建者',
                                                  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                                  `is_del` int NOT NULL DEFAULT 0 COMMENT '是否删除：0-否 1-是',
                                                  `del_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '删除者',
                                                  `del_time` datetime NULL DEFAULT NULL COMMENT '删除',
                                                  `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '更新者',
                                                  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                                  `remark` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
                                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '科目知识点管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of exam_subject_knowledge_points
-- ----------------------------

-- ----------------------------
-- Table structure for exam_test_paper
-- ----------------------------
DROP TABLE IF EXISTS `exam_test_paper`;
CREATE TABLE `exam_test_paper`  (
                                    `id` bigint NOT NULL COMMENT '主键',
                                    `name` char(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '试卷名称',
                                    `type` int NOT NULL COMMENT '试卷类型：0-随机试卷 1-标准试卷',
                                    `pass_grade` double NOT NULL COMMENT '及格分数',
                                    `total_grade` double NOT NULL COMMENT '试卷总分数',
                                    `dept_id` bigint NULL DEFAULT NULL COMMENT '该试卷适合部门id',
                                    `post_id` bigint NULL DEFAULT NULL COMMENT '该试卷适合的岗位id',
                                    `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                    `is_del` int NOT NULL DEFAULT 0 COMMENT '是否删除：0-否 1-是',
                                    `del_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '删除者',
                                    `del_time` datetime NULL DEFAULT NULL COMMENT '删除',
                                    `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '更新者',
                                    `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                    `remark` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
                                    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '试卷' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of exam_test_paper
-- ----------------------------

-- ----------------------------
-- Table structure for exam_test_paper_question_extract_rule
-- ----------------------------
DROP TABLE IF EXISTS `exam_test_paper_question_extract_rule`;
CREATE TABLE `exam_test_paper_question_extract_rule`  (
                                                          `id` bigint NOT NULL COMMENT '主键',
                                                          `exam_test_paper_id` bigint NOT NULL COMMENT '试卷id',
                                                          `question_type` int NOT NULL COMMENT '抽取试题的类型',
                                                          `grade` double NOT NULL COMMENT '抽取的试题每个的分数',
                                                          `number` int NOT NULL COMMENT '抽取数量',
                                                          `dept_id` bigint NULL DEFAULT NULL COMMENT '抽取试题适合的部门id',
                                                          `syllabus_code` char(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '抽取试题适合的大纲：来源于数据字典',
                                                          `post_id` bigint NULL DEFAULT NULL COMMENT '抽取试题适合的岗位id',
                                                          `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                                          `is_del` int NOT NULL DEFAULT 0 COMMENT '是否删除：0-否 1-是',
                                                          `del_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '删除者',
                                                          `del_time` datetime NULL DEFAULT NULL COMMENT '删除',
                                                          `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '更新者',
                                                          `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                                          `remark` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
                                                          PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '试卷试题抽取规则' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of exam_test_paper_question_extract_rule
-- ----------------------------

-- ----------------------------
-- Table structure for exam_test_paper_required_question
-- ----------------------------
DROP TABLE IF EXISTS `exam_test_paper_required_question`;
CREATE TABLE `exam_test_paper_required_question`  (
                                                      `id` bigint NOT NULL COMMENT '主键',
                                                      `exam_test_paper_id` bigint NOT NULL COMMENT '试卷id',
                                                      `question_id` bigint NOT NULL COMMENT '题目id',
                                                      `grade` double NOT NULL COMMENT '该考题分数',
                                                      `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                                      `is_del` int NOT NULL DEFAULT 0 COMMENT '是否删除：0-否 1-是',
                                                      `del_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '删除者',
                                                      `del_time` datetime NULL DEFAULT NULL COMMENT '删除',
                                                      `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '更新者',
                                                      `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                                      `remark` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
                                                      PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '试卷必选题目' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of exam_test_paper_required_question
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;


-- 2024-03-26 考培管理菜单
INSERT INTO `train`.`sys_menu`(`menu_id`, `menu_name`, `parent_id`, `order_num`, `path`, `component`, `query`, `is_frame`, `is_cache`, `menu_type`, `visible`, `status`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2000, '考培管理', 0, 0, 'train', NULL, NULL, 1, 0, 'M', '0', '0', '', '#', 'admin', '2024-03-26 10:52:10', 'admin', '2024-03-26 11:03:37', '');
