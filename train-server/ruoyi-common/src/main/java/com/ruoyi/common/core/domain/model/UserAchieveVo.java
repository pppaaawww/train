package com.ruoyi.common.core.domain.model;

/**
 * @author keke
 * @description  用户的勋章实体
 * @date 2024/5/17 14:37
 */

public class UserAchieveVo {

    /**
     * 用户id
     */
    private Long userId;
    /**
     * 文件路径
     */
    private String path;
    /**
     * 文件名称
     */
    private String fileName;

    /**
     * 成就名称
     */
    private String achieveName;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getAchieveName() {
        return achieveName;
    }

    public void setAchieveName(String achieveName) {
        this.achieveName = achieveName;
    }

    @Override
    public String toString() {
        return "UserAchieveVo{" +
                "userId=" + userId +
                ", path='" + path + '\'' +
                ", fileName='" + fileName + '\'' +
                ", achieveName='" + achieveName + '\'' +
                '}';
    }
}
