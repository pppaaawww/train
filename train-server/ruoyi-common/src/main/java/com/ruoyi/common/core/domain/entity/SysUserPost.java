package com.ruoyi.common.core.domain.entity;

/**
 * 用户和岗位关联 sys_user_post
 *
 * @author ruoyi
 */
public class SysUserPost {
    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 岗位ID
     */
    private Long postId;

    /**
     * 岗位等级
     */
    private String postLevelCode;

    /**
     * 岗位能力
     */
    private String postAbility;

    public String getPostLevelCode() {
        return postLevelCode;
    }

    public void setPostLevelCode(String postLevelCode) {
        this.postLevelCode = postLevelCode;
    }


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public String getPostAbility() {
        return postAbility;
    }

    public void setPostAbility(String postAbility) {
        this.postAbility = postAbility;
    }

    @Override
    public String toString() {
        return "SysUserPost{" +
                "userId=" + userId +
                ", postId=" + postId +
                ", postLevelCode='" + postLevelCode + '\'' +
                ", postAbility='" + postAbility + '\'' +
                '}';
    }
}
