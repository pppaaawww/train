package com.ruoyi.common.enums;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public enum DictDataTypeEnums {
    SYLLABUS("syllabus","大纲"),
    POST_ABILITY("post_ability","岗位能力"),
    POST_LEVEL("post_level","岗位等级");
    private String code;
    private String value;
    private DictDataTypeEnums(String code, String value){
        this.code = code;
        this.value = value;
    }

    public String getCode(){
        return code;
    };

    public String getValue(){
        return value;
    }

    private static Map<String,String> map = new HashMap<>();

    static {
        for (DictDataTypeEnums e : DictDataTypeEnums.values()) {
            map.put(e.getCode(),e.getValue());
        }
    }

    public static String getValueByCode(Integer code){
        return map.get(code);
    }

    public static Collection<String> getCodes(){
        return map.keySet();
    }
}
