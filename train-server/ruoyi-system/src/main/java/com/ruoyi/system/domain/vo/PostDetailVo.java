package com.ruoyi.system.domain.vo;

/**
 * 岗位详情vo
 * @author baiyang
 */
public class PostDetailVo {

    private Long postId;

    private String postName;

    private String postLevelCode;

    private String postLevelStr;

    private String postAbilityCode;

    private String postAbilityStr;

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getPostLevelCode() {
        return postLevelCode;
    }

    public void setPostLevelCode(String postLevelCode) {
        this.postLevelCode = postLevelCode;
    }

    public String getPostLevelStr() {
        return postLevelStr;
    }

    public void setPostLevelStr(String postLevelStr) {
        this.postLevelStr = postLevelStr;
    }

    public String getPostAbilityCode() {
        return postAbilityCode;
    }

    public void setPostAbilityCode(String postAbilityCode) {
        this.postAbilityCode = postAbilityCode;
    }

    public String getPostAbilityStr() {
        return postAbilityStr;
    }

    public void setPostAbilityStr(String postAbilityStr) {
        this.postAbilityStr = postAbilityStr;
    }
}
