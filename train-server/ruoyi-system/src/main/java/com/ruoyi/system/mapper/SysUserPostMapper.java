package com.ruoyi.system.mapper;

import com.ruoyi.common.core.domain.entity.SysUserPost;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户与岗位关联表 数据层
 *
 * @author ruoyi
 */
public interface SysUserPostMapper {
    /**
     * 通过用户ID删除用户和岗位关联
     *
     * @param userId 用户ID
     * @return 结果
     */
    public int deleteUserPostByUserId(Long userId);

    /**
     * 通过岗位ID查询岗位使用数量
     *
     * @param postId 岗位ID
     * @return 结果
     */
    public int countUserPostById(Long postId);

    /**
     * 批量删除用户和岗位关联
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserPost(Long[] ids);

    /**
     * 批量新增用户岗位信息
     *
     * @param userPostList 用户岗位列表
     * @return 结果
     */
    public int batchUserPost(List<SysUserPost> userPostList);

    /**
     * 根据用户id进行查询
     *
     * @param userId
     * @return
     */
    List<SysUserPost> selectByUserId(Long userId);

    /**
     * 根据条件获取userId
     *
     * @param postId
     * @param postLevelCode
     * @return
     */
    List<Long> selectByPostWithLevel(@Param(value = "postId") Long postId, @Param(value = "postLevelCode") String postLevelCode);

    /**
     * 更新
     *
     * @param sysUserPost
     */
    void update(SysUserPost sysUserPost);

    List<SysUserPost> selectAll();

    /**
     * 根据岗位id获取该岗位下的用户信息
     *
     * @param postId
     * @return
     */
    List<SysUserPost> selectByPostId(Long postId);
}
