package com.train.base.entity.bo;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * 专项训练题目获取规则
 */
@Getter
@Setter
public class SpecialTrainRule extends BaseEntity {
    /** 抽取试题适合的部门id */
    private Long deptId;

    /** 抽取试题适合的大纲：来源于数据字典 */
    private String syllabusCode;

    /** 抽取试题适合的岗位id */
    @NotNull(message = "岗位不能为空")
    private Long postId;

    /** 抽取试题时候的能力级别code，来源数据字典 */
    private String postLevelCode;

    /** 抽取试题的科目id */
    @NotNull(message = "科目不能为空")
    private Long subjectId;

    /** 抽取试题的知识点id */
    @NotNull(message = "知识点不能为空")
    private Long knowledgePointId;
}
