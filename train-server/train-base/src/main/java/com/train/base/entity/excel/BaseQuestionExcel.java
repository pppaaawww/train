package com.train.base.entity.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseQuestionExcel {
    private Long id;
    @ExcelProperty("试题编码")
    private String code;
    @ExcelProperty("单位")
    private String unit;
    @ExcelProperty("大纲")
    private String syllabus;
    @ExcelProperty("岗位")
    private String post;
    @ExcelProperty("科目")
    private String subject;
    @ExcelProperty("知识点")
    private String knowledgePoint;
    @ExcelProperty("题干")
    private String content;
    @ExcelProperty("正确答案")
    private String rightKey;
    @ExcelProperty("岗位级别")
    private String postLevel;
    @ExcelProperty("试题解析")
    private String analysis;
}
