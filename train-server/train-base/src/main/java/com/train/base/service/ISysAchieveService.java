package com.train.base.service;


import com.train.base.domain.SysAchieve;

import java.util.List;

/**
 * 等级管理Service接口
 *
 * @author keke
 * @date 2024-05-14
 */
public interface ISysAchieveService {
    /**
     * 查询等级管理
     *
     * @param id 等级管理主键
     * @return 等级管理
     */
    public SysAchieve selectSysAchieveById(Long id);

    /**
     * 查询等级管理列表
     *
     * @param sysAchieve 等级管理
     * @return 等级管理集合
     */
    public List<SysAchieve> selectSysAchieveList(SysAchieve sysAchieve);

    /**
     * 新增等级管理
     *
     * @param sysAchieve 等级管理
     * @return 结果
     */
    public int insertSysAchieve(SysAchieve sysAchieve);

    /**
     * 修改等级管理
     *
     * @param sysAchieve 等级管理
     * @return 结果
     */
    public int updateSysAchieve(SysAchieve sysAchieve);

    /**
     * 批量删除等级管理
     *
     * @param ids 需要删除的等级管理主键集合
     * @return 结果
     */
    public int deleteSysAchieveByIds(Long[] ids);

    /**
     * 根据成就的ids查询成就的信息
     * @param ids 成就的ids
     * @return
     */
    List<SysAchieve> selectSysAchieveByIds(List<Long> ids);
}
