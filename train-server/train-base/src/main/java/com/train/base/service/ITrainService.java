package com.train.base.service;

import com.train.base.domain.ExamQuestionBank;
import com.train.base.domain.TrainRecord;
import com.train.base.domain.TrainTestPaper;
import com.train.base.entity.bo.*;
import com.train.base.entity.chart.LineChartData;
import com.train.base.entity.chart.SunriseChartData;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public interface ITrainService {
    /**
     * 根据模板生成试卷
     *
     * @param moduleId
     * @return
     */
    TrainTestPaper createTrainPaper(Long moduleId);

    /**
     * 获取专项训练的题目
     *
     * @param rule
     * @return
     */
    List<Question> listSpecial(SpecialTrainRule rule);

    /**
     * 新增训练记录
     *
     * @param record
     * @return
     */
    int insertTrainRecord(TrainRecord record);

    /**
     * 提交模拟试卷
     *
     * @param answer
     * @return
     */
    int submitPaper(PaperAnswer answer);

    /**
     * 获取某个用户的专项训练记录
     *
     * @param userId
     * @param questionIds
     * @return
     */
    List<TrainRecord> selectSpecialRecord(Long userId, List<Long> questionIds);

    /**
     * 根据规则随机获取试题题目
     *
     * @param rule
     * @return
     */
    Question getSpecialQuestion(SpecialTrainRule rule);

    /**
     * 回答专项训练题目
     *
     * @param record
     * @return
     */
    int answerSpecialQuestion(TrainRecord record);

    /**
     * 获取当前登录用户的模拟试卷
     *
     * @return
     */
    List<TrainTestPaper> listUserTrainPaper(TrainTestPaper trainTestPaper);

    /**
     * 获取某个试卷的问题
     *
     * @param id
     * @return
     */
    List<Question> getPaperDetail(Long id);

    /**
     * 获取专项题目训练记录
     *
     * @param bank
     * @return
     */
    List<TrainRecord> listSpecialRecode(ExamQuestionBank bank);

    List<SpecialTrainProgress> specialTrainProgress(Long postId, String postLevelCode);

    /**
     * 获取某个人某月的专项训练记录
     *
     * @param month
     * @return
     */
    List<TrainRecord> selectSpecialRecordByDate(LocalDate month);

    /**
     * 获取某个人某月的模拟考试记录
     *
     * @param month
     * @return
     */
    List<TrainTestPaper> listUserTrainPaperByDate(LocalDate month);

    /**
     * 获取某个月份专项训练的数量
     *
     * @return
     */
    List<TrainNum> getTrainNumByMonth();

    List<SubjectAnalysis> getTrainSubjectAnalysis();

    /**
     * 获取一段时间训练题目数量及错题数量
     *
     * @param statisticalParam 查询条件
     * @return 符合条件的数据
     */
    SpecialTrainProgress getTrainNumAndErrorNum(StatisticalParam statisticalParam);

    /**
     * 获取人员训练数量
     *
     * @param param 查询参数
     * @return 人员训练数据
     */
    List<UserRankingParam> userRanking(UserRankingParam param);

    /**
     * 获取常用的模板数据
     *
     * @return
     */
    List<CommonTemplatesVo> commonTemplates();

    /**
     * 获取训练错误占比
     *
     * @return
     */
    Map<String,Integer> errorProportion();

    /**
     * 获取模拟训练数据
     *
     * @param trainTestPaper 查询条件
     * @return 训练数据
     */
    List<TrainTestPaper> paperList(TrainTestPaper trainTestPaper);

    /**
     * 获取分析数据
     *
     * @param userId
     * @param startDate
     * @param endDate
     * @return
     */
    Map<String, List<SunriseChartData>> getSunriseChartData(Long userId, LocalDate startDate, LocalDate endDate);

    /**
     * 获取一段时间的错题数量
     *
     * @param userId
     * @param startDate
     * @param endDate
     * @return
     */
    LineChartData<LocalDate, Integer> getTrainLineChart(Long userId, LocalDate startDate, LocalDate endDate);
}
