package com.train.base.enums;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public enum ExamTestPaperStateEnums {
    WAIT_RECEIVE(0,"待领取"),
    WAIT_SUBMIT(1,"待回填"),
    WAIT_GRADE(2,"待判卷"),
    FINISH(3,"已完成");
    private int code;
    private String value;
    private ExamTestPaperStateEnums(int code, String value){
        this.code = code;
        this.value = value;
    }

    public int getCode(){
        return code;
    };

    public String getValue(){
        return value;
    }

    private static Map<Integer,String> map = new HashMap<>();

    static {
        for (ExamTestPaperStateEnums e : ExamTestPaperStateEnums.values()) {
            map.put(e.getCode(),e.getValue());
        }
    }

    public static String getValueByCode(Integer code){
        return map.get(code);
    }

    public static Collection<Integer> getCodes(){
        return map.keySet();
    }
}
