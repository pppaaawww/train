package com.train.base.enums;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public enum PostImproveTypeEnums{
    LEVEL(0L,"岗位级别"),
    ABILITY(1L,"岗位能力");

    private Long code;
    private String value;
    private PostImproveTypeEnums(Long code, String value){
        this.code = code;
        this.value = value;
    }

    public Long getCode(){
        return code;
    };

    public String getValue(){
        return value;
    }

    private static Map<Long,String> map = new HashMap<>();

    static {
        for (PostImproveTypeEnums e : PostImproveTypeEnums.values()) {
            map.put(e.getCode(),e.getValue());
        }
    }

    public static String getValueByCode(Integer code){
        return map.get(code);
    }

    public static Collection<Long> getCodes(){
        return map.keySet();
    }
}
