package com.train.base.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 模拟试卷对象 train_test_paper
 *
 * @author keke
 * @date 2024-03-29
 */
@Getter
@Setter
public class TrainTestPaper extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 试卷名称
     */
    @Excel(name = "试卷名称")
    private String name;

    /**
     * 试卷模板id
     */
    @Excel(name = "试卷模板id")
    private Long testPaperModuleId;

    private String testPaperModuleName;

    /**
     * 训练人员id
     */
    @Excel(name = "训练人员id")
    private Long userId;

    private String userName;

    /**
     * 模拟试卷题目总数量
     */
    @Excel(name = "模拟试卷题目总数量")
    private Long questionNum;

    /**
     * 正确回答数量
     */
    @Excel(name = "正确回答数量")
    private Long rightAnswerNum;

    /**
     * 未做时间
     */
    @Excel(name = "未做时间")
    private Long notDoneNum;

    /**
     * 模糊题目数量（论述题数量）
     */
    @Excel(name = "模糊题目数量", readConverterExp = "论=述题数量")
    private Long vagueNum;

    /**
     * 试卷提交时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "试卷提交时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date submitTime;

    /**
     * 训练时用户部门id
     */
    @Excel(name = "训练时用户部门id")
    private Long deptId;
    private String deptName;

    /**
     * 训练时用户岗位id
     */
    @Excel(name = "训练时用户岗位id")
    private Long postId;
    private String postName;

    /**
     * 训练时用户岗位能力
     */
    @Excel(name = "训练时用户岗位能力")
    private String postLevelCode;
    /**
     * 试卷状态：0-未完成 1-已完成
     */
    private Integer state;

    /**
     * 是否删除：0-否 1-是
     */
    @Excel(name = "是否删除：0-否 1-是")
    private Long isDel;

    /**
     * 删除者
     */
    @Excel(name = "删除者")
    private String delBy;

    /**
     * 删除
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除", width = 30, dateFormat = "yyyy-MM-dd")
    private Date delTime;

}
