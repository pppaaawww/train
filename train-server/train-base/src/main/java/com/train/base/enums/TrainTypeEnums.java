package com.train.base.enums;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * 训练类型
 */
public enum TrainTypeEnums {
    SPECIAL(0L, "专项训练"),
    SIMULATION(1L, "模拟训练");
    private Long code;
    private String value;

    private TrainTypeEnums(Long code, String value) {
        this.code = code;
        this.value = value;
    }

    public Long getCode() {
        return code;
    }

    ;

    public String getValue() {
        return value;
    }

    private static Map<Long, String> map = new HashMap<>();

    static {
        for (TrainTypeEnums e : TrainTypeEnums.values()) {
            map.put(e.getCode(), e.getValue());
        }
    }

    public static String getValueByCode(Integer code) {
        return map.get(code);
    }

    public static Collection<Long> getCodes() {
        return map.keySet();
    }
}

