package com.train.base.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.train.base.domain.PostImproveApplication;
import com.train.base.service.IPostImproveApplicationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 岗位提升申请Controller
 * 
 * @author keke
 * @date 2024-04-09
 */
@RestController
@RequestMapping("/train/PostImproveApplication")
public class PostImproveApplicationController extends BaseController
{
    @Autowired
    private IPostImproveApplicationService postImproveApplicationService;

    /**
     * 查询岗位提升申请列表
     */
    @GetMapping("/list")
    public TableDataInfo list(PostImproveApplication postImproveApplication)
    {
        startPage();
        List<PostImproveApplication> list = postImproveApplicationService.selectPostImproveApplicationList(postImproveApplication);
        return getDataTable(list);
    }

    @GetMapping("/listCurrentUser")
    public TableDataInfo listCurrentUser()
    {
        startPage();
        PostImproveApplication postImproveApplication = new PostImproveApplication();
        postImproveApplication.setApplicant(SecurityUtils.getUserId());
        List<PostImproveApplication> list = postImproveApplicationService.selectPostImproveApplicationList(postImproveApplication);
        return getDataTable(list);
    }

    /**
     * 导出岗位提升申请列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, PostImproveApplication postImproveApplication)
    {
        List<PostImproveApplication> list = postImproveApplicationService.selectPostImproveApplicationList(postImproveApplication);
        ExcelUtil<PostImproveApplication> util = new ExcelUtil<PostImproveApplication>(PostImproveApplication.class);
        util.exportExcel(response, list, "岗位提升申请数据");
    }

    /**
     * 获取岗位提升申请详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(postImproveApplicationService.selectPostImproveApplicationById(id));
    }

    /**
     * 新增岗位提升申请
     */
    @Log(title = "岗位提升申请", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PostImproveApplication postImproveApplication)
    {
        return toAjax(postImproveApplicationService.insertPostImproveApplication(postImproveApplication));
    }

    /**
     * 修改岗位提升申请
     */
    @PutMapping
    public AjaxResult edit(@RequestBody PostImproveApplication postImproveApplication)
    {
        return toAjax(postImproveApplicationService.updatePostImproveApplication(postImproveApplication));
    }

    /**
     * 删除岗位提升申请
     */
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(postImproveApplicationService.deletePostImproveApplicationByIds(ids));
    }

}
