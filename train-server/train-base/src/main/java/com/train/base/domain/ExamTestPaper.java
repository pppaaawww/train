package com.train.base.domain;

import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 试卷管理对象 exam_test_paper
 * 
 * @author keke
 * @date 2024-04-01
 */
@Setter
@Getter
public class ExamTestPaper extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;
    /**
     * 试卷状态：0-待领取 1-待提交 2-待判卷 3-已完成
     */
    private Integer state;
    private String stateStr;
    /** 考试计划id */
    @Excel(name = "考试计划id")
    private Long exemPlanId;
    /**
     * 计划名称
     */
    private String examPlanName;
    /** 试卷名称 */
    @Excel(name = "试卷名称")
    private String name;

    /** 试卷模板id */
    @Excel(name = "试卷模板id")
    private Long testPaperModuleId;
    /**
     * 试卷模板名称
     */
    private String testPaperModuleName;

    /** 人员id */
    @Excel(name = "人员id")
    private Long userId;

    private String userName;
    /** 试卷题目总数量 */
    @Excel(name = "试卷题目总数量")
    private Long questionNum;

    /** 正确回答数量 */
    @Excel(name = "正确回答数量")
    private Long rightAnswerNum;

    /** 未做数量 */
    @Excel(name = "未做数量")
    private Long notDoneNum;

    /** 模糊题目数量（论述题数量） */
    @Excel(name = "模糊题目数量", readConverterExp = "论=述题数量")
    private Long vagueNum;

    /** 试卷提交时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "试卷提交时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date submitTime;

    /** 考试时用户部门id */
    @Excel(name = "考试时用户部门id")
    private Long deptId;

    private String deptName;

    /** 考试时用户岗位id */
    @Excel(name = "考试时用户岗位id")
    private Long postId;

    private String postName;

    /** 考试时用户岗位能力 */
    @Excel(name = "考试时用户岗位能力")
    private String postLevelCode;

    /** 试卷总分数 */
    @Excel(name = "试卷总分数")
    private Double grade;

    /** 用户得分 */
    @Excel(name = "用户得分")
    private Double gainGrade;

    /** 是否删除：0-否 1-是 */
    @Excel(name = "是否删除：0-否 1-是")
    private Long isDel;

    /** 删除者 */
    @Excel(name = "删除者")
    private String delBy;

    /** 删除 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除", width = 30, dateFormat = "yyyy-MM-dd")
    private Date delTime;
    /**
     * 排名
     */
    private int ranking;

    /** 考试记录信息 */
    private List<ExamRecord> examRecordList;
}
