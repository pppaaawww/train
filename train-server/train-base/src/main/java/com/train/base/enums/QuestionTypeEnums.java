package com.train.base.enums;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * 题型枚举类
 */
public enum QuestionTypeEnums {

    JudgeQuestion(1L,"判断题"),
    FillBlankQuestionExcel(2L,"填空题"),
    EssayQuestion(4L,"论述题"),
    ChoiceQuestion(5L,"选择题");
    private Long code;
    private String value;
    private QuestionTypeEnums(Long code, String value){
        this.code = code;
        this.value = value;
    }

    public Long getCode(){
        return code;
    };

    public String getValue(){
        return value;
    }

    private static Map<Long,String> map = new HashMap<>();

    static {
        for (QuestionTypeEnums e : QuestionTypeEnums.values()) {
            map.put(e.getCode(),e.getValue());
        }
    }

    public static String getValueByCode(Integer code){
        return map.get(code);
    }

    public static Collection<Long> getCodes(){
        return map.keySet();
    }


}
