package com.train.base.entity.bo;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 试卷评卷记录
 */
@Getter
@Setter
public class PaperGradingRecord {
    /**
     * 试卷id
     */
    @NotNull
    private Long paperId;


    private List<PaperGradingRecord.GradingPaper> gradingPapers;

    @Getter
    @Setter
    public static class GradingPaper{
        /**
         * 试卷记录id
         */
        private Long recordId;
        /**
         * 答题内容
         */
        private String remark;
        /**
         * 正确与否
         */
        private Long result;
        /**
         * 获取分数
         */
        private Double gainGrade;
    }
}
