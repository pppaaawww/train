package com.train.base.controller;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.train.base.domain.FileInfo;
import com.train.base.service.IFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/file")
public class FileController extends BaseController {
    @Autowired
    private IFileService iFileService;

    @PostMapping(value = "upload")
    public AjaxResult upload(MultipartFile file, Long relId) {
        FileInfo fileInfo = iFileService.upload(file, relId);
        return AjaxResult.success(fileInfo);
    }



    /**
     * 查询文件列表
     */
    @GetMapping("/list")
    public TableDataInfo list(FileInfo fileInfo) {
        startPage();
        List<FileInfo> list = iFileService.selectFileInfo(fileInfo);
        return getDataTable(list);
    }

    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(iFileService.deleteFileByIds(ids));
    }

}
