package com.train.base.mapper;

import com.train.base.domain.ExamGradingPaperRecord;
import com.train.base.domain.ExamRecord;
import com.train.base.domain.ExamTestPaper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;
import java.util.List;

/**
 * 试卷管理Mapper接口
 *
 * @author keke
 * @date 2024-04-01
 */
public interface ExamTestPaperMapper {
    /**
     * 查询试卷管理
     *
     * @return 试卷管理
     */
    public ExamTestPaper selectExamTestPaperById(Long id);

    /**
     * 查询试卷管理列表
     *
     * @param examTestPaper 试卷管理
     * @return 试卷管理集合
     */
    public List<ExamTestPaper> selectExamTestPaperList(ExamTestPaper examTestPaper);

    /**
     * 新增试卷管理
     *
     * @param examTestPaper 试卷管理
     * @return 结果
     */
    public int insertExamTestPaper(ExamTestPaper examTestPaper);

    /**
     * 修改试卷管理
     *
     * @param examTestPaper 试卷管理
     * @return 结果
     */
    public int updateExamTestPaper(ExamTestPaper examTestPaper);

    /**
     * 删除试卷管理
     *
     * @param id 试卷管理主键
     * @return 结果
     */
    public int deleteExamTestPaperById(Long id);

    /**
     * 批量删除试卷管理
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExamTestPaperByIds(Long[] ids);

    /**
     * 批量删除考试记录
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExamRecordByTestPaperIds(Long[] ids);

    /**
     * 批量新增考试记录
     *
     * @param examRecordList 考试记录列表
     * @return 结果
     */
    public int batchExamRecord(List<ExamRecord> examRecordList);


    /**
     * 通过试卷管理主键删除考试记录信息
     *
     * @param id 试卷管理ID
     * @return 结果
     */
    public int deleteExamRecordByTestPaperId(Long id);

    /**
     * 批量新增试卷
     *
     * @param testPapers
     */
    void batchInsertExamTestPaper(List<ExamTestPaper> testPapers);

    /**
     * 修改试卷状态
     *
     * @param id
     * @param state
     * @return
     */
    int updateState(@Param(value = "id") Long id, @Param(value = "state") int state);

    /**
     * 更新考试记录
     *
     * @param record
     */
    int updateExamRecord(ExamRecord record);

    /**
     * 批量掺入
     *
     * @param records
     */
    int batchInsertGradingRecord(List<ExamGradingPaperRecord> records);


    List<ExamGradingPaperRecord> selectExamGradingPaperRecordList(ExamGradingPaperRecord examGradingPaperRecord);

    ExamGradingPaperRecord selectExamGradingPaperRecordById(Long id);

    List<ExamGradingPaperRecord> selectExamGradingPaperRecordByRecordIds(List<Long> recordIds);

    /**
     * 获取某个月某个人的考试信息
     *
     * @param userId
     * @param month
     * @return
     */
    List<ExamTestPaper> listByDate(@Param(value = "userId") Long userId, @Param(value = "month") LocalDate month);

    /**
     * 根据计划id获取
     *
     * @param planIds
     * @return
     */
    List<ExamTestPaper> selectExamTestPaperByPlanIds(List<Long> planIds);

    /**
     * 获取 考试信息
     *
     * @return 考试记录
     */
    List<ExamTestPaper> selectExamTestForRank(@Param("examPlanId") Long examPlanId);

    /**
     * 根据条件查询记录的数量
     *
     * @param examRecord 查询条件
     * @return 获取的数量
     */
    Integer count(ExamRecord examRecord);

    /**
     * 根据考试计划id 查询分数最高的试卷
     * @param planId 考试计划的id
     * @return  分数最高的试卷
     */
    ExamTestPaper selectExamPaperByPlanId(Long planId);

    /**
     * 根据用户的id查询该用户最近的一次考试
     * @param userId  用户的id
     * @param postId  岗位id
     * @return 用户最近的一次考试
     */
    ExamTestPaper selectExamTestPaperByUserId(@Param("userId") Long userId, @Param("postId") Long postId);
}
