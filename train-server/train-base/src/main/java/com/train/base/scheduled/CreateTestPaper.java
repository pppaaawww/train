package com.train.base.scheduled;

import com.train.base.domain.ExamPlan;
import com.train.base.service.IExamPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 床架循环试卷的定时任务
 */
//@Component
public class CreateTestPaper {
    @Autowired
    private IExamPlanService iExamPlanService;
    /**
     * 针对周期型考试计划进行定时任务自动生成试卷
     */
//    @Scheduled(cron = "0 0 1 * * ?")
    public void create(){
        // 获取需要循环的启用状态下的计划
        List<ExamPlan> needCreatePlan = iExamPlanService.selectNeedCreatePlan();
        List<Long> planIds = new ArrayList<>();
        needCreatePlan.forEach(e->{
            for (Long i = 0L; i < e.getFrequency(); i++) {
                planIds.add(e.getId());
            }
        });
        iExamPlanService.release(planIds);
    }
}
