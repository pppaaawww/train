package com.train.base.service;

import java.util.List;
import com.train.base.domain.PostImproveApplication;

/**
 * 岗位提升申请Service接口
 * 
 * @author keke
 * @date 2024-04-09
 */
public interface IPostImproveApplicationService 
{
    /**
     * 查询岗位提升申请
     * 
     * @param id 岗位提升申请主键
     * @return 岗位提升申请
     */
    public PostImproveApplication selectPostImproveApplicationById(Long id);

    /**
     * 查询岗位提升申请列表
     * 
     * @param postImproveApplication 岗位提升申请
     * @return 岗位提升申请集合
     */
    public List<PostImproveApplication> selectPostImproveApplicationList(PostImproveApplication postImproveApplication);

    /**
     * 新增岗位提升申请
     * 
     * @param postImproveApplication 岗位提升申请
     * @return 结果
     */
    public int insertPostImproveApplication(PostImproveApplication postImproveApplication);

    /**
     * 修改岗位提升申请
     * 
     * @param postImproveApplication 岗位提升申请
     * @return 结果
     */
    public int updatePostImproveApplication(PostImproveApplication postImproveApplication);

    /**
     * 批量删除岗位提升申请
     * 
     * @param ids 需要删除的岗位提升申请主键集合
     * @return 结果
     */
    public int deletePostImproveApplicationByIds(Long[] ids);

    /**
     * 删除岗位提升申请信息
     * 
     * @param id 岗位提升申请主键
     * @return 结果
     */
    public int deletePostImproveApplicationById(Long id);
}
