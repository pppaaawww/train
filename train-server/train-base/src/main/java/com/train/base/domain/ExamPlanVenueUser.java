package com.train.base.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 考试场次人员对象 exam_plan_venue_user
 * 
 * @author keke
 * @date 2024-03-29
 */
public class ExamPlanVenueUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 考试计划id */
    @Excel(name = "考试计划id")
    private Long examPlanId;

    /** 考试场次id */
    @Excel(name = "考试场次id")
    private Long examPlanVenueId;

    /** 考试人员id */
    @Excel(name = "考试人员id")
    private Long userId;

    /** 是否删除：0-否 1-是 */
    @Excel(name = "是否删除：0-否 1-是")
    private Long isDel;

    /** 删除者 */
    @Excel(name = "删除者")
    private String delBy;

    /** 删除 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除", width = 30, dateFormat = "yyyy-MM-dd")
    private Date delTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setExamPlanId(Long examPlanId) 
    {
        this.examPlanId = examPlanId;
    }

    public Long getExamPlanId() 
    {
        return examPlanId;
    }
    public void setExamPlanVenueId(Long examPlanVenueId) 
    {
        this.examPlanVenueId = examPlanVenueId;
    }

    public Long getExamPlanVenueId() 
    {
        return examPlanVenueId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setIsDel(Long isDel) 
    {
        this.isDel = isDel;
    }

    public Long getIsDel() 
    {
        return isDel;
    }
    public void setDelBy(String delBy) 
    {
        this.delBy = delBy;
    }

    public String getDelBy() 
    {
        return delBy;
    }
    public void setDelTime(Date delTime) 
    {
        this.delTime = delTime;
    }

    public Date getDelTime() 
    {
        return delTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("examPlanId", getExamPlanId())
            .append("examPlanVenueId", getExamPlanVenueId())
            .append("userId", getUserId())
            .append("createTime", getCreateTime())
            .append("isDel", getIsDel())
            .append("delBy", getDelBy())
            .append("delTime", getDelTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
