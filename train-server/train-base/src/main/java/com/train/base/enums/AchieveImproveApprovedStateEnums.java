package com.train.base.enums;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * 成就申请状态枚举
 */
public enum AchieveImproveApprovedStateEnums {
    WAIT(0L,"待审核"),
    PASS(1L,"通过"),
    REJECT(2L,"驳回");

    private Long code;
    private String value;
    private AchieveImproveApprovedStateEnums(Long code, String value){
        this.code = code;
        this.value = value;
    }

    public Long getCode(){
        return code;
    };

    public String getValue(){
        return value;
    }

    private static Map<Long,String> map = new HashMap<>();

    static {
        for (AchieveImproveApprovedStateEnums e : AchieveImproveApprovedStateEnums.values()) {
            map.put(e.getCode(),e.getValue());
        }
    }

    public static String getValueByCode(Integer code){
        return map.get(code);
    }

    public static Collection<Long> getCodes(){
        return map.keySet();
    }

}
