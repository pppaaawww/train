package com.train.base.entity.excel;

import com.train.base.enums.QuestionTypeEnums;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 填空题
 */
@Getter
@Setter
public class FillBlankQuestionExcel extends BaseQuestionExcel{
    /**
     * 填空答案
     */
    private List<String> answer;
}
