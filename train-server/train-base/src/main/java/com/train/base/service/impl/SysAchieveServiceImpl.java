package com.train.base.service.impl;

import com.ruoyi.common.exception.base.BaseException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SnowflakeIdWorker;
import com.train.base.domain.FileInfo;
import com.train.base.domain.SysAchieve;
import com.train.base.mapper.SysAchieveMapper;
import com.train.base.service.IFileService;
import com.train.base.service.ISysAchieveService;
import com.train.base.service.ISysUserAchieveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.io.File;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 等级管理Service业务层处理
 *
 * @author keke
 * @date 2024-05-14
 */
@Service
public class SysAchieveServiceImpl implements ISysAchieveService {
    @Autowired
    private SysAchieveMapper sysAchieveMapper;
    @Autowired
    private ISysUserAchieveService iSysUserAchieveService;
    @Autowired
    private IFileService iFileService;

    /**
     * 查询等级管理
     *
     * @param id 等级管理主键
     * @return 等级管理
     */
    @Override
    public SysAchieve selectSysAchieveById(Long id) {
        SysAchieve sysAchieve = sysAchieveMapper.selectSysAchieveById(id);
        sysAchieve.setFileInfo(iFileService.selectFileByRelId(id));
        return sysAchieve;
    }

    /**
     * 查询等级管理列表
     *
     * @param sysAchieve 等级管理
     * @return 等级管理
     */
    @Override
    public List<SysAchieve> selectSysAchieveList(SysAchieve sysAchieve) {
        return sysAchieveMapper.selectSysAchieveList(sysAchieve);
    }

    /**
     * 新增等级管理
     *
     * @param sysAchieve 等级管理
     * @return 结果
     */
    @Override
    public int insertSysAchieve(SysAchieve sysAchieve) {
        sysAchieve.setCreateTime(LocalDateTime.now());
        Long id = SnowflakeIdWorker.build().nextId();
        sysAchieve.setId(id);
        // 插入文件和成就的关联关系
        if(!ObjectUtils.isEmpty(sysAchieve.getFileId())) {
            FileInfo fileInfo = new FileInfo();
            fileInfo.setId(sysAchieve.getFileId());
            fileInfo.setRelId(id);
            fileInfo.setRelType("成就管理");
            iFileService.updateRelIdByFileId(fileInfo);
        }
        return sysAchieveMapper.insertSysAchieve(sysAchieve);
    }

    /**
     * 修改等级管理
     *
     * @param sysAchieve 等级管理
     * @return 结果
     */
    @Override
    public int updateSysAchieve(SysAchieve sysAchieve) {
        sysAchieve.setUpdateTime(DateUtils.getNowDate());
        FileInfo info = new FileInfo();
        info.setRelId(sysAchieve.getId());
        info.setId(sysAchieve.getFileId());
        if(!ObjectUtils.isEmpty(sysAchieve.getFileId()) && ObjectUtils.isEmpty(iFileService.selectFileInfo(info))) {
            // 插入文件和成就的关联关系
            FileInfo fileInfo = new FileInfo();
            fileInfo.setId(sysAchieve.getFileId());
            fileInfo.setRelId(sysAchieve.getId());
            fileInfo.setRelType("成就管理");
            iFileService.updateRelIdByFileId(fileInfo);
        }
        return sysAchieveMapper.updateSysAchieve(sysAchieve);
    }

    /**
     * 批量删除等级管理
     *
     * @param ids 需要删除的等级管理主键
     * @return 结果
     */
    @Override
    public int deleteSysAchieveByIds(Long[] ids) {
        // 判断删除的成就是否有关联的用户
        if(iSysUserAchieveService.countByAchieveIds(ids) > 0) {
            throw new BaseException("成就下有关联的用户，无法删除成就!");
        }
        return sysAchieveMapper.deleteSysAchieveByIds(ids);
    }

    @Override
    public List<SysAchieve> selectSysAchieveByIds(List<Long> ids) {
        return sysAchieveMapper.selectSysAchieveByIds(ids);
    }
}
