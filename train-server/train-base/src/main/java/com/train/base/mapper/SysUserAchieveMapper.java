package com.train.base.mapper;


import com.ruoyi.common.core.domain.model.UserAchieveVo;
import com.train.base.domain.SysUserAchieve;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 成就管理Mapper接口
 *
 * @author keke
 * @date 2024-05-14
 */
public interface SysUserAchieveMapper {
    /**
     * 查询成就管理
     *
     * @param id 成就管理主键
     * @return 成就管理
     */
    public SysUserAchieve selectSysUserAchieveById(Long id);

    /**
     * 查询成就管理列表
     *
     * @param sysUserAchieve 成就管理
     * @return 成就管理集合
     */
    public List<SysUserAchieve> selectSysUserAchieveList(SysUserAchieve sysUserAchieve);

    /**
     * 新增成就管理
     *
     * @param sysUserAchieve 成就管理
     * @return 结果
     */
    public int insertSysUserAchieve(SysUserAchieve sysUserAchieve);

    /**
     * 修改成就管理
     *
     * @param sysUserAchieve 成就管理
     * @return 结果
     */
    public int updateSysUserAchieve(SysUserAchieve sysUserAchieve);

    /**
     * 删除成就管理
     *
     * @param id 成就管理主键
     * @return 结果
     */
    public int deleteSysUserAchieveById(Long id);

    /**
     * 批量删除成就管理
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysUserAchieveByIds(Long[] ids);

    /**
     * 根据成就的ids查询是否有关联的用户
     * @param achieveIds 成就的ids
     * @return
     */
    int countByAchieveIds(@Param("achieveIds") Long[] achieveIds);

    /**
     * 根据用户的id查询用户的勋章列表
     * @param userId 用户的id
     * @return  勋章信息列表
     */
    List<SysUserAchieve> selectSysUserAchieveByUserId(Long userId);

    /**
     * 根据用户的ids查询用户成就
     * @param userIds 用户的ids
     * @return
     */
    List<SysUserAchieve> selectSysUserAchieveByUserIds(@Param("userIds") List<Long> userIds);
}
