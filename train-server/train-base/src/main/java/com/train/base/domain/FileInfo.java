package com.train.base.domain;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * 文件信息表
 *
 * @author baiyang
 */
@Setter
@Getter
@SuppressWarnings("serial")
public class FileInfo {
    /**
     * 文件名称
     */
    private String fileName;
    /**
     * 文件大小
     */
    private Long fileSize;
    /**
     * 文件类型
     */
    private String fileType;
    /**
     * 文件路径
     */
    private String path;
    /**
     * minio中的桶
     */
    private String bucket;
    /**
     * 关联的业务id
     */
    private Long relId;
    /**
     * 业务类型
     */
    private String relType;
    /**
     * 展示顺序
     */
    private Integer showIndex;
    /**
     * 备注
     */
    private String remark;
    /**
     * 主键
     */
    private Long id;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 修改人
     */
    private String updateBy;
    /**
     * 修改时间
     */
    private LocalDateTime updateTime;
    /**
     * 删除人
     */
    private String delBy;
    /**
     * 删除时间
     */
    private LocalDateTime delTime;
    /**
     * 删除标记(0:未删除，1:已删除)
     */
    private Long isDel;


// ---------------------------- 新增 ---------------------------- //
}
