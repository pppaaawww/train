package com.train.base.enums;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public enum TrainTestPaperStateEnums {
    WAIT_SUBMIT(0,"待回填"),
    FINISH(1,"已完成");
    private int code;
    private String value;
    private TrainTestPaperStateEnums(int code, String value){
        this.code = code;
        this.value = value;
    }

    public int getCode(){
        return code;
    };

    public String getValue(){
        return value;
    }

    private static Map<Integer,String> map = new HashMap<>();

    static {
        for (TrainTestPaperStateEnums e : TrainTestPaperStateEnums.values()) {
            map.put(e.getCode(),e.getValue());
        }
    }

    public static String getValueByCode(Integer code){
        return map.get(code);
    }

    public static Collection<Integer> getCodes(){
        return map.keySet();
    }
}
