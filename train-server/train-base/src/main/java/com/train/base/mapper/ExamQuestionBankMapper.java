package com.train.base.mapper;

import com.train.base.domain.ExamQuestionBank;
import com.train.base.domain.ExamQuestionBankOption;
import com.train.base.domain.ExamTestPaperExtractRule;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 题库管理Mapper接口
 *
 * @author ruoyi
 * @date 2024-03-26
 */
public interface ExamQuestionBankMapper {
    /**
     * 查询题库管理
     *
     * @param id 题库管理主键
     * @return 题库管理
     */
    public ExamQuestionBank selectExamQuestionBankById(Long id);

    /**
     * 查询题库管理列表
     *
     * @param examQuestionBank 题库管理
     * @return 题库管理集合
     */
    public List<ExamQuestionBank> selectExamQuestionBankList(ExamQuestionBank examQuestionBank);

    /**
     * 新增题库管理
     *
     * @param examQuestionBank 题库管理
     * @return 结果
     */
    public int insertExamQuestionBank(ExamQuestionBank examQuestionBank);


    /**
     * 新增题库管理
     *
     * @param examQuestionBanks 题库管理
     * @return 结果
     */
    public int batchInsertExamQuestionBank(List<ExamQuestionBank> examQuestionBanks);

    /**
     * 修改题库管理
     *
     * @param examQuestionBank 题库管理
     * @return 结果
     */
    public int updateExamQuestionBank(ExamQuestionBank examQuestionBank);

    /**
     * 删除题库管理
     *
     * @param id 题库管理主键
     * @return 结果
     */
    public int deleteExamQuestionBankById(Long id);

    /**
     * 批量删除题库管理
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExamQuestionBankByIds(Long[] ids);

    /**
     * 批量删除考题选项 适用于填空、选择题
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExamQuestionBankOptionByQuestionIds(Long[] ids);

    /**
     * 批量新增考题选项 适用于填空、选择题
     *
     * @param examQuestionBankOptionList 考题选项 适用于填空、选择题列表
     * @return 结果
     */
    public int batchExamQuestionBankOption(List<ExamQuestionBankOption> examQuestionBankOptionList);


    /**
     * 通过题库管理主键删除考题选项 适用于填空、选择题信息
     *
     * @param id 题库管理ID
     * @return 结果
     */
    public int deleteExamQuestionBankOptionByQuestionId(Long id);

    /**
     * 根据抽取规则随机抽取题库
     *
     * @param rule
     * @return
     */
    List<ExamQuestionBank> selectRandByRule(ExamTestPaperExtractRule rule);

    /**
     * 根据id集合获取
     *
     * @param bankIds
     * @return
     */
    List<ExamQuestionBank> selectExamQuestionBankByIds(List<Long> bankIds);

    /**
     * 分页查询
     *
     * @param examQuestionBank
     * @return
     */
    List<ExamQuestionBank> query(ExamQuestionBank examQuestionBank);

    List<ExamQuestionBank> listByPost(@Param(value = "postId") Long postId, @Param(value = "postLevelCode") String postLevelCode);

    List<ExamQuestionBankOption> listOptionsByBankId(Long id);
}
