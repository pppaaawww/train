package com.train.base.utils;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.SnowflakeIdWorker;
import com.train.base.domain.FileInfo;
import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Component
public class MinioUtils {

    @Autowired
    private MinioClient minioClient = null;
    @Value("${baiyang.minio.url}")
    private String URL = null;
    @Value("${baiyang.minio.bucket-name}")
    private String BUCKET_NAME = null;

    /**
     * 创建桶，有则不创建无则创建
     * @param bucketName
     * @throws Exception
     */
    public void createBucket(String bucketName) throws Exception{
        boolean found =
                minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
        if (!found) {
            minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
        }
    }

    /**
     * 上传一个文件
     * @param file
     * @return
     * @throws Exception
     */
    public FileInfo uploadFile(MultipartFile file) throws Exception{
        FileInfo fileInfo = upload(file,BUCKET_NAME);
        return fileInfo;
    }

    /**
     * 上传一个文件
     * @param file
     * @param bucketName
     * @return
     * @throws Exception
     */
    public FileInfo uploadFile(MultipartFile file, String bucketName) throws Exception{
        FileInfo fileInfo = upload(file,bucketName);
        return fileInfo;
    }

    /**
     * 上传多个文件
     */
    public List<FileInfo> uploadFile(MultipartFile[] files)throws Exception {
        List<FileInfo> fileInfos = new ArrayList<>();
        for (MultipartFile file : files) {
            fileInfos.add(upload(file,BUCKET_NAME));
        }
        return fileInfos;
    }

    /**
     * 上传多个文件
     */
    public List<FileInfo> uploadFile(MultipartFile[] files, String bucketName)throws Exception {
        List<FileInfo> fileInfos = new ArrayList<>();
        for (MultipartFile file : files) {
            fileInfos.add(upload(file,bucketName));
        }
        return fileInfos;
    }

    private FileInfo upload(MultipartFile file,String bucketName)throws Exception{
        String filename = file.getOriginalFilename();
        String type = filename.substring(filename.lastIndexOf(".") + 1);
        Long id = SnowflakeIdWorker.build().nextId();
        String name = id + "." + type;
        PutObjectArgs build = PutObjectArgs.builder().bucket(bucketName).object(name).stream(file.getInputStream(), file.getSize(),-1).build();
        minioClient.putObject(build);
        FileInfo fileInfo = new FileInfo();
        fileInfo.setId(id);
        fileInfo.setFileName(filename);
        fileInfo.setFileType(type);
        fileInfo.setBucket(bucketName);
        fileInfo.setPath(URL + "/" + bucketName + "/" + name);
        fileInfo.setFileSize(file.getSize());
        fileInfo.setCreateBy(SecurityUtils.getUsername());
        fileInfo.setIsDel(0L);
        return fileInfo;
    }
}
