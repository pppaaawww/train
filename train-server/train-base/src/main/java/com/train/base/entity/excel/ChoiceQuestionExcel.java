package com.train.base.entity.excel;

import com.train.base.enums.QuestionTypeEnums;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 选择题
 */
@Getter
@Setter
public class ChoiceQuestionExcel extends BaseQuestionExcel {
    /**
     * 后背答案
     */
    private List<String> alternativeAnswer;
}
