package com.train.base.entity.bo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class SpecialTrainTree {

    private List<PostSubjectNode> nodes;

    /**
     * 岗位科目树
     */
    @Setter
    @Getter
    public static class PostSubjectNode {
        private Long id;
        private String name;
        private String postLevelCode;
        private List<SubjectKnowledgePointNode> children;
    }


    @Getter
    @Setter
    public static class SubjectKnowledgePointNode {
        /**
         * 科目id
         */
        private Long id;
        /**
         * 科目名称
         */
        private String name;
        /**
         * 知识点节点
         */
        private List<KnowledgePointNode> children;
    }

    /**
     * 岗位-科目-知识点树结构中的知识点
     */
    @Getter
    @Setter
    public static class KnowledgePointNode {
        /**
         * 知识点id
         */
        private Long id;
        /**
         * 知识点名称
         */
        private String name;
        /**
         * 训练进度
         */
        private List<TrainScheduleNode> trainSchedules;
    }

    /**
     * 训练进度
     */
    @Getter
    @Setter
    public static class TrainScheduleNode {
        /**
         * 岗位级别code
         */
        private String postLevelId;
        /**
         * 问题数量
         */
        private int questionNum;
        /**
         * 训练题目数量
         */
        private int trainNum;
        /**
         * 正确题目数量
         */
        private int rightNum;
        /**
         * 错误题目数量
         */
        private int errorNum;
    }

}
