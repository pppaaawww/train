package com.train.base.entity.bo;

import lombok.Getter;
import lombok.Setter;

/**
 * @author keke
 * @description  考试通过率实体
 * @date 2024/4/20 16:38
 */
@Setter
@Getter
public class PassRate {
    /**
     * 试卷id
     */
    private Long paperId;
    /**
     * 试卷名称
     */
    private String paperName;
    /**
     * 通过率
     */
    private Double passRate;
}
