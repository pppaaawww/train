package com.train.base.entity.chart;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * 折线图的数据
 * @param <X>
 * @param <Y>
 */
@Getter
@Setter
@ToString
public class LineChartData<X,Y> {
    private List<X> xdatas;
    private List<Y> ydatas;
    private List<Y> ydatas1;
}
