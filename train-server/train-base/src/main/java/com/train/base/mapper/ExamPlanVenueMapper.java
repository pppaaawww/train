package com.train.base.mapper;

import com.train.base.domain.ExamPlanVenue;
import com.train.base.domain.ExamPlanVenueUser;

import java.time.LocalDate;
import java.util.List;

/**
 * 考试计划场次Mapper接口
 *
 * @author keke
 * @date 2024-03-29
 */
public interface ExamPlanVenueMapper {
    /**
     * 查询考试计划场次
     *
     * @param id 考试计划场次主键
     * @return 考试计划场次
     */
    public ExamPlanVenue selectExamPlanVenueById(Long id);

    /**
     * 查询考试计划场次列表
     *
     * @param examPlanVenue 考试计划场次
     * @return 考试计划场次集合
     */
    public List<ExamPlanVenue> selectExamPlanVenueList(ExamPlanVenue examPlanVenue);

    /**
     * 新增考试计划场次
     *
     * @param examPlanVenue 考试计划场次
     * @return 结果
     */
    public int insertExamPlanVenue(ExamPlanVenue examPlanVenue);

    /**
     * 修改考试计划场次
     *
     * @param examPlanVenue 考试计划场次
     * @return 结果
     */
    public int updateExamPlanVenue(ExamPlanVenue examPlanVenue);

    /**
     * 删除考试计划场次
     *
     * @param id 考试计划场次主键
     * @return 结果
     */
    public int deleteExamPlanVenueById(Long id);

    /**
     * 批量删除考试计划场次
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExamPlanVenueByIds(Long[] ids);

    /**
     * 批量删除考试场次人员
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExamPlanVenueUserByExamPlanVenueIds(Long[] ids);

    /**
     * 批量新增考试场次人员
     *
     * @param examPlanVenueUserList 考试场次人员列表
     * @return 结果
     */
    public int batchExamPlanVenueUser(List<ExamPlanVenueUser> examPlanVenueUserList);


    /**
     * 通过考试计划场次主键删除考试场次人员信息
     *
     * @param id 考试计划场次ID
     * @return 结果
     */
    public int deleteExamPlanVenueUserByExamPlanVenueId(Long id);

    /**
     * 获取考试计划的人员id
     *
     * @param planId
     * @return
     */
    List<Long> selectUserIdsByPlanId(Long planId);

    /**
     * 根据考试计划获取
     *
     * @param planId
     * @return
     */
    List<ExamPlanVenue> selectExamPlanVenueByPlanId(Long planId);

    List<ExamPlanVenue> selectExamPlanVenueDate(LocalDate month);

    List<ExamPlanVenue> selectExamPlanVenueByPlanIds(List<Long> planIds);
}
