package com.train.base.service;

import com.train.base.domain.ExamSubjectKnowledgePoints;
import com.train.base.entity.excel.PostSubjectKnowledgePointsExcel;

import java.util.List;
import java.util.Set;

/**
 * 科目知识点管理Service接口
 *
 * @author ruoyi
 * @date 2024-03-25
 */
public interface IExamSubjectKnowledgePointsService {
    /**
     * 查询科目知识点管理
     *
     * @param id 科目知识点管理主键
     * @return 科目知识点管理
     */
    public ExamSubjectKnowledgePoints selectExamSubjectKnowledgePointsById(Long id);

    /**
     * 查询科目知识点管理列表
     *
     * @param examSubjectKnowledgePoints 科目知识点管理
     * @return 科目知识点管理集合
     */
    public List<ExamSubjectKnowledgePoints> selectExamSubjectKnowledgePointsList(ExamSubjectKnowledgePoints examSubjectKnowledgePoints);

    /**
     * 新增科目知识点管理
     *
     * @param examSubjectKnowledgePoints 科目知识点管理
     * @return 结果
     */
    public int insertExamSubjectKnowledgePoints(ExamSubjectKnowledgePoints examSubjectKnowledgePoints);

    /**
     * 修改科目知识点管理
     *
     * @param examSubjectKnowledgePoints 科目知识点管理
     * @return 结果
     */
    public int updateExamSubjectKnowledgePoints(ExamSubjectKnowledgePoints examSubjectKnowledgePoints);

    /**
     * 批量删除科目知识点管理
     *
     * @param ids 需要删除的科目知识点管理主键集合
     * @return 结果
     */
    public int deleteExamSubjectKnowledgePointsByIds(Long[] ids);

    /**
     * 删除科目知识点管理信息
     *
     * @param id 科目知识点管理主键
     * @return 结果
     */
    public int deleteExamSubjectKnowledgePointsById(Long id);

    /**
     * 根据岗位信息和科目名称获取
     *
     * @param postId
     * @param subject
     * @return
     */
    ExamSubjectKnowledgePoints selectSubject(Long postId, String subject);

    /**
     * 根据科目id和知识点名称获取
     *
     * @param subjectId
     * @param knowledgePoint
     * @return
     */
    ExamSubjectKnowledgePoints selectKnowledgePoint(Long subjectId, String knowledgePoint);

    /**
     * 根据id集合查询
     *
     * @param examSubjectKnowledgePointsIds
     * @return
     */
    List<ExamSubjectKnowledgePoints> selectExamSubjectKnowledgePointsByIds(Set<Long> examSubjectKnowledgePointsIds);

    /***
     * 根据岗位id进行查询
     * @param postIds
     * @return
     */
    List<ExamSubjectKnowledgePoints> selectByPostIds(List<Long> postIds);

    /**
     * 根据科目id集合进行查询
     *
     * @param subjectIds
     * @return
     */
    List<ExamSubjectKnowledgePoints> selectBySubjectIds(List<Long> subjectIds);

    /**
     * 导入岗位-科目-知识点
     *
     * @param postSubjectKnowledgePointsExcels
     */
    void importData(List<PostSubjectKnowledgePointsExcel> postSubjectKnowledgePointsExcels);

}
