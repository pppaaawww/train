package com.train.base.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * 等级管理对象 sys_achieve
 *
 * @author keke
 * @date 2024-05-14
 */
@Setter
@Getter
public class SysAchieve extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 名称
     */
    @Excel(name = "名称")
    private String name;

    /**
     * 编码
     */
    @Excel(name = "编码")
    private String code;

    /**
     * 删除者
     */
    @Excel(name = "删除者")
    private String delBy;

    /**
     * 成就文件的ids
     */
    private Long fileId;

    /**
     * 删除
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除", width = 30, dateFormat = "yyyy-MM-dd")
    private Date delTime;

    /**
     * 文件信息
     */
    private FileInfo fileInfo;
}
