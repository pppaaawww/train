package com.train.base.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 训练记录对象 train_record
 *
 * @author keke
 * @date 2024-03-29
 */
@Getter
@Setter
public class TrainRecord extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 题目id
     */
    @Excel(name = "题目id")
    private Long questionId;
    /**
     * 题目内容
     */
    private ExamQuestionBank bank;

    /**
     * 训练人员id
     */
    @Excel(name = "训练人员id")
    private Long userId;

    /**
     * 训练时用户部门id
     */
    @Excel(name = "训练时用户部门id")
    private Long deptId;

    /**
     * 训练时用户岗位id
     */
    @Excel(name = "训练时用户岗位id")
    private Long postId;

    /**
     * 训练时用户岗位能力
     */
    @Excel(name = "训练时用户岗位能力")
    private String postLevelCode;

    /**
     * 训练题目类型：0-专项训练，1-模拟训练
     */
    @Excel(name = "训练题目类型：0-专项训练，1-模拟训练")
    private Long type;

    /**
     * 模拟训练试卷id
     */
    @Excel(name = "模拟训练试卷id")
    private Long trainSimulationTestPaperId;

    /**
     * 训练回答内容
     */
    @Excel(name = "训练回答内容")
    private String content;

    /**
     * 训练得分（模拟练习时存在）
     */
    @Excel(name = "训练得分", readConverterExp = "模=拟练习时存在")
    private Double grade;
    /**
     * 获取分数
     */
    private Double gainGrade;

    /**
     * 训练结果：0-回答错误，2-回答正确
     */
    @Excel(name = "训练结果：0-回答错误，2-回答正确")
    private Long result;

    /**
     * 是否删除：0-否 1-是
     */
    @Excel(name = "是否删除：0-否 1-是")
    private Long isDel;

    /**
     * 删除者
     */
    @Excel(name = "删除者")
    private String delBy;

    /**
     * 删除
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除", width = 30, dateFormat = "yyyy-MM-dd")
    private Date delTime;
}
