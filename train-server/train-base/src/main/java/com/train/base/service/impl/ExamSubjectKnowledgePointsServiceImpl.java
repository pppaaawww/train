package com.train.base.service.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SnowflakeIdWorker;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.system.domain.SysPost;
import com.ruoyi.system.service.ISysPostService;
import com.ruoyi.system.service.impl.SysPostServiceImpl;
import com.train.base.entity.bo.SpecialTrainTree;
import com.train.base.entity.excel.PostSubjectKnowledgePointsExcel;
import com.train.base.utils.PingYinUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.train.base.mapper.ExamSubjectKnowledgePointsMapper;
import com.train.base.domain.ExamSubjectKnowledgePoints;
import com.train.base.service.IExamSubjectKnowledgePointsService;
import org.springframework.util.ObjectUtils;

/**
 * 科目知识点管理Service业务层处理
 *
 * @author ruoyi
 * @date 2024-03-25
 */
@Service
public class ExamSubjectKnowledgePointsServiceImpl implements IExamSubjectKnowledgePointsService {
    @Autowired
    private ExamSubjectKnowledgePointsMapper examSubjectKnowledgePointsMapper;
    @Autowired
    private ISysPostService iSysPostService;

    /**
     * 查询科目知识点管理
     *
     * @param id 科目知识点管理主键
     * @return 科目知识点管理
     */
    @Override
    public ExamSubjectKnowledgePoints selectExamSubjectKnowledgePointsById(Long id) {
        return examSubjectKnowledgePointsMapper.selectExamSubjectKnowledgePointsById(id);
    }

    /**
     * 查询科目知识点管理列表
     *
     * @param examSubjectKnowledgePoints 科目知识点管理
     * @return 科目知识点管理
     */
    @Override
    public List<ExamSubjectKnowledgePoints> selectExamSubjectKnowledgePointsList(ExamSubjectKnowledgePoints examSubjectKnowledgePoints) {
        List<ExamSubjectKnowledgePoints> examSubjectKnowledgePointsList = examSubjectKnowledgePointsMapper.selectExamSubjectKnowledgePointsList(examSubjectKnowledgePoints);
        if(!ObjectUtils.isEmpty(examSubjectKnowledgePointsList)) {
            List<SysPost> sysPosts =
                    iSysPostService.selectPostListByIds(examSubjectKnowledgePointsList.stream().map(ExamSubjectKnowledgePoints::getPostId).collect(Collectors.toList()));
            Map<Long, String> postMap = sysPosts.stream().collect(Collectors.toMap(SysPost::getPostId, SysPost::getPostName));
            examSubjectKnowledgePointsList.forEach(v -> v.setPostName(postMap.get(v.getPostId())));
        }
        return examSubjectKnowledgePointsList;
    }

    /**
     * 新增科目知识点管理
     *
     * @param examSubjectKnowledgePoints 科目知识点管理
     * @return 结果
     */
    @Override
    public int insertExamSubjectKnowledgePoints(ExamSubjectKnowledgePoints examSubjectKnowledgePoints) {
        examSubjectKnowledgePoints.setId(SnowflakeIdWorker.build().nextId());
        examSubjectKnowledgePoints.setCreateTime(LocalDateTime.now());
        return examSubjectKnowledgePointsMapper.insertExamSubjectKnowledgePoints(examSubjectKnowledgePoints);
    }

    /**
     * 修改科目知识点管理
     *
     * @param examSubjectKnowledgePoints 科目知识点管理
     * @return 结果
     */
    @Override
    public int updateExamSubjectKnowledgePoints(ExamSubjectKnowledgePoints examSubjectKnowledgePoints) {
        examSubjectKnowledgePoints.setUpdateTime(DateUtils.getNowDate());
        return examSubjectKnowledgePointsMapper.updateExamSubjectKnowledgePoints(examSubjectKnowledgePoints);
    }

    /**
     * 批量删除科目知识点管理
     *
     * @param ids 需要删除的科目知识点管理主键
     * @return 结果
     */
    @Override
    public int deleteExamSubjectKnowledgePointsByIds(Long[] ids) {
        return examSubjectKnowledgePointsMapper.deleteExamSubjectKnowledgePointsByIds(ids);
    }

    /**
     * 删除科目知识点管理信息
     *
     * @param id 科目知识点管理主键
     * @return 结果
     */
    @Override
    public int deleteExamSubjectKnowledgePointsById(Long id) {
        return examSubjectKnowledgePointsMapper.deleteExamSubjectKnowledgePointsById(id);
    }

    @Override
    public ExamSubjectKnowledgePoints selectSubject(Long postId, String subject) {
        return examSubjectKnowledgePointsMapper.selectSubject(postId,subject);
    }

    @Override
    public ExamSubjectKnowledgePoints selectKnowledgePoint(Long subjectId, String knowledgePoint) {
        return examSubjectKnowledgePointsMapper.selectKnowledgePoint(subjectId,knowledgePoint);
    }

    @Override
    public List<ExamSubjectKnowledgePoints> selectExamSubjectKnowledgePointsByIds(Set<Long> examSubjectKnowledgePointsIds) {
        return examSubjectKnowledgePointsMapper.selectExamSubjectKnowledgePointsByIds(examSubjectKnowledgePointsIds.stream().toList());
    }

    @Override
    public List<ExamSubjectKnowledgePoints> selectByPostIds(List<Long> postIds) {
        return examSubjectKnowledgePointsMapper.selectByPostIds(postIds);
    }

    @Override
    public List<ExamSubjectKnowledgePoints> selectBySubjectIds(List<Long> subjectIds) {
        return examSubjectKnowledgePointsMapper.selectBySubjectIds(subjectIds);
    }

    @Override
    public void importData(List<PostSubjectKnowledgePointsExcel> postSubjectKnowledgePointsExcels) {
        if(postSubjectKnowledgePointsExcels.isEmpty()){
            return;
        }
        Map<String,Long> postMap = new HashMap<>();
        List<String> existsPost = new ArrayList<>();
        Map<String,Long> subjectMap = new HashMap<>();
        for (int i = 0; i < postSubjectKnowledgePointsExcels.size(); i++) {
            PostSubjectKnowledgePointsExcel e = postSubjectKnowledgePointsExcels.get(i);
            if(!existsPost.contains(e.getPost())){
                // 创建岗位
                SysPost sysPost = new SysPost();
                sysPost.setPostId(SnowflakeIdWorker.build().nextId());
                sysPost.setPostName(e.getPost());
                sysPost.setPostCode(PingYinUtil.getFirstLetter(e.getPost()));
                sysPost.setStatus("0");
                sysPost.setPostSort(i);
                iSysPostService.insertPost(sysPost);
                postMap.put(e.getPost(),sysPost.getPostId());
                existsPost.add(e.getPost());
                // 创建科目
                ExamSubjectKnowledgePoints subjectKnowledgePoints = new ExamSubjectKnowledgePoints();
                subjectKnowledgePoints.setIsDel(0L);
                subjectKnowledgePoints.setId(SnowflakeIdWorker.build().nextId());
                subjectKnowledgePoints.setPostId(sysPost.getPostId());
                subjectKnowledgePoints.setName(e.getSubject());
                subjectKnowledgePoints.setType(0L);
                examSubjectKnowledgePointsMapper.insertExamSubjectKnowledgePoints(subjectKnowledgePoints);
                subjectMap.put(e.getPost() + "-" + e.getSubject(),subjectKnowledgePoints.getId());
                // 创建知识点
                ExamSubjectKnowledgePoints knowledgePoints = new ExamSubjectKnowledgePoints();
                knowledgePoints.setIsDel(0L);
                knowledgePoints.setId(SnowflakeIdWorker.build().nextId());
                knowledgePoints.setSubjectId(subjectKnowledgePoints.getId());
                knowledgePoints.setPostId(sysPost.getPostId());
                knowledgePoints.setName(e.getKnow());
                knowledgePoints.setType(1L);
                examSubjectKnowledgePointsMapper.insertExamSubjectKnowledgePoints(knowledgePoints);
            }else{
                if(subjectMap.containsKey(e.getPost() + "-" + e.getSubject())){
                    // 创建知识点
                    ExamSubjectKnowledgePoints knowledgePoints = new ExamSubjectKnowledgePoints();
                    knowledgePoints.setIsDel(0L);
                    knowledgePoints.setId(SnowflakeIdWorker.build().nextId());
                    knowledgePoints.setSubjectId(subjectMap.get(e.getPost() + "-" + e.getSubject()));
                    knowledgePoints.setPostId(postMap.get(e.getPost()));
                    knowledgePoints.setName(e.getKnow());
                    knowledgePoints.setType(1L);
                    examSubjectKnowledgePointsMapper.insertExamSubjectKnowledgePoints(knowledgePoints);
                }else{
                    // 创建科目
                    ExamSubjectKnowledgePoints subjectKnowledgePoints = new ExamSubjectKnowledgePoints();
                    subjectKnowledgePoints.setIsDel(0L);
                    subjectKnowledgePoints.setId(SnowflakeIdWorker.build().nextId());
                    subjectKnowledgePoints.setPostId(postMap.get(e.getPost()));
                    subjectKnowledgePoints.setName(e.getSubject());
                    subjectKnowledgePoints.setType(0L);
                    examSubjectKnowledgePointsMapper.insertExamSubjectKnowledgePoints(subjectKnowledgePoints);
                    subjectMap.put(e.getPost() + "-" + e.getSubject(),subjectKnowledgePoints.getId());
                    // 创建知识点
                    ExamSubjectKnowledgePoints knowledgePoints = new ExamSubjectKnowledgePoints();
                    knowledgePoints.setIsDel(0L);
                    knowledgePoints.setId(SnowflakeIdWorker.build().nextId());
                    knowledgePoints.setSubjectId(subjectKnowledgePoints.getId());
                    knowledgePoints.setPostId(postMap.get(e.getPost()));
                    knowledgePoints.setName(e.getKnow());
                    knowledgePoints.setType(1L);
                    examSubjectKnowledgePointsMapper.insertExamSubjectKnowledgePoints(knowledgePoints);
                }
            }
        }
    }
}
