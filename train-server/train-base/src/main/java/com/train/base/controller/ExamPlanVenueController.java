package com.train.base.controller;

import java.time.LocalDate;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.train.base.domain.ExamPlanVenue;
import com.train.base.service.IExamPlanVenueService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 考试计划场次Controller
 * 
 * @author keke
 * @date 2024-03-29
 */
@RestController
@RequestMapping("/train/ExamPlanVenue")
public class ExamPlanVenueController extends BaseController
{
    @Autowired
    private IExamPlanVenueService examPlanVenueService;

    /**
     * 查询考试计划场次列表
     */
    @GetMapping("/list")
    public TableDataInfo list(ExamPlanVenue examPlanVenue)
    {
        startPage();
        List<ExamPlanVenue> list = examPlanVenueService.selectExamPlanVenueList(examPlanVenue);
        return getDataTable(list);
    }




    /**
     * 导出考试计划场次列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExamPlanVenue examPlanVenue)
    {
        List<ExamPlanVenue> list = examPlanVenueService.selectExamPlanVenueList(examPlanVenue);
        ExcelUtil<ExamPlanVenue> util = new ExcelUtil<ExamPlanVenue>(ExamPlanVenue.class);
        util.exportExcel(response, list, "考试计划场次数据");
    }

    /**
     * 获取考试计划场次详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(examPlanVenueService.selectExamPlanVenueById(id));
    }

    /**
     * 新增考试计划场次
     */
    @PostMapping
    public AjaxResult add(@RequestBody ExamPlanVenue examPlanVenue)
    {
        return toAjax(examPlanVenueService.insertExamPlanVenue(examPlanVenue));
    }

    /**
     * 修改考试计划场次
     */
    @PutMapping
    public AjaxResult edit(@RequestBody ExamPlanVenue examPlanVenue)
    {
        return toAjax(examPlanVenueService.updateExamPlanVenue(examPlanVenue));
    }

    /**
     * 删除考试计划场次
     */
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(examPlanVenueService.deleteExamPlanVenueByIds(ids));
    }
}
