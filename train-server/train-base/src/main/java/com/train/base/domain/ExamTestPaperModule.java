package com.train.base.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;
import java.util.List;

/**
 * 试卷模板对象 exam_test_paper_module
 *
 * @author keke
 * @date 2024-03-28
 */
@Getter
@Setter
public class ExamTestPaperModule extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 试卷名称
     */
    @Excel(name = "试卷名称")
    private String name;

    /**
     * 试用类型：0-模拟 1-考试
     */
    @Excel(name = "试用类型：0-模拟 1-考试")
    private Long userType;

    /**
     * 试卷类型：0-随机试卷 1-标准试卷
     */
    @Excel(name = "试卷类型：0-随机试卷 1-标准试卷")
    private Long type;

    /**
     * 及格分数
     */
    @Excel(name = "及格分数")
    private Long passGrade;

    /**
     * 试卷总分数
     */
    @Excel(name = "试卷总分数")
    private Double totalGrade;

    /**
     * 该试卷适合部门id
     */
    @Excel(name = "该试卷适合部门id")
    private Long deptId;

    /**
     * 该试卷适合的岗位id
     */
    @Excel(name = "该试卷适合的岗位id")
    private Long postId;

    /**
     * 该试卷时候的岗位级别(来源数据字典)
     */
    @Excel(name = "该试卷时候的岗位级别(来源数据字典)")
    private String postLevelCode;

    /**
     * 是否删除：0-否 1-是
     */
    @Excel(name = "是否删除：0-否 1-是")
    private Long isDel;

    /**
     * 删除者
     */
    @Excel(name = "删除者")
    private String delBy;

    /**
     * 删除
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除", width = 30, dateFormat = "yyyy-MM-dd")
    private Date delTime;

    /**
     * 试卷试题抽取规则信息
     */
    private List<ExamTestPaperExtractRule> examTestPaperExtractRuleList;
    /**
     * 试卷必选题目信息
     */
    private List<ExamTestPaperRequired> examTestPaperRequired;

}
