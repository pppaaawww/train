package com.train.base.service.impl;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.SnowflakeIdWorker;
import com.ruoyi.system.service.ISysUserService;
import com.train.base.domain.AchieveImproveApplication;
import com.train.base.domain.SysAchieve;
import com.train.base.domain.SysUserAchieve;
import com.train.base.enums.AchieveImproveApprovedStateEnums;
import com.train.base.mapper.AchieveImproveApplicationMapper;
import com.train.base.service.IAchieveImproveApplicationService;
import com.train.base.service.ISysAchieveService;
import com.train.base.service.ISysUserAchieveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 等级申请Service业务层处理
 *
 * @author keke
 * @date 2024-05-14
 */
@Service
public class AchieveImproveApplicationServiceImpl implements IAchieveImproveApplicationService {
    @Autowired
    private AchieveImproveApplicationMapper achieveImproveApplicationMapper;
    @Autowired
    private ISysUserAchieveService iSysUserAchieveService;
    @Autowired
    private ISysUserService iSysUserService;
    @Autowired
    private ISysAchieveService iSysAchieveService;

    /**
     * 查询等级申请
     *
     * @param id 等级申请主键
     * @return 等级申请
     */
    @Override
    public AchieveImproveApplication selectAchieveImproveApplicationById(Long id) {
        AchieveImproveApplication achieveImproveApplication = achieveImproveApplicationMapper.selectAchieveImproveApplicationById(id);
        // 查询用户名称
        List<Long> userIds = new ArrayList<>(2);
        userIds.add(achieveImproveApplication.getReviewer());
        userIds.add(achieveImproveApplication.getApplicant());
        Map<Long, String> userMap = iSysUserService.selectUserByIds(userIds).stream().collect(Collectors.toMap(SysUser::getUserId, SysUser::getNickName));
        // 查询成就名称
        SysAchieve achieve = iSysAchieveService.selectSysAchieveById(achieveImproveApplication.getAchieveId());
        achieveImproveApplication.setAchieveName(achieve.getName());
        achieveImproveApplication.setReviewerName(userMap.get(achieveImproveApplication.getReviewer()));
        achieveImproveApplication.setApplicantName(userMap.get(achieveImproveApplication.getApplicant()));
        return achieveImproveApplication;
    }

    /**
     * 查询等级申请列表
     *
     * @param achieveImproveApplication 等级申请
     * @return 等级申请
     */
    @Override
    public List<AchieveImproveApplication> selectAchieveImproveApplicationList(AchieveImproveApplication achieveImproveApplication) {
        List<AchieveImproveApplication> achieveImproveApplications = achieveImproveApplicationMapper.selectAchieveImproveApplicationList(achieveImproveApplication);
        // 查询用户名称
        if (ObjectUtils.isEmpty(achieveImproveApplications)) {
            return new ArrayList<>();
        }
        List<Long> userIds = achieveImproveApplications.stream().map(AchieveImproveApplication::getReviewer).collect(Collectors.toList());
        List<Long> applicantIds = achieveImproveApplications.stream().map(AchieveImproveApplication::getApplicant).toList();
        if(!ObjectUtils.isEmpty(applicantIds)) {
            userIds.addAll(applicantIds);
        }
        Map<Long, String> userMap = iSysUserService.selectUserByIds(userIds).stream().collect(Collectors.toMap(SysUser::getUserId, SysUser::getNickName));
        // 查询成就名称
        List<Long> achieveIds = achieveImproveApplications.stream().map(AchieveImproveApplication::getAchieveId).toList();
        Map<Long, String> achieveMap = iSysAchieveService.selectSysAchieveByIds(achieveIds).stream().collect(Collectors.toMap(SysAchieve::getId, SysAchieve::getName));
        achieveImproveApplications.forEach(v -> {
            if(!ObjectUtils.isEmpty(userMap.get(v.getReviewer()))) {
                v.setReviewerName(userMap.get(v.getReviewer()));
            }
            if(!ObjectUtils.isEmpty(userMap.get(v.getApplicant()))) {
                v.setApplicantName(userMap.get(v.getApplicant()));
            }
            if(!ObjectUtils.isEmpty(achieveMap.get(v.getAchieveId()))) {
                v.setAchieveName(achieveMap.get(v.getAchieveId()));
            }
        });
        return achieveImproveApplications;
    }

    /**
     * 新增等级申请
     *
     * @param achieveImproveApplication 等级申请
     * @return 结果
     */
    @Override
    public int insertAchieveImproveApplication(AchieveImproveApplication achieveImproveApplication) {
        achieveImproveApplication.setId(SnowflakeIdWorker.build().nextId());
        achieveImproveApplication.setCreateTime(LocalDateTime.now());
        achieveImproveApplication.setApplicant(SecurityUtils.getUserId());
        return achieveImproveApplicationMapper.insertAchieveImproveApplication(achieveImproveApplication);
    }

    /**
     * 修改等级申请
     *
     * @param achieveImproveApplication 等级申请
     * @return 结果
     */
    @Override
    public int updateAchieveImproveApplication(AchieveImproveApplication achieveImproveApplication) {
        achieveImproveApplication.setUpdateTime(DateUtils.getNowDate());
        // 判断审核结果
        if (AchieveImproveApprovedStateEnums.PASS.getCode().equals(achieveImproveApplication.getState())) {
            AchieveImproveApplication achieve = achieveImproveApplicationMapper.selectAchieveImproveApplicationById(achieveImproveApplication.getId());
            // 审核通过，添加用户和成就的关联关系
            SysUserAchieve sysUserAchieve = new SysUserAchieve();
            sysUserAchieve.setAchieveId(achieve.getAchieveId());
            sysUserAchieve.setUserId(achieve.getApplicant());
            sysUserAchieve.setId(SnowflakeIdWorker.build().nextId());
            iSysUserAchieveService.insertSysUserAchieve(sysUserAchieve);
        }
        achieveImproveApplication.setReviewer(SecurityUtils.getUserId());
        return achieveImproveApplicationMapper.updateAchieveImproveApplication(achieveImproveApplication);
    }

    /**
     * 批量删除等级申请
     *
     * @param ids 需要删除的等级申请主键
     * @return 结果
     */
    @Override
    public int deleteAchieveImproveApplicationByIds(Long[] ids) {
        return achieveImproveApplicationMapper.deleteAchieveImproveApplicationByIds(ids);
    }

    /**
     * 删除等级申请信息
     *
     * @param id 等级申请主键
     * @return 结果
     */
    @Override
    public int deleteAchieveImproveApplicationById(Long id) {
        return achieveImproveApplicationMapper.deleteAchieveImproveApplicationById(id);
    }

    @Override
    public int review(AchieveImproveApplication achieveImproveApplication) {
        // 判断审核结果
        if (AchieveImproveApprovedStateEnums.PASS.getCode().equals(achieveImproveApplication.getState())) {
            AchieveImproveApplication achieve = achieveImproveApplicationMapper.selectAchieveImproveApplicationById(achieveImproveApplication.getId());
            // 审核通过，添加用户和成就的关联关系
            SysUserAchieve sysUserAchieve = new SysUserAchieve();
            sysUserAchieve.setAchieveId(achieve.getAchieveId());
            sysUserAchieve.setUserId(achieve.getApplicant());
            sysUserAchieve.setId(SnowflakeIdWorker.build().nextId());
            iSysUserAchieveService.insertSysUserAchieve(sysUserAchieve);
        }
        achieveImproveApplication.setReviewer(SecurityUtils.getUserId());
        return achieveImproveApplicationMapper.updateAchieveImproveApplication(achieveImproveApplication);
    }
}
