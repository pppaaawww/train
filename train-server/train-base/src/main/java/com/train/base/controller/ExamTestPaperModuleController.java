package com.train.base.controller;

import java.util.List;
import java.util.Objects;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.train.base.domain.ExamTestPaperModule;
import com.train.base.service.IExamTestPaperModuleService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 试卷模板Controller
 * 
 * @author keke
 * @date 2024-03-28
 */
@RestController
@RequestMapping("/train/ExamTestPaperModule")
public class ExamTestPaperModuleController extends BaseController
{
    @Autowired
    private IExamTestPaperModuleService examTestPaperModuleService;

    /**
     * 查询试卷模板列表
     */
    @GetMapping("/list")
    public TableDataInfo list(ExamTestPaperModule examTestPaperModule)
    {
        startPage();
        List<ExamTestPaperModule> list = examTestPaperModuleService.selectExamTestPaperModuleList(examTestPaperModule);
        return getDataTable(list);
    }

    /**
     * 导出试卷模板列表
     */
    @Log(title = "试卷模板", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExamTestPaperModule examTestPaperModule)
    {
        List<ExamTestPaperModule> list = examTestPaperModuleService.selectExamTestPaperModuleList(examTestPaperModule);
        ExcelUtil<ExamTestPaperModule> util = new ExcelUtil<ExamTestPaperModule>(ExamTestPaperModule.class);
        util.exportExcel(response, list, "试卷模板数据");
    }

    /**
     * 获取试卷模板详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(examTestPaperModuleService.selectExamTestPaperModuleById(id));
    }

    /**
     * 新增试卷模板
     */
    @Log(title = "试卷模板", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ExamTestPaperModule examTestPaperModule)
    {
        if(Objects.nonNull(examTestPaperModuleService.getByName(examTestPaperModule.getName()))){
            return error("名称重复维护");
        }
        return toAjax(examTestPaperModuleService.insertExamTestPaperModule(examTestPaperModule));
    }

    /**
     * 修改试卷模板
     */
    @Log(title = "试卷模板", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ExamTestPaperModule examTestPaperModule)
    {
        return toAjax(examTestPaperModuleService.updateExamTestPaperModule(examTestPaperModule));
    }

    /**
     * 删除试卷模板
     */
    @Log(title = "试卷模板", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(examTestPaperModuleService.deleteExamTestPaperModuleByIds(ids));
    }
}
