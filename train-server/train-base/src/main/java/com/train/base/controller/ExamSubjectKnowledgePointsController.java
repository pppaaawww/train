package com.train.base.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.train.base.domain.ExamQuestionBank;
import com.train.base.domain.ExamSubjectKnowledgePoints;
import com.train.base.entity.excel.PostSubjectKnowledgePointsExcel;
import com.train.base.service.IExamSubjectKnowledgePointsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 科目知识点管理Controller
 *
 * @author ruoyi
 * @date 2024-03-25
 */
@RestController
@RequestMapping("/base/ExamSubjectKnowledgePoints")
public class ExamSubjectKnowledgePointsController extends BaseController {
    @Autowired
    private IExamSubjectKnowledgePointsService IExamSubjectKnowledgePointsService;

    /**
     * 查询科目知识点管理列表
     */
    @GetMapping("/list")
    public TableDataInfo list(ExamSubjectKnowledgePoints examSubjectKnowledgePoints) {
        startPage();
        List<ExamSubjectKnowledgePoints> list = IExamSubjectKnowledgePointsService.selectExamSubjectKnowledgePointsList(examSubjectKnowledgePoints);
        return getDataTable(list);
    }

    @Log(title = "题库管理", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file) throws Exception {
        List<PostSubjectKnowledgePointsExcel> postSubjectKnowledgePointsExcels = EasyExcel.read(file.getInputStream())
                .head(PostSubjectKnowledgePointsExcel.class)
                .sheet()
                .doReadSync();
        IExamSubjectKnowledgePointsService.importData(postSubjectKnowledgePointsExcels);
        return success("导入成功");
    }


    /**
     * 导出科目知识点管理列表
     */

    @Log(title = "科目知识点管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExamSubjectKnowledgePoints examSubjectKnowledgePoints) {
        List<ExamSubjectKnowledgePoints> list = IExamSubjectKnowledgePointsService.selectExamSubjectKnowledgePointsList(examSubjectKnowledgePoints);
        ExcelUtil<ExamSubjectKnowledgePoints> util = new ExcelUtil<ExamSubjectKnowledgePoints>(ExamSubjectKnowledgePoints.class);
        util.exportExcel(response, list, "科目知识点管理数据");
    }

    /**
     * 获取科目知识点管理详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(IExamSubjectKnowledgePointsService.selectExamSubjectKnowledgePointsById(id));
    }

    /**
     * 新增科目知识点管理
     */
    @Log(title = "科目知识点管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ExamSubjectKnowledgePoints examSubjectKnowledgePoints) {
        return toAjax(IExamSubjectKnowledgePointsService.insertExamSubjectKnowledgePoints(examSubjectKnowledgePoints));
    }

    /**
     * 修改科目知识点管理
     */
    @Log(title = "科目知识点管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ExamSubjectKnowledgePoints examSubjectKnowledgePoints) {
        return toAjax(IExamSubjectKnowledgePointsService.updateExamSubjectKnowledgePoints(examSubjectKnowledgePoints));
    }

    /**
     * 删除科目知识点管理
     */
    @Log(title = "科目知识点管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(IExamSubjectKnowledgePointsService.deleteExamSubjectKnowledgePointsByIds(ids));
    }

}
