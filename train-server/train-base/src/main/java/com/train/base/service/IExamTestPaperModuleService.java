package com.train.base.service;

import com.train.base.domain.ExamTestPaperModule;
import com.train.base.entity.bo.Question;

import java.util.List;

/**
 * 试卷模板Service接口
 *
 * @author keke
 * @date 2024-03-28
 */
public interface IExamTestPaperModuleService {
    /**
     * 查询试卷模板
     *
     * @param id 试卷模板主键
     * @return 试卷模板
     */
    public ExamTestPaperModule selectExamTestPaperModuleById(Long id);

    /**
     * 查询试卷模板列表
     *
     * @param examTestPaperModule 试卷模板
     * @return 试卷模板集合
     */
    public List<ExamTestPaperModule> selectExamTestPaperModuleList(ExamTestPaperModule examTestPaperModule);

    /**
     * 新增试卷模板
     *
     * @param examTestPaperModule 试卷模板
     * @return 结果
     */
    public int insertExamTestPaperModule(ExamTestPaperModule examTestPaperModule);

    /**
     * 修改试卷模板
     *
     * @param examTestPaperModule 试卷模板
     * @return 结果
     */
    public int updateExamTestPaperModule(ExamTestPaperModule examTestPaperModule);

    /**
     * 批量删除试卷模板
     *
     * @param ids 需要删除的试卷模板主键集合
     * @return 结果
     */
    public int deleteExamTestPaperModuleByIds(Long[] ids);

    /**
     * 删除试卷模板信息
     *
     * @param id 试卷模板主键
     * @return 结果
     */
    public int deleteExamTestPaperModuleById(Long id);

    /**
     * 根据规则获取该规则生成试卷的考题id集合
     *
     * @param examTestPaperModule
     * @return
     */
    List<Question> createPaper(ExamTestPaperModule examTestPaperModule);

    /**
     * 根据id集合获取
     * @param moduleIds
     * @return
     */
    List<ExamTestPaperModule> selectExamTestPaperModuleByIds(List<Long> moduleIds);

    ExamTestPaperModule getByName(String name);
}
