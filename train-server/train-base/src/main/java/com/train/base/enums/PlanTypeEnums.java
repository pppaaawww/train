package com.train.base.enums;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author baiyang
 * 考试计划类型
 */
public enum PlanTypeEnums {

    FIXED(0L,"固定计划"),
    CYCLE(1L,"周期计划");

    private Long code;
    private String value;
    private PlanTypeEnums(Long code, String value){
        this.code = code;
        this.value = value;
    }

    public Long getCode(){
        return code;
    };

    public String getValue(){
        return value;
    }

    private static Map<Long,String> map = new HashMap<>();

    static {
        for (PlanTypeEnums e : PlanTypeEnums.values()) {
            map.put(e.getCode(),e.getValue());
        }
    }

    public static String getValueByCode(Integer code){
        return map.get(code);
    }

    public static Collection<Long> getCodes(){
        return map.keySet();
    }

}
