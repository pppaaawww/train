package com.train.base.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 考试记录对象 exam_record
 * 
 * @author keke
 * @date 2024-04-01
 */
@Getter
@Setter
public class ExamRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 题目id */
    @Excel(name = "题目id")
    private Long questionId;

    /** 人员id */
    @Excel(name = "人员id")
    private Long userId;

    /** 试卷id */
    @Excel(name = "试卷id")
    private Long testPaperId;

    /** 回答内容 */
    @Excel(name = "回答内容")
    private String content;

    /** 结果：0-回答错误，2-回答正确 */
    @Excel(name = "结果：0-回答错误，2-回答正确")
    private Long result;

    /** 试题分数 */
    @Excel(name = "试题分数")
    private Double grade;

    /** 获取分数 */
    @Excel(name = "获取分数")
    private Double gainGrade;

    /** 是否删除：0-否 1-是 */
    @Excel(name = "是否删除：0-否 1-是")
    private Long isDel;

    /** 删除者 */
    @Excel(name = "删除者")
    private String delBy;

    /** 删除 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除", width = 30, dateFormat = "yyyy-MM-dd")
    private Date delTime;

}
