package com.train.base.entity.bo;

import lombok.Getter;
import lombok.Setter;

/**
 * @author keke
 * @description  历史考试的第一名
 * @date 2024/4/22 19:09
 */
@Setter
@Getter
public class HistoryExam {

    /**
     * 试卷名称
     */
    private String paperName;
    /**
     * 试卷id
     */
    private Long paperId;
    /**
     * 考试人id
     */
    private Long userId;
    /**
     * 考试人名称
     */
    private String userName;
    /**
     * 试卷总分数
     */
    private Double grade;
    /**
     * 取得的分数
     */
    private Double gainGrade;
}
