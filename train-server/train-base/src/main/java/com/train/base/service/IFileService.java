package com.train.base.service;

import com.train.base.domain.FileInfo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface IFileService {
    /**
     * 上传文档
     *
     * @param file
     * @return
     */
    FileInfo upload(MultipartFile file, Long relId);

    /**
     * 查询文件列表
     * @param fileInfo  查询参数
     * @return 文件列表
     */
    List<FileInfo> selectFileInfo(FileInfo fileInfo);

    /**
     * 根据id删除文件
     * @param ids
     * @return
     */
    int deleteFileByIds(Long[] ids);

    /**
     * 根据文件的id绑定关联的业务id
     * @param fileInfo 条件
     * @return  影响的记录条数
     */
    void updateRelIdByFileId(FileInfo fileInfo);

    /**
     * 根据业务的id查询文件信息
     * @param relIds 业务id
     * @return  文件信息
     */
    List<FileInfo> selectFileByRelIds(List<Long> relIds);

    /**
     * 根据业务的id查询文件信息
     * @param relId 业务id
     * @return  文件信息
     */
    FileInfo selectFileByRelId(Long relId);
    /**
     * 根据文件的id查询文件信息
     * @param id 文件id
     * @return
     */
    FileInfo selectFileById(Long id);
}
