package com.train.base.service.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SnowflakeIdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.train.base.domain.ExamPlanVenueUser;
import com.train.base.mapper.ExamPlanVenueMapper;
import com.train.base.domain.ExamPlanVenue;
import com.train.base.service.IExamPlanVenueService;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 考试计划场次Service业务层处理
 *
 * @author keke
 * @date 2024-03-29
 */
@Service
public class ExamPlanVenueServiceImpl implements IExamPlanVenueService {
    @Autowired
    private ExamPlanVenueMapper examPlanVenueMapper;

    /**
     * 查询考试计划场次
     *
     * @param id 考试计划场次主键
     * @return 考试计划场次
     */
    @Override
    public ExamPlanVenue selectExamPlanVenueById(Long id) {
        return examPlanVenueMapper.selectExamPlanVenueById(id);
    }

    @Override
    public List<ExamPlanVenue> selectExamPlanVenueByPlanId(Long planId) {
        return examPlanVenueMapper.selectExamPlanVenueByPlanId(planId);
    }


    /**
     * 查询考试计划场次列表
     *
     * @param examPlanVenue 考试计划场次
     * @return 考试计划场次
     */
    @Override
    public List<ExamPlanVenue> selectExamPlanVenueList(ExamPlanVenue examPlanVenue) {
        return examPlanVenueMapper.selectExamPlanVenueList(examPlanVenue);
    }

    /**
     * 新增考试计划场次
     *
     * @param examPlanVenue 考试计划场次
     * @return 结果
     */
    @Transactional
    @Override
    public int insertExamPlanVenue(ExamPlanVenue examPlanVenue) {
        if(ObjectUtils.isEmpty(examPlanVenue.getExamPlanVenueUserList())) {
            return 0;
        }
        examPlanVenue.setCreateTime(LocalDateTime.now());
        examPlanVenue.setId(SnowflakeIdWorker.build().nextId());
        int rows = examPlanVenueMapper.insertExamPlanVenue(examPlanVenue);
        insertExamPlanVenueUser(examPlanVenue);
        return rows;
    }

    /**
     * 修改考试计划场次
     *
     * @param examPlanVenue 考试计划场次
     * @return 结果
     */
    @Transactional
    @Override
    public int updateExamPlanVenue(ExamPlanVenue examPlanVenue) {
        if(ObjectUtils.isEmpty(examPlanVenue.getExamPlanVenueUserList())) {
            return 0;
        }
        examPlanVenue.setUpdateTime(DateUtils.getNowDate());
        examPlanVenueMapper.deleteExamPlanVenueUserByExamPlanVenueId(examPlanVenue.getId());
        insertExamPlanVenueUser(examPlanVenue);
        return examPlanVenueMapper.updateExamPlanVenue(examPlanVenue);
    }

    /**
     * 批量删除考试计划场次
     *
     * @param ids 需要删除的考试计划场次主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteExamPlanVenueByIds(Long[] ids) {
        examPlanVenueMapper.deleteExamPlanVenueUserByExamPlanVenueIds(ids);
        return examPlanVenueMapper.deleteExamPlanVenueByIds(ids);
    }

    /**
     * 删除考试计划场次信息
     *
     * @param id 考试计划场次主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteExamPlanVenueById(Long id) {
        examPlanVenueMapper.deleteExamPlanVenueUserByExamPlanVenueId(id);
        return examPlanVenueMapper.deleteExamPlanVenueById(id);
    }

    @Override
    public List<Long> selectUserIdsByPlanId(Long id) {
        return examPlanVenueMapper.selectUserIdsByPlanId(id);
    }

    @Override
    public List<ExamPlanVenue> selectExamPlanVenueDate(LocalDate month) {
        return examPlanVenueMapper.selectExamPlanVenueDate(month);
    }

    @Override
    public List<ExamPlanVenue> selectExamPlanVenueByPlanIds(List<Long> planIds) {
        if(CollectionUtils.isEmpty(planIds)){
            return Collections.emptyList();
        }
        return examPlanVenueMapper.selectExamPlanVenueByPlanIds(planIds);
    }

    /**
     * 新增考试场次人员信息
     *
     * @param examPlanVenue 考试计划场次对象
     */
    public void insertExamPlanVenueUser(ExamPlanVenue examPlanVenue) {
        List<ExamPlanVenueUser> examPlanVenueUserList = examPlanVenue.getExamPlanVenueUserList();
        Long id = examPlanVenue.getId();
        if (StringUtils.isNotNull(examPlanVenueUserList)) {
            List<ExamPlanVenueUser> list = new ArrayList<ExamPlanVenueUser>();
            for (ExamPlanVenueUser examPlanVenueUser : examPlanVenueUserList) {
                examPlanVenueUser.setExamPlanVenueId(id);
                examPlanVenueUser.setId(SnowflakeIdWorker.build().nextId());
                examPlanVenueUser.setExamPlanId(examPlanVenue.getExamPlanId());
                list.add(examPlanVenueUser);
            }
            if (list.size() > 0) {
                examPlanVenueMapper.batchExamPlanVenueUser(list);
            }
        }
    }
}
