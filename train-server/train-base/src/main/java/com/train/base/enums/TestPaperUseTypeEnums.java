package com.train.base.enums;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * 试卷模板的使用类型
 */
public enum TestPaperUseTypeEnums {
    TRAIN(0L, "模拟"),
    EXAM(1L, "考试");
    private Long code;
    private String value;

    private TestPaperUseTypeEnums(Long code, String value) {
        this.code = code;
        this.value = value;
    }

    public Long getCode() {
        return code;
    }

    ;

    public String getValue() {
        return value;
    }

    private static Map<Long, String> map = new HashMap<>();

    static {
        for (TestPaperUseTypeEnums e : TestPaperUseTypeEnums.values()) {
            map.put(e.getCode(), e.getValue());
        }
    }

    public static String getValueByCode(Integer code) {
        return map.get(code);
    }

    public static Collection<Long> getCodes() {
        return map.keySet();
    }
}
