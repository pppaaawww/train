package com.train.base.mapper;

import com.train.base.domain.ExamPlan;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;
import java.util.List;

/**
 * 考试计划Mapper接口
 *
 * @author keke
 * @date 2024-03-28
 */
public interface ExamPlanMapper {
    /**
     * 查询考试计划
     *
     * @param id 考试计划主键
     * @return 考试计划
     */
    public ExamPlan selectExamPlanById(Long id);

    /**
     * 查询考试计划列表
     *
     * @param examPlan 考试计划
     * @return 考试计划集合
     */
    public List<ExamPlan> selectExamPlanList(ExamPlan examPlan);

    /**
     * 新增考试计划
     *
     * @param examPlan 考试计划
     * @return 结果
     */
    public int insertExamPlan(ExamPlan examPlan);

    /**
     * 修改考试计划
     *
     * @param examPlan 考试计划
     * @return 结果
     */
    public int updateExamPlan(ExamPlan examPlan);

    /**
     * 删除考试计划
     *
     * @param id 考试计划主键
     * @return 结果
     */
    public int deleteExamPlanById(Long id);

    /**
     * 批量删除考试计划
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExamPlanByIds(Long[] ids);

    /**
     * 根据考试计划的IDS更新计划的状态
     *
     * @param ids    考试计划的IDS
     * @param status 更新的状态
     * @return
     */
    int updateStatus(@Param("ids") List<Long> ids, @Param("status") Long status);

    List<ExamPlan> selectExamPlanByIds(List<Long> ids);

    /**
     * 获取需要发布生成的计划
     *
     * @return
     */
    List<ExamPlan> selectNeedCreatePlan();
    /**
     * 根据创建时间查询已完成的考试计划
     * @return
     */
    List<Long> selectExamPlanOrderByCreateTime(@Param("limitNum") Integer limitNum);
}
