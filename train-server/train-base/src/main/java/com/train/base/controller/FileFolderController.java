package com.train.base.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.train.base.converter.FileConverter;
import com.train.base.domain.FileFolder;
import com.train.base.domain.FileInfo;
import com.train.base.service.IFileFolderService;
import com.train.base.service.impl.FileServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 资源文件夹Controller
 *
 * @author keke
 * @date 2024-04-07
 */
@RestController
@RequestMapping("/FileFolder")
public class FileFolderController extends BaseController {
    @Autowired
    private IFileFolderService fileFolderService;
    @Autowired
    private FileServiceImpl fileService;
    @Autowired
    private FileConverter fileConverter;

        @GetMapping("tree")
    public TableDataInfo tree(FileFolder fileFolder){
        startPage();
        List<FileFolder> list = fileFolderService.selectFileFolderList(fileFolder);
        if(list.isEmpty()){
            return getDataTable(list);
        }
        List<Long> folderIds = list.stream().map(FileFolder::getId).collect(Collectors.toList());
        List<FileInfo> fileInfos = fileService.selectFileByRelIds(folderIds);
        Map<Long,List<FileInfo>> fileMap = fileInfos.stream().collect(Collectors.groupingBy(FileInfo::getRelId));
        list.forEach(e->{
            e.setChildren(fileMap.get(e.getId()));
        });
        return getDataTable(list);
    }

    /**
     * 查询资源文件夹列表
     */
    @GetMapping("/list")
    public TableDataInfo list(FileFolder fileFolder) {
        startPage();
        List<FileFolder> list = fileFolderService.selectFileFolderList(fileFolder);
        return getDataTable(list);
    }

    /**
     * 导出资源文件夹列表
     */
    @Log(title = "资源文件夹", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FileFolder fileFolder) {
        List<FileFolder> list = fileFolderService.selectFileFolderList(fileFolder);
        ExcelUtil<FileFolder> util = new ExcelUtil<FileFolder>(FileFolder.class);
        util.exportExcel(response, list, "资源文件夹数据");
    }

    /**
     * 获取资源文件夹详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(fileFolderService.selectFileFolderById(id));
    }

    /**
     * 新增资源文件夹
     */
    @Log(title = "资源文件夹", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FileFolder fileFolder) {
        return toAjax(fileFolderService.insertFileFolder(fileFolder));
    }

    /**
     * 修改资源文件夹
     */
    @Log(title = "资源文件夹", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FileFolder fileFolder) {
        return toAjax(fileFolderService.updateFileFolder(fileFolder));
    }

    /**
     * 删除资源文件夹
     */
    @Log(title = "资源文件夹", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(fileFolderService.deleteFileFolderByIds(ids));
    }
}
