package com.train.base.service.impl;

import com.ruoyi.common.utils.SecurityUtils;
import com.train.base.domain.FileInfo;
import com.train.base.mapper.FileInfoMapper;
import com.train.base.service.IFileService;
import com.train.base.utils.MinioUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;

@Service
public class FileServiceImpl implements IFileService {
    @Autowired
    private MinioUtils minioUtils;
    @Autowired
    private FileInfoMapper mapper;
    @Override
    public FileInfo upload(MultipartFile file, Long relId){
        FileInfo fileInfo = null;
        try {
            fileInfo = minioUtils.uploadFile(file);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        fileInfo.setRelId(relId);
        mapper.insert(fileInfo);
        return fileInfo;
    }

    @Override
    public List<FileInfo> selectFileInfo(FileInfo fileInfo) {
        return mapper.list(fileInfo);
    }

    @Override
    public int deleteFileByIds(Long[] ids) {
        return mapper.deleteByIds(Arrays.asList(ids), SecurityUtils.getUserId());
    }

    @Override
    public void updateRelIdByFileId(FileInfo fileInfo) {
        mapper.updateById(fileInfo);
    }

    @Override
    public List<FileInfo> selectFileByRelIds(List<Long> relIds) {
        return mapper.selectFileByRelIds(relIds);
    }

    @Override
    public FileInfo selectFileByRelId(Long relId) {
        return mapper.selectFileByRelId(relId);
    }

    @Override
    public FileInfo selectFileById(Long id) {
        return mapper.selectFIleById(id);
    }
}
