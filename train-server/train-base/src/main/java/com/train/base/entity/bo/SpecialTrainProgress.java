package com.train.base.entity.bo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SpecialTrainProgress {
    /**
     * 科目id
     */
    private Long subjectId;
    /**
     * 科目id
     */
    private String subjectName;
    /**
     * 总数量
     */
    private int num;
    /**
     * 正确数量
     */
    private int rightNum;
    /**
     * 错误数量
     */
    private int errorNum;
    /**
     * 训练数量
     */
    private int trainNum;
    /**
     * 训练题目数量
     */
    private int trainQuestionNum;

}
