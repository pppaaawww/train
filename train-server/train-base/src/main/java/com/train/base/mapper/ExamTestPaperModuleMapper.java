package com.train.base.mapper;

import com.train.base.domain.ExamTestPaperExtractRule;
import com.train.base.domain.ExamTestPaperModule;
import com.train.base.domain.ExamTestPaperRequired;

import java.util.List;

/**
 * 试卷模板Mapper接口
 *
 * @author keke
 * @date 2024-03-28
 */
public interface ExamTestPaperModuleMapper {
    /**
     * 查询试卷模板
     *
     * @param id 试卷模板主键
     * @return 试卷模板
     */
    public ExamTestPaperModule selectExamTestPaperModuleById(Long id);

    /**
     * 查询试卷模板列表
     *
     * @param examTestPaperModule 试卷模板
     * @return 试卷模板集合
     */
    public List<ExamTestPaperModule> selectExamTestPaperModuleList(ExamTestPaperModule examTestPaperModule);

    /**
     * 新增试卷模板
     *
     * @param examTestPaperModule 试卷模板
     * @return 结果
     */
    public int insertExamTestPaperModule(ExamTestPaperModule examTestPaperModule);

    /**
     * 修改试卷模板
     *
     * @param examTestPaperModule 试卷模板
     * @return 结果
     */
    public int updateExamTestPaperModule(ExamTestPaperModule examTestPaperModule);

    /**
     * 删除试卷模板
     *
     * @param id 试卷模板主键
     * @return 结果
     */
    public int deleteExamTestPaperModuleById(Long id);

    /**
     * 批量删除试卷模板
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExamTestPaperModuleByIds(Long[] ids);

    /**
     * 批量删除试卷试题抽取规则
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExamTestPaperExtractRuleByExamTestPaperModuleIds(Long[] ids);

    /**
     * 批量新增试卷试题抽取规则
     *
     * @param examTestPaperExtractRuleList 试卷试题抽取规则列表
     * @return 结果
     */
    public int batchExamTestPaperExtractRule(List<ExamTestPaperExtractRule> examTestPaperExtractRuleList);


    /**
     * 通过试卷模板主键删除试卷试题抽取规则信息
     *
     * @param id 试卷模板ID
     * @return 结果
     */
    public int deleteExamTestPaperExtractRuleByExamTestPaperModuleId(Long id);

    /**
     * 批量新增试卷固定题目
     *
     * @param list
     */
    public int batchExamTestPaperRequired(List<ExamTestPaperRequired> list);

    /**
     * 通过模板id删除试卷模板的固定题目
     *
     * @param id
     */
    public int deleteExamTestPaperRequiredByExamTestPaperModuleId(Long id);

    /**
     * 根据模板id进行批量删除
     *
     * @param ids
     * @return
     */
    public int deleteExamTestPaperRequiredByExamTestPaperModuleIds(Long[] ids);

    /**
     * 根据id集合获取
     *
     * @param moduleIds
     * @return
     */
    List<ExamTestPaperModule> selectExamTestPaperModuleByIds(List<Long> list);

    ExamTestPaperModule getByName(String name);
}
