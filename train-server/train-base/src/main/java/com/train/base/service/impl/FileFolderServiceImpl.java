package com.train.base.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SnowflakeIdWorker;
import com.train.base.domain.FileFolder;
import com.train.base.mapper.FileFolderMapper;
import com.train.base.service.IFileFolderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 资源文件夹Service业务层处理
 *
 * @author keke
 * @date 2024-04-07
 */
@Service
public class FileFolderServiceImpl implements IFileFolderService {
    @Autowired
    private FileFolderMapper fileFolderMapper;

    /**
     * 查询资源文件夹
     *
     * @param id 资源文件夹主键
     * @return 资源文件夹
     */
    @Override
    public FileFolder selectFileFolderById(Long id) {
        return fileFolderMapper.selectFileFolderById(id);
    }

    /**
     * 查询资源文件夹列表
     *
     * @param fileFolder 资源文件夹
     * @return 资源文件夹
     */
    @Override
    public List<FileFolder> selectFileFolderList(FileFolder fileFolder) {
        return fileFolderMapper.selectFileFolderList(fileFolder);
    }

    /**
     * 新增资源文件夹
     *
     * @param fileFolder 资源文件夹
     * @return 结果
     */
    @Override
    public int insertFileFolder(FileFolder fileFolder) {
        fileFolder.setId(SnowflakeIdWorker.build().nextId());
        fileFolder.setCreateTime(LocalDateTime.now());
        return fileFolderMapper.insertFileFolder(fileFolder);
    }

    /**
     * 修改资源文件夹
     *
     * @param fileFolder 资源文件夹
     * @return 结果
     */
    @Override
    public int updateFileFolder(FileFolder fileFolder) {
        fileFolder.setUpdateTime(DateUtils.getNowDate());
        return fileFolderMapper.updateFileFolder(fileFolder);
    }

    /**
     * 批量删除资源文件夹
     *
     * @param ids 需要删除的资源文件夹主键
     * @return 结果
     */
    @Override
    public int deleteFileFolderByIds(Long[] ids) {
        return fileFolderMapper.deleteFileFolderByIds(ids);
    }

    /**
     * 删除资源文件夹信息
     *
     * @param id 资源文件夹主键
     * @return 结果
     */
    @Override
    public int deleteFileFolderById(Long id) {
        return fileFolderMapper.deleteFileFolderById(id);
    }
}
