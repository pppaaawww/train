package com.train.base.service;

import com.train.base.domain.ExamTestPaper;
import com.train.base.entity.bo.HistoryExam;
import com.train.base.entity.bo.PaperAnswer;
import com.train.base.entity.bo.PaperGradingRecord;
import com.train.base.entity.bo.PassRate;
import com.train.base.entity.bo.Question;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * 试卷管理Service接口
 *
 * @author keke
 * @date 2024-04-01
 */
public interface IExamTestPaperService {
    /**
     * 查询试卷管理
     *
     * @param id 试卷管理主键
     * @return 试卷管理
     */
    public ExamTestPaper selectExamTestPaperById(Long id);

    /**
     * 查询试卷管理列表
     *
     * @param examTestPaper 试卷管理
     * @return 试卷管理集合
     */
    public List<ExamTestPaper> selectExamTestPaperList(ExamTestPaper examTestPaper);

    /**
     * 新增试卷管理
     *
     * @param examTestPaper 试卷管理
     * @return 结果
     */
    public int insertExamTestPaper(ExamTestPaper examTestPaper);

    /**
     * 修改试卷管理
     *
     * @param examTestPaper 试卷管理
     * @return 结果
     */
    public int updateExamTestPaper(ExamTestPaper examTestPaper);

    /**
     * 批量删除试卷管理
     *
     * @param ids 需要删除的试卷管理主键集合
     * @return 结果
     */
    public int deleteExamTestPaperByIds(Long[] ids);

    /**
     * 删除试卷管理信息
     *
     * @param id 试卷管理主键
     * @return 结果
     */
    public int deleteExamTestPaperById(Long id);

    /**
     * 领取试卷
     *
     * @param id
     * @return
     */
    List<Question> receive(Long id);

    /**
     * 批量新增试卷
     *
     * @param testPapers
     */
    void batchInsertExamTestPaper(List<ExamTestPaper> testPapers);

    /**
     * 获取试卷问题详情
     *
     * @param id
     * @return
     */
    List<Question> getDetail(Long id);

    /**
     * 提交试卷
     *
     * @param answer
     * @return
     */
    int submitPaper(PaperAnswer answer);

    /**
     * 批阅试卷
     *
     * @param paperGradingRecord
     * @return
     */
    int gradingPaper(PaperGradingRecord paperGradingRecord);

    /**
     * 获取某个月某个用户的考试试卷
     *
     * @param month
     * @return
     */
    List<ExamTestPaper> listByDate(LocalDate month);

    /**
     * 根据计划id集合进行获取
     *
     * @param planIds
     * @return
     */
    List<ExamTestPaper> selectExamTestPaperByPlanIds(List<Long> planIds);

    /**
     * 获取前几次考试前十名的用户
     *
     * @return 排名信息
     */
    Map<Integer, List<ExamTestPaper>> examRanking();

    /**
     * 获取前十次考试的通过率
     *
     * @return
     */
    List<PassRate> passRate();

    /**
     * 获取历史考试的第一名
     *
     * @return 信息
     */
    List<HistoryExam> historyExamFirst();

    /**
     * 获取错题占比
     */
    Map<String,Integer> errorProportion();

    /**
     * 根据用户的id查询该用户最近的一次考试
     * @param userId  用户的id
     * @param postId  岗位id
     * @return 用户最近的一次考试
     */
    ExamTestPaper selectExamTestPaperByUserId(Long userId, Long postId);
}
