package com.train.base.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 科目知识点管理对象 exam_subject_knowledge_points
 * 
 * @author ruoyi
 * @date 2024-03-25
 */
public class ExamSubjectKnowledgePoints extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 科目/知识点名称 */
    @Excel(name = "科目/知识点名称")
    private String name;

    /** 0-科目；1-知识点 */
    @Excel(name = "0-科目；1-知识点")
    private Long type;

    /** 当type=1时存在，并为某个科目的id */
    @Excel(name = "当type=1时存在，并为某个科目的id")
    private Long subjectId;

    /** 当type=0时存在，并为某个岗位的id */
    @Excel(name = "当type=0时存在，并为某个岗位的id")
    private Long postId;

    /** 是否删除：0-否 1-是 */
    @Excel(name = "是否删除：0-否 1-是")
    private Long isDel;

    /** 删除者 */
    @Excel(name = "删除者")
    private String delBy;

    /** 删除 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除", width = 30, dateFormat = "yyyy-MM-dd")
    private Date delTime;

    //=========================新增====================================

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    /** 岗位名称 */
    private String postName;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setSubjectId(Long subjectId) 
    {
        this.subjectId = subjectId;
    }

    public Long getSubjectId() 
    {
        return subjectId;
    }
    public void setPostId(Long postId) 
    {
        this.postId = postId;
    }

    public Long getPostId() 
    {
        return postId;
    }
    public void setIsDel(Long isDel) 
    {
        this.isDel = isDel;
    }

    public Long getIsDel() 
    {
        return isDel;
    }
    public void setDelBy(String delBy) 
    {
        this.delBy = delBy;
    }

    public String getDelBy() 
    {
        return delBy;
    }
    public void setDelTime(Date delTime) 
    {
        this.delTime = delTime;
    }

    public Date getDelTime() 
    {
        return delTime;
    }

    @Override
    public String toString() {
        return "ExamSubjectKnowledgePoints{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", subjectId=" + subjectId +
                ", postId=" + postId +
                ", isDel=" + isDel +
                ", delBy='" + delBy + '\'' +
                ", delTime=" + delTime +
                ", postName='" + postName + '\'' +
                '}';
    }
}
