package com.train.base.mapper;

import com.train.base.domain.FileInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author baiyang
 */
@Mapper
public interface FileInfoMapper {

    /**
     * 根据条件获取一个
     *
     * @param condition condition
     * @return po
     */
    public FileInfo get(FileInfo condition);

    /**
     * 根据id获取一个
     *
     * @param id long
     * @return po
     */
    public FileInfo getById(Long id);

    /**
     * 根据id集合获取多个
     *
     * @param ids id集合
     * @return list
     */
    public List<FileInfo> listByIds(List<Long> ids);


    /**
     * 根绝条件获取多个
     *
     * @param condition condition
     * @return list
     */
    public List<FileInfo> list(FileInfo condition);

    /**
     * 插入
     *
     * @param p po
     */
    public void insert(FileInfo p);

    /**
     * 批量插入
     *
     * @param ps list
     **/
    public void insertBatch(@Param(value = "list") List<FileInfo> ps);

    /**
     * 根绝id进行删除，逻辑删除，返回值表明是否对数据进行了操作，，如果未对任何行进行操作，则返回false
     *
     * @param id 主键
     * @return 操作返回结果
     */
    public boolean deleteById(Long id, @Param(value = "delBy") Long delBy);

    /**
     * 根绝id集合进行删除，逻辑删除，返回值表明是否对数据进行了操作，，如果未对任何行进行操作，则返回false
     *
     * @param ids 主键
     * @return 操作返回结果
     */
    public int deleteByIds(@Param(value = "list") List<Long> ids, @Param(value = "delBy") Long delBy);

    /**
     * 根据id进行更新，p中的id字段不能未空
     *
     * @param p
     */
    public void updateById(FileInfo p);

    /**
     * 根据业务的id查询文件信息
     *
     * @param relIds 业务id
     * @return 文件信息
     */
    List<FileInfo> selectFileByRelIds(@Param("relIds") List<Long> relIds);

    /**
     * 根据业务的id查询文件信息
     *
     * @param relId 业务id
     * @return 文件信息
     */
    FileInfo selectFileByRelId(@Param("relId") Long relId);

    /**
     * 根据文件的id查询文件信息
     * @param id 文件id
     * @return
     */
    FileInfo selectFIleById(@Param("id") Long id);
}
