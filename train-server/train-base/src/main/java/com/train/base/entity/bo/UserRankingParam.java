package com.train.base.entity.bo;

import lombok.Getter;
import lombok.Setter;

/**
 * @author keke
 * @description 人员数量排行
 * @date 2024/4/20 11:04
 */
@Getter
@Setter
public class UserRankingParam {

    /**
     * 训练人员id
     */
    private Long userId;
    /**
     * 训练时用户岗位id
     */
    private Long postId;

    /**
     * 训练时用户岗位能力
     */
    private String postLevelCode;
    /**
     * 岗位能力级别
     */
    private String postAbility;
    /**
     * 数量
     */
    private Integer num;
    /**
     * 错题数量
     */
    private Integer errorNum;
    /**
     * 错误率
     */
    private Double errorRate;

}
