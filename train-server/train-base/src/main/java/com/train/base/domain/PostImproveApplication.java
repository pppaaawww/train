package com.train.base.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 岗位提升申请对象 post_improve_application
 *
 * @author keke
 * @date 2024-04-09
 */
@Getter
@Setter
public class PostImproveApplication extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    private Long postId;
    private String postName;

    /** 申请前code */
    @Excel(name = "申请前code")
    private String oldCode;

    /** 申请后code */
    @Excel(name = "申请后code")
    private String toCode;

    /** 类型：0-岗位级别。1-岗位能力 */
    @Excel(name = "类型：0-岗位级别。1-岗位能力")
    private Long type;

    /** 审核状态：0-待审核，1-通过，2-不通过 */
    @Excel(name = "审核状态：0-待审核，1-通过，2-不通过")
    private Long state;

    /** 审核人 */
    @Excel(name = "审核人")
    private Long reviewer;
    private String reviewerName;


    /** 是否删除：0-否 1-是 */
    @Excel(name = "是否删除：0-否 1-是")
    private Long isDel;

    /** 删除者 */
    @Excel(name = "删除者")
    private String delBy;
    /**
     * 申请人
     */
    private Long applicant;
    private String applicantName;

    /** 删除 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除", width = 30, dateFormat = "yyyy-MM-dd")
    private Date delTime;

    /**
     * 试卷id
     */
    private Long examPaperId;

    /**
     * 实操分数
     */
    private Double operationGrade;

    /**
     * 应急分数
     */
    private Double emergencyGrade;
}
