package com.train.base.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;
import java.util.List;

/**
 * 资源文件夹对象 file_folder
 *
 * @author keke
 * @date 2024-04-07
 */
public class FileFolder extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 名称
     */
    @Excel(name = "名称")
    private String name;

    /**
     * 删除标记：0 未删除 1 删除
     */
    private Long isDel;

    /**
     * 删除人id
     */
    private String delBy;

    /**
     * 删除时间
     */
    private Date delTime;

    private List<FileInfo> children;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setIsDel(Long isDel) {
        this.isDel = isDel;
    }

    public Long getIsDel() {
        return isDel;
    }

    public void setDelBy(String delBy) {
        this.delBy = delBy;
    }

    public String getDelBy() {
        return delBy;
    }

    public void setDelTime(Date delTime) {
        this.delTime = delTime;
    }

    public Date getDelTime() {
        return delTime;
    }

    public List<FileInfo> getChildren() {
        return children;
    }

    public void setChildren(List<FileInfo> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("name", getName())
                .append("remark", getRemark())
                .append("createTime", getCreateTime())
                .append("createBy", getCreateBy())
                .append("updateTime", getUpdateTime())
                .append("updateBy", getUpdateBy())
                .append("isDel", getIsDel())
                .append("delBy", getDelBy())
                .append("delTime", getDelTime())
                .toString();
    }
}
