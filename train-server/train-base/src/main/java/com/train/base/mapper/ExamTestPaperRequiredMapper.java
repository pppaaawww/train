package com.train.base.mapper;

import com.train.base.domain.ExamTestPaperRequired;

import java.util.List;

/**
 * 试卷必选题目Mapper接口
 * 
 * @author keke
 * @date 2024-03-28
 */
public interface ExamTestPaperRequiredMapper 
{
    /**
     * 查询试卷必选题目
     * 
     * @param id 试卷必选题目主键
     * @return 试卷必选题目
     */
    public ExamTestPaperRequired selectExamTestPaperRequiredById(Long id);

    /**
     * 查询试卷必选题目列表
     * 
     * @param examTestPaperRequired 试卷必选题目
     * @return 试卷必选题目集合
     */
    public List<ExamTestPaperRequired> selectExamTestPaperRequiredList(ExamTestPaperRequired examTestPaperRequired);

    /**
     * 新增试卷必选题目
     * 
     * @param examTestPaperRequired 试卷必选题目
     * @return 结果
     */
    public int insertExamTestPaperRequired(ExamTestPaperRequired examTestPaperRequired);

    /**
     * 修改试卷必选题目
     * 
     * @param examTestPaperRequired 试卷必选题目
     * @return 结果
     */
    public int updateExamTestPaperRequired(ExamTestPaperRequired examTestPaperRequired);

    /**
     * 删除试卷必选题目
     * 
     * @param id 试卷必选题目主键
     * @return 结果
     */
    public int deleteExamTestPaperRequiredById(Long id);

    /**
     * 批量删除试卷必选题目
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExamTestPaperRequiredByIds(Long[] ids);
}
