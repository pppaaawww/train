package com.train.base.mapper;


import com.train.base.domain.AchieveImproveApplication;

import java.util.List;

/**
 * 等级申请Mapper接口
 *
 * @author keke
 * @date 2024-05-14
 */
public interface AchieveImproveApplicationMapper {
    /**
     * 查询等级申请
     *
     * @param id 等级申请主键
     * @return 等级申请
     */
    public AchieveImproveApplication selectAchieveImproveApplicationById(Long id);

    /**
     * 查询等级申请列表
     *
     * @param achieveImproveApplication 等级申请
     * @return 等级申请集合
     */
    public List<AchieveImproveApplication> selectAchieveImproveApplicationList(AchieveImproveApplication achieveImproveApplication);

    /**
     * 新增等级申请
     *
     * @param achieveImproveApplication 等级申请
     * @return 结果
     */
    public int insertAchieveImproveApplication(AchieveImproveApplication achieveImproveApplication);

    /**
     * 修改等级申请
     *
     * @param achieveImproveApplication 等级申请
     * @return 结果
     */
    public int updateAchieveImproveApplication(AchieveImproveApplication achieveImproveApplication);

    /**
     * 删除等级申请
     *
     * @param id 等级申请主键
     * @return 结果
     */
    public int deleteAchieveImproveApplicationById(Long id);

    /**
     * 批量删除等级申请
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAchieveImproveApplicationByIds(Long[] ids);
}
