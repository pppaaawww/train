package com.train.base.domain;

import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 题库管理对象 exam_question_bank
 * 
 * @author ruoyi
 * @date 2024-03-26
 */
public class ExamQuestionBank extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 考题编码 */
    @Excel(name = "考题编码")
    private String code;

    /** 该考题适合部门id */
    @Excel(name = "该考题适合部门id")
    private Long deptId;

    /** 大纲：来源于数据字典 */
    @Excel(name = "大纲：来源于数据字典")
    private String syllabusCode;

    /** 该考题适合的岗位id */
    @Excel(name = "该考题适合的岗位id")
    private Long postId;

    /** 该考题所属科目 */
    @Excel(name = "该考题所属科目")
    private Long subjectId;
    /**
     * 科目id
     */
    private String subjectName;

    /** 该考题所属知识点id */
    @Excel(name = "该考题所属知识点id")
    private Long knowledgePointsId;
    /**
     * 知识点名称
     */
    private String knowledgePointsName;

    /** 考题类型：1-判断题 2-填空题 3-问答题 4-论述题 5-选择题 */
    @Excel(name = "考题类型：1-判断题 2-填空题 3-问答题 4-论述题 5-选择题")
    private Long questionType;

    /** 题干内容 */
    @Excel(name = "题干内容")
    private String content;

    /** 正确答案 */
    @Excel(name = "正确答案")
    private String rightKey;

    /** 适合的能力级别，来源于数据字典 */
    @Excel(name = "适合的能力级别，来源于数据字典")
    private String postLevelCode;

    /** 试题解析 */
    @Excel(name = "试题解析")
    private String analysis;

    /** 是否删除：0-否 1-是 */
    @Excel(name = "是否删除：0-否 1-是")
    private Long isDel;

    /** 删除者 */
    @Excel(name = "删除者")
    private String delBy;

    /** 删除 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除", width = 30, dateFormat = "yyyy-MM-dd")
    private Date delTime;

    /** 考题选项 适用于填空、选择题信息 */
    private List<ExamQuestionBankOption> examQuestionBankOptionList;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }
    public void setSyllabusCode(String syllabusCode) 
    {
        this.syllabusCode = syllabusCode;
    }

    public String getSyllabusCode() 
    {
        return syllabusCode;
    }
    public void setPostId(Long postId) 
    {
        this.postId = postId;
    }

    public Long getPostId() 
    {
        return postId;
    }
    public void setSubjectId(Long subjectId) 
    {
        this.subjectId = subjectId;
    }

    public Long getSubjectId() 
    {
        return subjectId;
    }
    public void setKnowledgePointsId(Long knowledgePointsId) 
    {
        this.knowledgePointsId = knowledgePointsId;
    }

    public Long getKnowledgePointsId() 
    {
        return knowledgePointsId;
    }
    public void setQuestionType(Long questionType) 
    {
        this.questionType = questionType;
    }

    public Long getQuestionType() 
    {
        return questionType;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setRightKey(String rightKey) 
    {
        this.rightKey = rightKey;
    }

    public String getRightKey() 
    {
        return rightKey;
    }
    public void setPostLevelCode(String postLevelCode) 
    {
        this.postLevelCode = postLevelCode;
    }

    public String getPostLevelCode() 
    {
        return postLevelCode;
    }
    public void setAnalysis(String analysis) 
    {
        this.analysis = analysis;
    }

    public String getAnalysis() 
    {
        return analysis;
    }
    public void setIsDel(Long isDel) 
    {
        this.isDel = isDel;
    }

    public Long getIsDel() 
    {
        return isDel;
    }
    public void setDelBy(String delBy) 
    {
        this.delBy = delBy;
    }

    public String getDelBy() 
    {
        return delBy;
    }
    public void setDelTime(Date delTime) 
    {
        this.delTime = delTime;
    }

    public Date getDelTime() 
    {
        return delTime;
    }

    public List<ExamQuestionBankOption> getExamQuestionBankOptionList()
    {
        return examQuestionBankOptionList;
    }

    public void setExamQuestionBankOptionList(List<ExamQuestionBankOption> examQuestionBankOptionList)
    {
        this.examQuestionBankOptionList = examQuestionBankOptionList;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getKnowledgePointsName() {
        return knowledgePointsName;
    }

    public void setKnowledgePointsName(String knowledgePointsName) {
        this.knowledgePointsName = knowledgePointsName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("deptId", getDeptId())
            .append("syllabusCode", getSyllabusCode())
            .append("postId", getPostId())
            .append("subjectId", getSubjectId())
            .append("knowledgePointsId", getKnowledgePointsId())
            .append("questionType", getQuestionType())
            .append("content", getContent())
            .append("rightKey", getRightKey())
            .append("postLevelCode", getPostLevelCode())
            .append("analysis", getAnalysis())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("isDel", getIsDel())
            .append("delBy", getDelBy())
            .append("delTime", getDelTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("examQuestionBankOptionList", getExamQuestionBankOptionList())
            .toString();
    }
}
