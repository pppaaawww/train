package com.train.base.entity.chart;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Objects;

/**
 * 旭日图的数据结构
 *
 * @author baiyang
 * @since 2024-04-29
 */
@Getter
@Setter
public class SunriseChartData {
    private Long id;
    /**
     * 0- 岗位
     * 1- 科目
     * 2- 知识点
     */
    private Integer type;
    private String name;
    private Integer value;
    private List<SunriseChartData> children;

    public void setValue(Integer value) {
        if(Objects.isNull(value)){
            this.value = 0;
        }else{
            this.value = value;
        }
    }
}
