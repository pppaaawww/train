package com.train.base.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 等级申请对象 achieve_improve_application
 *
 * @author keke
 * @date 2024-05-14
 */

@Setter
@Getter
public class AchieveImproveApplication extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 申请的成就id
     */
    @Excel(name = "申请的成就id")
    private Long achieveId;
    private String achieveName;

    /**
     * 审核状态：0-待审核，1-通过，2-不通过
     */
    @Excel(name = "审核状态：0-待审核，1-通过，2-不通过")
    private Long state;

    /**
     * 审核人
     */
    @Excel(name = "审核人")
    private Long reviewer;
    private String reviewerName;

    /**
     * 申请人
     */
    @Excel(name = "申请人")
    private Long applicant;
    private String applicantName;

    /**
     * 删除者
     */
    @Excel(name = "删除者")
    private String delBy;

    /**
     * 删除
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除", width = 30, dateFormat = "yyyy-MM-dd")
    private Date delTime;
}
