package com.train.base.config;

import io.minio.MinioClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ImosMinioClient {

    @Value("${baiyang.minio.url}")
    private String URL = null;
    @Value("${baiyang.minio.accessKey}")
    private String ACCESS_KEY = null;
    @Value("${baiyang.minio.secretKey}")
    private String SECRET_KEY = null;
    @Bean
    public MinioClient initClient(){
        return MinioClient.builder()
                        .endpoint(URL)
                        .credentials(ACCESS_KEY, SECRET_KEY)
                        .build();
    }

}
