package com.train.base.controller;

import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.system.service.ISysUserService;
import com.train.base.domain.ExamQuestionBank;
import com.train.base.domain.TrainRecord;
import com.train.base.domain.TrainTestPaper;
import com.train.base.entity.bo.PaperAnswer;
import com.train.base.entity.bo.Question;
import com.train.base.entity.bo.SpecialTrainRule;
import com.train.base.entity.bo.StatisticalParam;
import com.train.base.entity.bo.UserRankingParam;
import com.train.base.service.ITrainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 训练
 */
@RestController
@RequestMapping("/train")
public class TrainController extends BaseController {
    @Autowired
    private ITrainService iTrainService;
    @Autowired
    private ISysUserService sysUserService;



    /**
     * 根据用户信息随机获取训练试题
     *
     * @param rule
     * @return
     */
    @GetMapping(value = "getSpecialQuestion")
    @RepeatSubmit(interval = 1001, message = "请求过于频繁")
    public AjaxResult getSpecialQuestion(@Validated SpecialTrainRule rule) {
        Question question = iTrainService.getSpecialQuestion(rule);
        return AjaxResult.success(question);
    }

    /**
     * 回答专项训练题目
     *
     * @param record
     * @return
     */
    @PutMapping(value = "answerSpecialQuestion")
    public AjaxResult answerSpecialQuestion(@RequestBody TrainRecord record) {
        return toAjax(iTrainService.answerSpecialQuestion(record));
    }

    /**
     * 获取专项训练的题目
     *
     * @param rule 规则
     * @return
     */
    @GetMapping("listSpecial")
    public TableDataInfo listSpecial(SpecialTrainRule rule) {
        startPage();
        List<Question> list = iTrainService.listSpecial(rule);
        return getDataTable(list);
    }

    /**
     * 分页获取历史专项题训练记录
     *
     * @param bank
     * @return
     */
    @GetMapping(value = "listSpecialRecode")
    public TableDataInfo listSpecialRecode(ExamQuestionBank bank) {
        List<TrainRecord> list = iTrainService.listSpecialRecode(bank);
        return getDataTable(list);
    }


    /**
     * 生成训练试卷
     *
     * @param moduleId 规则id
     * @return
     */
    @PostMapping("createTrainPaper")
    public AjaxResult createTrainPaper(@RequestBody Long moduleId) {
        TrainTestPaper trainTestPaper = iTrainService.createTrainPaper(moduleId);
        return success(trainTestPaper);
    }


    /**
     * 获取个人模拟试卷列表
     *
     * @return
     */
    @GetMapping(value = "queryUserTrainPaper")
    public TableDataInfo queryUserTrainPaper(TrainTestPaper trainTestPaper) {
        startPage();
        List<TrainTestPaper> testPapers = iTrainService.listUserTrainPaper(trainTestPaper);
        if (testPapers.isEmpty()) {
            return getDataTable(testPapers);
        }
        List<Long> userIds = testPapers.stream().map(TrainTestPaper::getUserId).toList();
        List<SysUser> sysUsers = sysUserService.selectUserByIds(userIds);
        Map<Long, String> userMap = sysUsers.stream().collect(Collectors.toMap(SysUser::getUserId, SysUser::getUserName));
        testPapers.forEach(e -> {
            e.setUserName(userMap.get(e.getUserId()));
        });
        return getDataTable(testPapers);
    }

    /**
     * 获取某个模拟试卷详情
     *
     * @param id 模拟试卷id
     * @return
     */
    @GetMapping(value = "getPaperDetail")
    public AjaxResult getPaperDetail(@RequestParam Long id) {
        return AjaxResult.success(iTrainService.getPaperDetail(id));
    }

    /**
     * 获取某个月专项训练题目数量
     *
     * @return
     */
    @GetMapping(value = "getTrainNumByMonth")
    public AjaxResult getTrainNumByMonth() {
        return AjaxResult.success(iTrainService.getTrainNumByMonth());
    }

    /**
     * 获取某个人的可以答题的科目的分析
     *
     * @return
     */
    @GetMapping(value = "getTrainSubjectAnalysis")
    public AjaxResult getTrainSubjectAnalysis() {
        return success(iTrainService.getTrainSubjectAnalysis());
    }

    /**
     * 提交试卷
     *
     * @param answer
     * @return
     */
    @PostMapping(value = "submitPaper")
    public AjaxResult submitPaper(@RequestBody PaperAnswer answer) {
        return toAjax(iTrainService.submitPaper(answer));
    }

    /**
     * 记录训练记录(针对专项训练是调用)
     *
     * @Deprecated
     */
    @PostMapping(value = "addRecord")
    public AjaxResult addRecord(@RequestBody TrainRecord record) {
        return toAjax(iTrainService.insertTrainRecord(record));
    }

    /**
     * 获取某个岗位下以科目为维度的训练进度
     *
     * @return
     */
    @GetMapping(value = "specialTrainProgress")
    public AjaxResult specialTrainProgress(@RequestParam Long postId, @RequestParam String postLevelCode) {
        return success(iTrainService.specialTrainProgress(postId, postLevelCode));
    }

    /**
     * 获取一段时间训练题目数量和错题数量
     * @Deprecated
     */
    @GetMapping("getTrainNumAndErrorNum")
    public AjaxResult getTrainNumAndErrorNum(StatisticalParam statisticalParam) {
        return success(iTrainService.getTrainNumAndErrorNum(statisticalParam));
    }

    /**
     * 人员训练数量
     */
    @GetMapping("userRanking")
    public TableDataInfo userRanking(UserRankingParam param) {
        startPage();
        List<UserRankingParam> list = iTrainService.userRanking(param);
        return getDataTable(list);
    }

    /**
     * 获取常用的模板
     */
    @GetMapping("commonTemplates")
    public AjaxResult commonTemplates() {
        return AjaxResult.success(iTrainService.commonTemplates());
    }

    /**
     * 训练错误数量占比
     */
    @GetMapping("errorProportion")
    public AjaxResult errorProportion() {
        return AjaxResult.success(iTrainService.errorProportion());
    }

    /**
     * 模拟试卷列表
     */
    @GetMapping("paperList")
    public TableDataInfo paperList(TrainTestPaper trainTestPaper) {
        startPage();
        List<TrainTestPaper> testPapers = iTrainService.paperList(trainTestPaper);
        if (testPapers.isEmpty()) {
            return getDataTable(testPapers);
        }
        List<Long> userIds = testPapers.stream().map(TrainTestPaper::getUserId).toList();
        List<SysUser> sysUsers = sysUserService.selectUserByIds(userIds);
        Map<Long, String> userMap = sysUsers.stream().collect(Collectors.toMap(SysUser::getUserId, SysUser::getUserName));
        testPapers.forEach(e -> e.setUserName(userMap.get(e.getUserId())));
        return getDataTable(testPapers);
    }
}
