package com.train.base.service.impl;

import com.ruoyi.common.core.domain.model.UserAchieveVo;
import com.ruoyi.common.utils.DateUtils;
import com.train.base.domain.FileInfo;
import com.train.base.domain.SysAchieve;
import com.train.base.domain.SysUserAchieve;
import com.train.base.mapper.SysUserAchieveMapper;
import com.train.base.service.IFileService;
import com.train.base.service.ISysAchieveService;
import com.train.base.service.ISysUserAchieveService;
import net.sf.jsqlparser.util.validation.metadata.DatabaseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 等级管理Service业务层处理
 *
 * @author keke
 * @date 2024-05-14
 */
@Service
public class SysUserAchieveServiceImpl implements ISysUserAchieveService {
    @Autowired
    private SysUserAchieveMapper sysUserAchieveMapper;
    @Autowired
    private IFileService iFileService;
    @Autowired
    private ISysAchieveService iSysAchieveService;

    /**
     * 查询等级管理
     *
     * @param id 等级管理主键
     * @return 等级管理
     */
    @Override
    public SysUserAchieve selectSysUserAchieveById(Long id) {
        return sysUserAchieveMapper.selectSysUserAchieveById(id);
    }

    /**
     * 查询等级管理列表
     *
     * @param sysUserAchieve 等级管理
     * @return 等级管理
     */
    @Override
    public List<SysUserAchieve> selectSysUserAchieveList(SysUserAchieve sysUserAchieve) {
        return sysUserAchieveMapper.selectSysUserAchieveList(sysUserAchieve);
    }

    /**
     * 新增等级管理
     *
     * @param sysUserAchieve 等级管理
     * @return 结果
     */
    @Override
    public int insertSysUserAchieve(SysUserAchieve sysUserAchieve) {
        sysUserAchieve.setCreateTime(LocalDateTime.now());
        return sysUserAchieveMapper.insertSysUserAchieve(sysUserAchieve);
    }

    /**
     * 修改等级管理
     *
     * @param sysUserAchieve 等级管理
     * @return 结果
     */
    @Override
    public int updateSysUserAchieve(SysUserAchieve sysUserAchieve) {
        sysUserAchieve.setUpdateTime(DateUtils.getNowDate());
        return sysUserAchieveMapper.updateSysUserAchieve(sysUserAchieve);
    }

    /**
     * 批量删除等级管理
     *
     * @param ids 需要删除的等级管理主键
     * @return 结果
     */
    @Override
    public int deleteSysUserAchieveByIds(Long[] ids) {
        return sysUserAchieveMapper.deleteSysUserAchieveByIds(ids);
    }

    /**
     * 删除等级管理信息
     *
     * @param id 等级管理主键
     * @return 结果
     */
    @Override
    public int deleteSysUserAchieveById(Long id) {
        return sysUserAchieveMapper.deleteSysUserAchieveById(id);
    }

    @Override
    public int countByAchieveIds(Long[] ids) {
        return sysUserAchieveMapper.countByAchieveIds(ids);
    }

    @Override
    public List<UserAchieveVo> listAchieveByUserId(Long userId) {
        List<SysUserAchieve> sysUserAchieves = sysUserAchieveMapper.selectSysUserAchieveByUserId(userId);
        if (ObjectUtils.isEmpty(sysUserAchieves)) {
            return new ArrayList<>();
        }
        List<Long> achieveIds = sysUserAchieves.stream().map(SysUserAchieve::getAchieveId).toList();
        //查询成就信息
        Map<Long, String> achieveMap = iSysAchieveService.selectSysAchieveByIds(achieveIds).stream().collect(Collectors.toMap(SysAchieve::getId, SysAchieve::getName));
        // 查询文件信息
        List<FileInfo> fileInfos = iFileService.selectFileByRelIds(achieveIds);
        if (ObjectUtils.isEmpty(fileInfos)) {
            return new ArrayList<>();
        }
        List<UserAchieveVo> userAchieveVos = new ArrayList<>(fileInfos.size());
        fileInfos.forEach(info -> {
            UserAchieveVo userAchieveVo = new UserAchieveVo();
            userAchieveVo.setUserId(userId);
            userAchieveVo.setPath(info.getPath());
            userAchieveVo.setFileName(info.getFileName());
            userAchieveVo.setAchieveName(achieveMap.get(info.getRelId()));
            userAchieveVos.add(userAchieveVo);
        });
        return userAchieveVos;
    }

    @Override
    public List<UserAchieveVo> listAchieveByUserIds(List<Long> userIds) {
        if (ObjectUtils.isEmpty(userIds)) {
            throw new DatabaseException("用户的ids不能为空!");
        }
        List<SysUserAchieve> sysUserAchieves = sysUserAchieveMapper.selectSysUserAchieveByUserIds(userIds);
        if (ObjectUtils.isEmpty(sysUserAchieves)) {
            return new ArrayList<>();
        }
        Map<Long, SysUserAchieve> sysUserAchieveMap = sysUserAchieves.stream().collect(Collectors.toMap(SysUserAchieve::getAchieveId, b -> b));
        List<Long> achieveIds = sysUserAchieves.stream().map(SysUserAchieve::getAchieveId).toList();
        //查询成就信息
        Map<Long, String> achieveMap = iSysAchieveService.selectSysAchieveByIds(achieveIds).stream().collect(Collectors.toMap(SysAchieve::getId, SysAchieve::getName));
        // 查询文件信息
        List<FileInfo> fileInfos = iFileService.selectFileByRelIds(achieveIds);
        if (ObjectUtils.isEmpty(fileInfos)) {
            return new ArrayList<>();
        }
        Map<Long, FileInfo> fileInfoMap = fileInfos.stream().collect(Collectors.toMap(FileInfo::getRelId, b -> b));
        List<UserAchieveVo> userAchieveVos = new ArrayList<>(fileInfos.size());
        for (Long achieveId : achieveIds) {
            SysUserAchieve sysUserAchieve = sysUserAchieveMap.get(achieveId);
            FileInfo fileInfo = fileInfoMap.get(achieveId);
            UserAchieveVo userAchieveVo = new UserAchieveVo();
            userAchieveVo.setUserId(sysUserAchieve.getUserId());
            userAchieveVo.setPath(fileInfo.getPath());
            userAchieveVo.setFileName(fileInfo.getFileName());
            userAchieveVo.setAchieveName(achieveMap.get(fileInfo.getRelId()));
            userAchieveVos.add(userAchieveVo);
        }
        return userAchieveVos;
    }
}
