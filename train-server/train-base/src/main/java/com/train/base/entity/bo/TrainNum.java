package com.train.base.entity.bo;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * 每日驯良数量
 */
@Data
public class TrainNum {
    /**
     * 日期
     */
    private LocalDate date;
    /**
     * 数量
     */
    private int num;
}
