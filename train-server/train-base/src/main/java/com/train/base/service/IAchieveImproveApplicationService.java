package com.train.base.service;


import com.train.base.domain.AchieveImproveApplication;

import java.util.List;

/**
 * 成就申请Service接口
 *
 * @author keke
 * @date 2024-05-14
 */
public interface IAchieveImproveApplicationService {
    /**
     * 查询成就申请
     *
     * @param id 成就申请主键
     * @return 成就申请
     */
    public AchieveImproveApplication selectAchieveImproveApplicationById(Long id);

    /**
     * 查询成就申请列表
     *
     * @param achieveImproveApplication 成就申请
     * @return 成就申请集合
     */
    public List<AchieveImproveApplication> selectAchieveImproveApplicationList(AchieveImproveApplication achieveImproveApplication);

    /**
     * 新增成就申请
     *
     * @param achieveImproveApplication 成就申请
     * @return 结果
     */
    public int insertAchieveImproveApplication(AchieveImproveApplication achieveImproveApplication);

    /**
     * 修改成就申请
     *
     * @param achieveImproveApplication 成就申请
     * @return 结果
     */
    public int updateAchieveImproveApplication(AchieveImproveApplication achieveImproveApplication);

    /**
     * 批量删除成就申请
     *
     * @param ids 需要删除的成就申请主键集合
     * @return 结果
     */
    public int deleteAchieveImproveApplicationByIds(Long[] ids);

    /**
     * 删除成就申请信息
     *
     * @param id 成就申请主键
     * @return 结果
     */
    public int deleteAchieveImproveApplicationById(Long id);

    /**
     * 成就申请审核
     * @param achieveImproveApplication 审核参数
     * @return 影响的记录条数
     */
    int review(AchieveImproveApplication achieveImproveApplication);
}
