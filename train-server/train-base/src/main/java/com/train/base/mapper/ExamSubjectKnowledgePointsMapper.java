package com.train.base.mapper;

import com.train.base.domain.ExamSubjectKnowledgePoints;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 科目知识点管理Mapper接口
 *
 * @author ruoyi
 * @date 2024-03-25
 */
public interface ExamSubjectKnowledgePointsMapper {
    /**
     * 查询科目知识点管理
     *
     * @param id 科目知识点管理主键
     * @return 科目知识点管理
     */
    public ExamSubjectKnowledgePoints selectExamSubjectKnowledgePointsById(Long id);

    /**
     * 查询科目知识点管理列表
     *
     * @param examSubjectKnowledgePoints 科目知识点管理
     * @return 科目知识点管理集合
     */
    public List<ExamSubjectKnowledgePoints> selectExamSubjectKnowledgePointsList(ExamSubjectKnowledgePoints examSubjectKnowledgePoints);

    /**
     * 新增科目知识点管理
     *
     * @param examSubjectKnowledgePoints 科目知识点管理
     * @return 结果
     */
    public int insertExamSubjectKnowledgePoints(ExamSubjectKnowledgePoints examSubjectKnowledgePoints);

    /**
     * 修改科目知识点管理
     *
     * @param examSubjectKnowledgePoints 科目知识点管理
     * @return 结果
     */
    public int updateExamSubjectKnowledgePoints(ExamSubjectKnowledgePoints examSubjectKnowledgePoints);

    /**
     * 删除科目知识点管理
     *
     * @param id 科目知识点管理主键
     * @return 结果
     */
    public int deleteExamSubjectKnowledgePointsById(Long id);

    /**
     * 批量删除科目知识点管理
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExamSubjectKnowledgePointsByIds(Long[] ids);

    /**
     * 根据岗位id和科目名称获取科目信息
     *
     * @param postId
     * @param subject
     * @return
     */
    ExamSubjectKnowledgePoints selectSubject(@Param(value = "postId") Long postId, @Param(value = "subject") String subject);

    /**
     * 根据科目id和知识点名称获取知识点信息
     *
     * @param subjectId
     * @param knowledgePoint
     * @return
     */
    ExamSubjectKnowledgePoints selectKnowledgePoint(@Param(value = "subjectId") Long subjectId, @Param(value = "knowledgePoint") String knowledgePoint);

    /**
     * 根基id集合查询
     *
     * @param examSubjectKnowledgePointsIds
     * @return
     */
    List<ExamSubjectKnowledgePoints> selectExamSubjectKnowledgePointsByIds(List<Long> examSubjectKnowledgePointsIds);

    /**
     * 根据岗位id集合进行查询
     *
     * @param postIds
     * @return
     */
    List<ExamSubjectKnowledgePoints> selectByPostIds(List<Long> postIds);

    /**
     * 根据科目id集合进行查询
     *
     * @param subjectIds
     * @return
     */
    List<ExamSubjectKnowledgePoints> selectBySubjectIds(List<Long> subjectIds);
}
