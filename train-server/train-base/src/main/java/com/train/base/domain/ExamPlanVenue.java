package com.train.base.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * 考试计划场次对象 exam_plan_venue
 *
 * @author keke
 * @date 2024-03-29
 */
public class ExamPlanVenue extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 考试计划id
     */
    @Excel(name = "考试计划id")
    private Long examPlanId;

    private List<Long> examPlanIds;

    /**
     * 考试计划开始时间
     */
    @Excel(name = "考试计划开始时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime examStartTime;

    /**
     * 考试计划结束时间
     */
    @Excel(name = "考试计划结束时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime examEndTime;

    /**
     * 考试地点
     */
    @Excel(name = "考试地点")
    private String examPlace;

    /**
     * 是否删除：0-否 1-是
     */
    @Excel(name = "是否删除：0-否 1-是")
    private Long isDel;

    /**
     * 删除者
     */
    @Excel(name = "删除者")
    private String delBy;

    /**
     * 删除
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除", width = 30, dateFormat = "yyyy-MM-dd")
    private Date delTime;

    /**
     * 考试场次人员信息
     */
    private List<ExamPlanVenueUser> examPlanVenueUserList;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public List<Long> getExamPlanIds() {
        return examPlanIds;
    }

    public void setExamPlanIds(List<Long> examPlanIds) {
        this.examPlanIds = examPlanIds;
    }

    public void setExamPlanId(Long examPlanId) {
        this.examPlanId = examPlanId;
    }

    public Long getExamPlanId() {
        return examPlanId;
    }

    public LocalDateTime getExamStartTime() {
        return examStartTime;
    }

    public void setExamStartTime(LocalDateTime examStartTime) {
        this.examStartTime = examStartTime;
    }

    public LocalDateTime getExamEndTime() {
        return examEndTime;
    }

    public void setExamEndTime(LocalDateTime examEndTime) {
        this.examEndTime = examEndTime;
    }

    public void setExamPlace(String examPlace) {
        this.examPlace = examPlace;
    }

    public String getExamPlace() {
        return examPlace;
    }

    public void setIsDel(Long isDel) {
        this.isDel = isDel;
    }

    public Long getIsDel() {
        return isDel;
    }

    public void setDelBy(String delBy) {
        this.delBy = delBy;
    }

    public String getDelBy() {
        return delBy;
    }

    public void setDelTime(Date delTime) {
        this.delTime = delTime;
    }

    public Date getDelTime() {
        return delTime;
    }

    public List<ExamPlanVenueUser> getExamPlanVenueUserList() {
        return examPlanVenueUserList;
    }

    public void setExamPlanVenueUserList(List<ExamPlanVenueUser> examPlanVenueUserList) {
        this.examPlanVenueUserList = examPlanVenueUserList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("examPlanId", getExamPlanId())
                .append("examStartTime", getExamStartTime())
                .append("examEndTime", getExamEndTime())
                .append("examPlace", getExamPlace())
                .append("createTime", getCreateTime())
                .append("isDel", getIsDel())
                .append("delBy", getDelBy())
                .append("delTime", getDelTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .append("examPlanVenueUserList", getExamPlanVenueUserList())
                .toString();
    }
}
