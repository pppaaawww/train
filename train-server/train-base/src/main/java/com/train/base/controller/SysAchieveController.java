package com.train.base.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.train.base.domain.SysAchieve;
import com.train.base.service.ISysAchieveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 等级管理Controller
 *
 * @author keke
 * @date 2024-05-14
 */
@RestController
@RequestMapping("/base/SysAchieve")
public class SysAchieveController extends BaseController {
    @Autowired
    private ISysAchieveService sysAchieveService;

    /**
     * 查询等级管理列表
     */
        @GetMapping("/list")
    public TableDataInfo list(SysAchieve sysAchieve) {
        startPage();
        List<SysAchieve> list = sysAchieveService.selectSysAchieveList(sysAchieve);
        return getDataTable(list);
    }

    /**
     * 导出等级管理列表
     */
        @Log(title = "等级管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysAchieve sysAchieve) {
        List<SysAchieve> list = sysAchieveService.selectSysAchieveList(sysAchieve);
        ExcelUtil<SysAchieve> util = new ExcelUtil<SysAchieve>(SysAchieve.class);
        util.exportExcel(response, list, "等级管理数据");
    }

    /**
     * 获取等级管理详细信息
     */
        @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(sysAchieveService.selectSysAchieveById(id));
    }

    /**
     * 新增等级管理
     */
        @Log(title = "等级管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysAchieve sysAchieve) {
        return toAjax(sysAchieveService.insertSysAchieve(sysAchieve));
    }

    /**
     * 修改等级管理
     */
        @Log(title = "等级管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysAchieve sysAchieve) {
        return toAjax(sysAchieveService.updateSysAchieve(sysAchieve));
    }

    /**
     * 删除等级管理
     */
        @Log(title = "等级管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(sysAchieveService.deleteSysAchieveByIds(ids));
    }
}
