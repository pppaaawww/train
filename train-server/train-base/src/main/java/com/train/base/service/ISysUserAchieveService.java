package com.train.base.service;


import com.ruoyi.common.core.domain.model.UserAchieveVo;
import com.train.base.domain.SysUserAchieve;

import java.util.List;

/**
 * 等级管理Service接口
 *
 * @author keke
 * @date 2024-05-14
 */
public interface ISysUserAchieveService {
    /**
     * 查询等级管理
     *
     * @param id 等级管理主键
     * @return 等级管理
     */
    public SysUserAchieve selectSysUserAchieveById(Long id);

    /**
     * 查询等级管理列表
     *
     * @param sysUserAchieve 等级管理
     * @return 等级管理集合
     */
    public List<SysUserAchieve> selectSysUserAchieveList(SysUserAchieve sysUserAchieve);

    /**
     * 新增等级管理
     *
     * @param sysUserAchieve 等级管理
     * @return 结果
     */
    public int insertSysUserAchieve(SysUserAchieve sysUserAchieve);

    /**
     * 修改等级管理
     *
     * @param sysUserAchieve 等级管理
     * @return 结果
     */
    public int updateSysUserAchieve(SysUserAchieve sysUserAchieve);

    /**
     * 批量删除等级管理
     *
     * @param ids 需要删除的等级管理主键集合
     * @return 结果
     */
    public int deleteSysUserAchieveByIds(Long[] ids);

    /**
     * 删除等级管理信息
     *
     * @param id 等级管理主键
     * @return 结果
     */
    public int deleteSysUserAchieveById(Long id);

    /**
     * 根据成就的ids查询是否有关联的用户
     * @param ids 成就的ids
     * @return
     */
    int countByAchieveIds(Long[] ids);

    /**
     * 根据用户的id查询用户的勋章列表
     * @param userId 用户的id
     * @return  勋章信息列表
     */
    List<UserAchieveVo> listAchieveByUserId(Long userId);

    /**
     * 根据用户的ids查询用户成就
     * @param userIds 用户的ids
     * @return
     */
    List<UserAchieveVo> listAchieveByUserIds(List<Long> userIds);
}
