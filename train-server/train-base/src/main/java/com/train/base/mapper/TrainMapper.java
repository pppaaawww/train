package com.train.base.mapper;

import com.train.base.domain.TrainRecord;
import com.train.base.domain.TrainTestPaper;
import com.train.base.entity.bo.CommonTemplatesVo;
import com.train.base.entity.bo.StatisticalParam;
import com.train.base.entity.bo.TrainNum;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;
import java.util.List;

/**
 * 训练接口
 */
public interface TrainMapper {
    /**
     * 查询训练记录
     *
     * @param id 训练记录主键
     * @return 训练记录
     */
    public TrainRecord selectTrainRecordById(Long id);

    /**
     * 查询训练记录列表
     *
     * @param trainRecord 训练记录
     * @return 训练记录集合
     */
    public List<TrainRecord> selectTrainRecordList(TrainRecord trainRecord);

    /**
     * 新增训练记录
     *
     * @param trainRecord 训练记录
     * @return 结果
     */
    public int insertTrainRecord(TrainRecord trainRecord);

    /**
     * 批量新增
     *
     * @param trainRecord
     * @return
     */
    public int batchTrainRecord(List<TrainRecord> trainRecord);

    /**
     * 修改训练记录
     *
     * @param trainRecord 训练记录
     * @return 结果
     */
    public int updateTrainRecord(TrainRecord trainRecord);

    /**
     * 删除训练记录
     *
     * @param id 训练记录主键
     * @return 结果
     */
    public int deleteTrainRecordById(Long id);

    /**
     * 批量删除训练记录
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTrainRecordByIds(Long[] ids);


    /**
     * 查询模拟试卷
     *
     * @param id 模拟试卷主键
     * @return 模拟试卷
     */
    public TrainTestPaper selectTrainTestPaperById(Long id);

    /**
     * 查询模拟试卷列表
     *
     * @param trainTestPaper 模拟试卷
     * @return 模拟试卷集合
     */
    public List<TrainTestPaper> selectTrainTestPaperList(TrainTestPaper trainTestPaper);

    /**
     * 新增模拟试卷
     *
     * @param trainTestPaper 模拟试卷
     * @return 结果
     */
    public int insertTrainTestPaper(TrainTestPaper trainTestPaper);

    /**
     * 修改模拟试卷
     *
     * @param trainTestPaper 模拟试卷
     * @return 结果
     */
    public int updateTrainTestPaper(TrainTestPaper trainTestPaper);

    /**
     * 删除模拟试卷
     *
     * @param id 模拟试卷主键
     * @return 结果
     */
    public int deleteTrainTestPaperById(Long id);

    /**
     * 批量删除模拟试卷
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTrainTestPaperByIds(Long[] ids);

    /**
     * 根据试卷获取记录
     *
     * @param paperId
     * @return
     */
    List<TrainRecord> selectTrainRecordByPaperId(Long paperId);

    /**
     * 获取某个用户的专项训练记录
     *
     * @param userId
     * @param questionIds
     * @return
     */
    List<TrainRecord> selectSpecialRecord(@Param(value = "userId") Long userId, @Param(value = "questionIds") List<Long> questionIds);

    /**
     * 获取最近一次的训练题目
     *
     * @param userId
     * @param questionIds
     * @return
     */
    List<TrainRecord> selectLastSpecialRecord(@Param(value = "userId") Long userId, @Param(value = "questionIds") List<Long> questionIds);

    /**
     * @param postId
     * @param postLevelCode
     * @param userId
     * @return
     */
    List<TrainRecord> listSepcialTrainByPost(@Param(value = "postId") Long postId, @Param(value = "postLevelCode") String postLevelCode, @Param(value = "userId") Long userId);

    /**
     * 获取某个人某个月份的专项训练记录
     *
     * @param userId
     * @param month
     * @return
     */
    List<TrainRecord> selectSpecialRecordByDate(@Param(value = "userId") Long userId, @Param(value = "month") LocalDate month);

    /**
     * 获取某个人某个月份模拟考试的记录
     *
     * @param userId
     * @param month
     * @return
     */
    List<TrainTestPaper> listUserTrainPaperByDate(@Param(value = "userId") Long userId, @Param(value = "month") LocalDate month);

    List<TrainNum> getTrainNumByMonth(@Param(value = "userId") Long userId, @Param(value = "startDate") LocalDate startDate,@Param(value = "endDate")LocalDate endDate);

    List<TrainRecord> selectSpecialRecordByUser(Long userId);

    /**
     * 获取一段时间训练题目数量及错题数量
     * @param statisticalParam  查询条件
     * @return  符合条件的数据
     */
    List<TrainRecord> selectTrainRecordByTime(StatisticalParam statisticalParam);

    /**
     * 查询常用的模板id和模板数量
     * @return
     */
    List<CommonTemplatesVo> selectCountAndModuleId();

    /**
     * 根据试卷模板的ids查询训练试卷
     * @param moduleIds 试卷模板的ids
     * @return 训练试卷数据
     */
    List<TrainTestPaper> selectTrainPaperByModuleIds(@Param("moduleIds") List<Long> moduleIds);

    /**
     * 获取一段时间内的训练记录
     * @param userId
     * @param startDate
     * @param endDate
     * @return
     */
    List<TrainRecord> selectSpecialRecordByCondition(@Param(value = "userId") Long userId,
                                                     @Param(value = "startDate") LocalDate startDate,
                                                     @Param(value = "endDate") LocalDate endDate);
}
