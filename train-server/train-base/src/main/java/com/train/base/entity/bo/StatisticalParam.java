package com.train.base.entity.bo;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author keke
 * @description  统计分析查询参数
 * @date 2024/4/20 10:13
 */
@Setter
@Getter
public class StatisticalParam {

    /**
     * 用户的ids
     */
    private List<Long> userIds;

    /**
     * 开始时间
     */
    private LocalDateTime startTime;
    /**
     * 结束时间
     */
    private LocalDateTime endTime;
}
