package com.train.base.enums;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author baiyang
 * 试卷类型
 */
public enum TestPaperTypeEnums {
    RANDOM(0L,"随机卷"),
    STANDARD(1L,"标准卷");
    private Long code;
    private String value;
    private TestPaperTypeEnums(Long code, String value){
        this.code = code;
        this.value = value;
    }

    public Long getCode(){
        return code;
    };

    public String getValue(){
        return value;
    }

    private static Map<Long,String> map = new HashMap<>();

    static {
        for (TestPaperTypeEnums e : TestPaperTypeEnums.values()) {
            map.put(e.getCode(),e.getValue());
        }
    }

    public static String getValueByCode(Integer code){
        return map.get(code);
    }

    public static Collection<Long> getCodes(){
        return map.keySet();
    }
}
