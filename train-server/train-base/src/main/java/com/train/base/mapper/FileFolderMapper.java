package com.train.base.mapper;


import com.train.base.domain.FileFolder;

import java.util.List;

/**
 * 资源文件夹Mapper接口
 *
 * @author keke
 * @date 2024-04-07
 */
public interface FileFolderMapper {
    /**
     * 查询资源文件夹
     *
     * @param id 资源文件夹主键
     * @return 资源文件夹
     */
    public FileFolder selectFileFolderById(Long id);

    /**
     * 查询资源文件夹列表
     *
     * @param fileFolder 资源文件夹
     * @return 资源文件夹集合
     */
    public List<FileFolder> selectFileFolderList(FileFolder fileFolder);

    /**
     * 新增资源文件夹
     *
     * @param fileFolder 资源文件夹
     * @return 结果
     */
    public int insertFileFolder(FileFolder fileFolder);

    /**
     * 修改资源文件夹
     *
     * @param fileFolder 资源文件夹
     * @return 结果
     */
    public int updateFileFolder(FileFolder fileFolder);

    /**
     * 删除资源文件夹
     *
     * @param id 资源文件夹主键
     * @return 结果
     */
    public int deleteFileFolderById(Long id);

    /**
     * 批量删除资源文件夹
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFileFolderByIds(Long[] ids);
}
