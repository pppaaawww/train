package com.train.base.entity.bo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 试卷回答
 */
@Setter
@Getter
public class PaperAnswer {
    /**
     * 试卷id
     */
    private Long paperId;


    private List<Answer> answers;

    @Getter
    @Setter
    public static class Answer{
        /**
         * 试卷记录id
         */
        private Long recordId;
        /**
         * 问题id
         */
        private Long questionId;
        /**
         * 答题内容
         */
        private String content;

        private Long result;
        /**
         * 获取分数
         */
        private Double gainGrade;
    }

}
