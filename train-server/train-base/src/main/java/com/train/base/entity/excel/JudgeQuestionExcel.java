package com.train.base.entity.excel;

import com.train.base.enums.QuestionTypeEnums;
import lombok.Getter;
import lombok.Setter;

/**
 * 判断题
 * @author baiyang
 */
@Getter
@Setter
public class JudgeQuestionExcel extends BaseQuestionExcel{
}
