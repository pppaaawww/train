package com.train.base.entity.excel;


import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PostSubjectKnowledgePointsExcel {
    @ExcelProperty("岗位")
    private String post;
    @ExcelProperty("科目")
    private String subject;
    @ExcelProperty("知识点")
    private String know;
}
