package com.train.base.controller;

import com.alibaba.excel.support.cglib.core.Local;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.train.base.entity.chart.LineChartData;
import com.train.base.entity.chart.SunriseChartData;
import com.train.base.entity.bo.UserRankingParam;
import com.train.base.service.ITrainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * 统计分析
 */
@RestController
@RequestMapping(value = "analysis")
public class AnalysisController extends BaseController {
    @Autowired
    private ITrainService iTrainService;

    /**
     * 统计分析界面
     * 各科目-知识点训练数量占比
     * 岗位-科目-知识点 的训练数量及错误数量占比
     * 前端采用旭日图的形式进行实现
     * @return
     */
    @GetMapping(value = "getAnalysisData")
    public AjaxResult getAnalysisData(@RequestParam(required = false) Long userId,
                                      @RequestParam(required = false) LocalDate startDate,
                                      @RequestParam(required = false) LocalDate endDate){
        Map<String, List<SunriseChartData>> result =  iTrainService.getSunriseChartData(userId,startDate,endDate);
        return AjaxResult.success(result);
    }

    /**
     * 统计分析界面：
     * 一段时间训练题目数量及错题数量
     */
    @GetMapping("getTrainLineChart")
    public AjaxResult getTrainLineChart(@RequestParam(required = false) Long userId,
                                     @RequestParam(required = false) LocalDate startDate,
                                     @RequestParam(required = false) LocalDate endDate) {
        LineChartData<LocalDate,Integer> result = iTrainService.getTrainLineChart(userId,startDate,endDate);
        return AjaxResult.success(result);
    }
}
