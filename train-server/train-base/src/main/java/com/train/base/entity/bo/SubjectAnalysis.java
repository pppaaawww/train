package com.train.base.entity.bo;

import lombok.Getter;
import lombok.Setter;

/**
 * 雷达图 科目分析
 */
@Getter
@Setter
public class SubjectAnalysis {
    /**
     * 科目id
     */
    private Long subjectId;
    /**
     * 科目名称
     */
    private String subjectName;
    /**
     * 训练总数量
     */
    private Integer num;
    /**
     * 正确数量
     */
    private Integer rightNum;
}
