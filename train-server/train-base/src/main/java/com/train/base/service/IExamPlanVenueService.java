package com.train.base.service;

import com.train.base.domain.ExamPlanVenue;

import java.time.LocalDate;
import java.util.List;

/**
 * 考试计划场次Service接口
 *
 * @author keke
 * @date 2024-03-29
 */
public interface IExamPlanVenueService {
    /**
     * 查询考试计划场次
     *
     * @param id 考试计划场次主键
     * @return 考试计划场次
     */
    public ExamPlanVenue selectExamPlanVenueById(Long id);

    /**
     * 查询考试计划场次
     *
     * @param planId 考试计划
     * @return 考试计划场次
     */
    public List<ExamPlanVenue> selectExamPlanVenueByPlanId(Long planId);

    /**
     * 查询考试计划场次列表
     *
     * @param examPlanVenue 考试计划场次
     * @return 考试计划场次集合
     */
    public List<ExamPlanVenue> selectExamPlanVenueList(ExamPlanVenue examPlanVenue);

    /**
     * 新增考试计划场次
     *
     * @param examPlanVenue 考试计划场次
     * @return 结果
     */
    public int insertExamPlanVenue(ExamPlanVenue examPlanVenue);

    /**
     * 修改考试计划场次
     *
     * @param examPlanVenue 考试计划场次
     * @return 结果
     */
    public int updateExamPlanVenue(ExamPlanVenue examPlanVenue);

    /**
     * 批量删除考试计划场次
     *
     * @param ids 需要删除的考试计划场次主键集合
     * @return 结果
     */
    public int deleteExamPlanVenueByIds(Long[] ids);

    /**
     * 删除考试计划场次信息
     *
     * @param id 考试计划场次主键
     * @return 结果
     */
    public int deleteExamPlanVenueById(Long id);

    /**
     * 获取某个计划下的考试人员
     *
     * @param id
     * @return
     */
    List<Long> selectUserIdsByPlanId(Long id);


    List<ExamPlanVenue> selectExamPlanVenueDate(LocalDate month);

    List<ExamPlanVenue> selectExamPlanVenueByPlanIds(List<Long> planIds);
}
