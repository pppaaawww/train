package com.train.base.utils;

import net.sourceforge.pinyin4j.PinyinHelper;

/**
 * 拼音工具类
 * @author baiyang
 */
public class PingYinUtil {
    /**
     * 获取首字母（大写）
     * @param chinese
     * @return
     */
    public static String getFirstLetter(String chinese) {
        StringBuilder sb = new StringBuilder();
        for (char c : chinese.toCharArray()) {
            if((c>='a' && c<='z') || (c>='A' && c<='Z') || (c>='0' && c<='9')){
                sb.append(c);
                continue;
            }
            String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(c);
            if (pinyinArray != null) {
                sb.append(pinyinArray[0].charAt(0));

            }
        }
        return sb.toString().toUpperCase();
    }
}
