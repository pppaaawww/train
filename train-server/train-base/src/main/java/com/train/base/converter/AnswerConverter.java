package com.train.base.converter;

import com.train.base.domain.ExamRecord;
import com.train.base.domain.TrainRecord;
import com.train.base.entity.bo.PaperAnswer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.TargetType;

@Mapper(componentModel = "Spring")
public abstract class AnswerConverter {

    @Mapping(target = "id",source = "recordId")
    public abstract TrainRecord answerToTrainRecord(PaperAnswer.Answer answer);

    @Mapping(target = "id",source = "recordId")
    public abstract ExamRecord answerToExamRecord(PaperAnswer.Answer answer);

}
