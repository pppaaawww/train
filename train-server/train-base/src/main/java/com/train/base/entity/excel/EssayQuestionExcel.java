package com.train.base.entity.excel;

import com.train.base.enums.QuestionTypeEnums;
import lombok.Getter;
import lombok.Setter;

/**
 * 问答题，论述题
 */
@Getter
@Setter
public class EssayQuestionExcel extends BaseQuestionExcel{
}
