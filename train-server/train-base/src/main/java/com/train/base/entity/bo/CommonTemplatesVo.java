package com.train.base.entity.bo;

import lombok.Getter;
import lombok.Setter;

/**
 * @author keke
 * @description 常用的模板数量
 * @date 2024/4/22 20:55
 */

@Getter
@Setter
public class CommonTemplatesVo {

    /**
     * 使用的次数
     */
    private Integer countNum;

    /**
     * 模板id
     */
    private Long moduleId;

    /**
     * 模板名称
     */
    private String moduleName;

    /**
     * 错题数量
     */
    private Integer errorNum;

    /**
     * 错题率
     */
    private Double errorRate;

}
