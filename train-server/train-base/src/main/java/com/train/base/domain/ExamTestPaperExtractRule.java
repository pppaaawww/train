package com.train.base.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 试卷试题抽取规则对象 exam_test_paper_extract_rule
 * 
 * @author keke
 * @date 2024-03-28
 */
@Getter
@Setter
public class ExamTestPaperExtractRule extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 试卷id */
    @Excel(name = "试卷id")
    private Long examTestPaperModuleId;

    /** 抽取试题的类型（1-判断题 2-填空题  4-论述题 5-选择题） */
    @Excel(name = "抽取试题的类型", readConverterExp = "1=-判断题,2=-填空题,4=-论述题,5=-选择题")
    private Long questionType;

    /** 抽取的试题每个的分数 */
    @Excel(name = "抽取的试题每个的分数")
    private Double grade;

    /** 抽取数量 */
    @Excel(name = "抽取数量")
    private Long number;

    /** 抽取试题适合的部门id */
    @Excel(name = "抽取试题适合的部门id")
    private Long deptId;

    /** 抽取试题适合的大纲：来源于数据字典 */
    @Excel(name = "抽取试题适合的大纲：来源于数据字典")
    private String syllabusCode;

    /** 抽取试题适合的岗位id */
    @Excel(name = "抽取试题适合的岗位id")
    private Long postId;

    /** 抽取试题时候的能力级别code，来源数据字典 */
    @Excel(name = "抽取试题时候的能力级别code，来源数据字典")
    private String postLevelCode;

    /** 抽取试题的科目id */
    @Excel(name = "抽取试题的科目id")
    private Long subjectId;

    /** 抽取试题的知识点id */
    @Excel(name = "抽取试题的知识点id")
    private Long knowledgePointId;

    /** 是否删除：0-否 1-是 */
    @Excel(name = "是否删除：0-否 1-是")
    private Long isDel;

    /** 删除者 */
    @Excel(name = "删除者")
    private String delBy;

    /** 删除 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除", width = 30, dateFormat = "yyyy-MM-dd")
    private Date delTime;

}
