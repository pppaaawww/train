package com.train.base.service;

import com.alibaba.excel.read.listener.ReadListener;
import com.train.base.domain.ExamQuestionBank;
import com.train.base.domain.ExamQuestionBankOption;
import com.train.base.domain.ExamTestPaperExtractRule;

import java.util.List;
import java.util.Map;

/**
 * 题库管理Service接口
 *
 * @author ruoyi
 * @date 2024-03-26
 */
public interface IExamQuestionBankService {
    /**
     * 查询题库管理
     *
     * @param id 题库管理主键
     * @return 题库管理
     */
    public ExamQuestionBank selectExamQuestionBankById(Long id);

    /**
     * 查询题库管理列表
     *
     * @param examQuestionBank 题库管理
     * @return 题库管理集合
     */
    public List<ExamQuestionBank> selectExamQuestionBankList(ExamQuestionBank examQuestionBank);

    /**
     * 新增题库管理
     *
     * @param examQuestionBank 题库管理
     * @return 结果
     */
    public int insertExamQuestionBank(ExamQuestionBank examQuestionBank);

    /**
     * 修改题库管理
     *
     * @param examQuestionBank 题库管理
     * @return 结果
     */
    public int updateExamQuestionBank(ExamQuestionBank examQuestionBank);

    /**
     * 批量删除题库管理
     *
     * @param ids 需要删除的题库管理主键集合
     * @return 结果
     */
    public int deleteExamQuestionBankByIds(Long[] ids);

    /**
     * 删除题库管理信息
     *
     * @param id 题库管理主键
     * @return 结果
     */
    public int deleteExamQuestionBankById(Long id);

    /**
     * 获取判断题的excel读取监听器
     *
     * @return
     */
    ReadListener<Map<Integer, String>> getJudgeQuestionReadListener();

    /**
     * 获取论述题的sheet读取监听器
     *
     * @return
     */
    ReadListener<Map<Integer, String>> getEssayQuestionReadListener();

    /**
     * 选择题的sheet读取监听器
     *
     * @return
     */
    ReadListener<Map<Integer, String>> getChoiceQuestionReadListener();

    /**
     * 填空题sheet的读取监听器
     *
     * @return
     */
    ReadListener<Map<Integer, String>> getFillBlankQuestionReadListener();

    /**
     * 根据规则随机取题库
     *
     * @param rule
     * @return
     */
    List<ExamQuestionBank> selecRandByRule(ExamTestPaperExtractRule rule);

    /**
     * 根据id集合获取
     *
     * @param bankIds
     * @return
     */
    List<ExamQuestionBank> selectExamQuestionBankByIds(List<Long> bankIds);

    List<ExamQuestionBank> listByPost(Long postId, String postLevelCode);

    /**
     * \根据题目id获取可选选项
     *
     * @param id
     * @return
     */
    List<ExamQuestionBankOption> listOptionsByBankId(Long id);
}
