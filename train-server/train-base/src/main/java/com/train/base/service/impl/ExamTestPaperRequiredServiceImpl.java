package com.train.base.service.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.train.base.domain.ExamTestPaperRequired;
import com.train.base.mapper.ExamTestPaperRequiredMapper;
import com.train.base.service.IExamTestPaperRequiredService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 试卷必选题目Service业务层处理
 * 
 * @author keke
 * @date 2024-03-28
 */
@Service
public class ExamTestPaperRequiredServiceImpl implements IExamTestPaperRequiredService
{
    @Autowired
    private ExamTestPaperRequiredMapper examTestPaperRequiredMapper;

    /**
     * 查询试卷必选题目
     * 
     * @param id 试卷必选题目主键
     * @return 试卷必选题目
     */
    @Override
    public ExamTestPaperRequired selectExamTestPaperRequiredById(Long id)
    {
        return examTestPaperRequiredMapper.selectExamTestPaperRequiredById(id);
    }

    /**
     * 查询试卷必选题目列表
     * 
     * @param examTestPaperRequired 试卷必选题目
     * @return 试卷必选题目
     */
    @Override
    public List<ExamTestPaperRequired> selectExamTestPaperRequiredList(ExamTestPaperRequired examTestPaperRequired)
    {
        return examTestPaperRequiredMapper.selectExamTestPaperRequiredList(examTestPaperRequired);
    }

    /**
     * 新增试卷必选题目
     * 
     * @param examTestPaperRequired 试卷必选题目
     * @return 结果
     */
    @Override
    public int insertExamTestPaperRequired(ExamTestPaperRequired examTestPaperRequired)
    {
        examTestPaperRequired.setCreateTime(LocalDateTime.now());
        return examTestPaperRequiredMapper.insertExamTestPaperRequired(examTestPaperRequired);
    }

    /**
     * 修改试卷必选题目
     * 
     * @param examTestPaperRequired 试卷必选题目
     * @return 结果
     */
    @Override
    public int updateExamTestPaperRequired(ExamTestPaperRequired examTestPaperRequired)
    {
        examTestPaperRequired.setUpdateTime(DateUtils.getNowDate());
        return examTestPaperRequiredMapper.updateExamTestPaperRequired(examTestPaperRequired);
    }

    /**
     * 批量删除试卷必选题目
     * 
     * @param ids 需要删除的试卷必选题目主键
     * @return 结果
     */
    @Override
    public int deleteExamTestPaperRequiredByIds(Long[] ids)
    {
        return examTestPaperRequiredMapper.deleteExamTestPaperRequiredByIds(ids);
    }

    /**
     * 删除试卷必选题目信息
     * 
     * @param id 试卷必选题目主键
     * @return 结果
     */
    @Override
    public int deleteExamTestPaperRequiredById(Long id)
    {
        return examTestPaperRequiredMapper.deleteExamTestPaperRequiredById(id);
    }
}
