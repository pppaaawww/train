package com.train.base.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 试卷必选题目对象 exam_test_paper_required
 *
 * @author keke
 * @date 2024-03-28
 */
@Getter
@Setter
public class ExamTestPaperRequired extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 试卷id */
    @Excel(name = "试卷id")
    private Long examTestPaperModuleId;

    /** 题目id */
    @Excel(name = "题目id")
    private Long questionId;

    /** 该考题分数 */
    @Excel(name = "该考题分数")
    private Double grade;

    /** 是否删除：0-否 1-是 */
    @Excel(name = "是否删除：0-否 1-是")
    private Long isDel;

    /** 删除者 */
    @Excel(name = "删除者")
    private String delBy;

    // =============================== 新增 ==============================================

    /** 考题编码 */
    @Excel(name = "考题编码")
    private String code;

    /** 该考题适合部门id */
    @Excel(name = "该考题适合部门id")
    private Long deptId;

    /** 大纲：来源于数据字典 */
    @Excel(name = "大纲：来源于数据字典")
    private String syllabusCode;

    /** 该考题适合的岗位id */
    @Excel(name = "该考题适合的岗位id")
    private Long postId;

    /**
     * 科目id
     */
    private String subjectName;

    /**
     * 知识点名称
     */
    private String knowledgePointsName;

    /** 考题类型：1-判断题 2-填空题 3-问答题 4-论述题 5-选择题 */
    @Excel(name = "考题类型：1-判断题 2-填空题 3-问答题 4-论述题 5-选择题")
    private Long questionType;

    /** 题干内容 */
    @Excel(name = "题干内容")
    private String content;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getSyllabusCode() {
        return syllabusCode;
    }

    public void setSyllabusCode(String syllabusCode) {
        this.syllabusCode = syllabusCode;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getKnowledgePointsName() {
        return knowledgePointsName;
    }

    public void setKnowledgePointsName(String knowledgePointsName) {
        this.knowledgePointsName = knowledgePointsName;
    }

    public Long getQuestionType() {
        return questionType;
    }

    public void setQuestionType(Long questionType) {
        this.questionType = questionType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPostLevelCode() {
        return postLevelCode;
    }

    public void setPostLevelCode(String postLevelCode) {
        this.postLevelCode = postLevelCode;
    }

    /** 适合的能力级别，来源于数据字典 */
    @Excel(name = "适合的能力级别，来源于数据字典")
    private String postLevelCode;

    /** 删除 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除", width = 30, dateFormat = "yyyy-MM-dd")
    private Date delTime;
}
