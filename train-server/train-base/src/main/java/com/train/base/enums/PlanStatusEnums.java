package com.train.base.enums;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * 计划状态
 */
public enum PlanStatusEnums {
    DRAFT(0L,"草稿"),
    RELEASE(1L,"发布"),
    FINISH(2L,"结束");

    private Long code;
    private String value;
    private PlanStatusEnums(Long code, String value){
        this.code = code;
        this.value = value;
    }

    public Long getCode(){
        return code;
    };

    public String getValue(){
        return value;
    }

    private static Map<Long,String> map = new HashMap<>();

    static {
        for (PlanStatusEnums e : PlanStatusEnums.values()) {
            map.put(e.getCode(),e.getValue());
        }
    }

    public static String getValueByCode(Integer code){
        return map.get(code);
    }

    public static Collection<Long> getCodes(){
        return map.keySet();
    }
}
