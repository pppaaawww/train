package com.train.base.enums;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * 是否可以补考枚举
 */
public enum WhetherCanResitEnums {
    NOT_CAN(0L,"不可以"),
    CAN(1L,"可以");
    private Long code;
    private String value;
    private WhetherCanResitEnums(Long code, String value){
        this.code = code;
        this.value = value;
    }

    public Long getCode(){
        return code;
    };

    public String getValue(){
        return value;
    }

    private static Map<Long,String> map = new HashMap<>();

    static {
        for (WhetherCanResitEnums e : WhetherCanResitEnums.values()) {
            map.put(e.getCode(),e.getValue());
        }
    }

    public static String getValueByCode(Integer code){
        return map.get(code);
    }

    public static Collection<Long> getCodes(){
        return map.keySet();
    }
}
