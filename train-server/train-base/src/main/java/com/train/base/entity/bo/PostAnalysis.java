package com.train.base.entity.bo;

import lombok.Data;

import java.util.List;

/**
 * 岗位分析,适用于饼状图
 */
@Data
public class PostAnalysis {

    private List<Post> posts;
    private List<PostLevel> postLevels;
    private List<PostAbility> postAbilities;
    @Data
    public static class Post{
        private String name;
        private double value;
    }
    @Data
    public static class PostLevel{
        private String name;
        private double value;
    }
    @Data
    public static class PostAbility{
        private String name;
        private double value;
    }
}
