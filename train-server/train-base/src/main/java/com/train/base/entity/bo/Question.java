package com.train.base.entity.bo;

import com.train.base.domain.ExamQuestionBankOption;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Question {
    /**
     * 记录id
     */
    private Long recordId;
    /** 题目id */
    private Long questionId;
    /**
     * 题目类型
     */
    private Long type;
    /** 试题解析 */
    private String analysis;
    /** 该考题分数 */
    private Double grade;
    /** 题干内容 */
    private String content;
    /** 正确答案 */
    private String rightKey;
    /** 考题选项 适用于填空、选择题信息 */
    private List<ExamQuestionBankOption> examQuestionBankOptionList;
    /**
     * 回答内容
     */
    private String resultContent;
    /**
     * 结果分数
     */
    private Double gainGrade;

    /** 训练结果：0-回答错误，2-回答正确 */
    private Long result;
    /**
     * 阅卷备注
     */
    private String remark;
}
