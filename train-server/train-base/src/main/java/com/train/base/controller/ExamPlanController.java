package com.train.base.controller;

import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;

import com.alibaba.fastjson2.JSONObject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.train.base.domain.ExamPlan;
import com.train.base.service.IExamPlanService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 考试计划Controller
 *
 * @author keke
 * @date 2024-03-28
 */
@RestController
@RequestMapping("/train/ExamPlan")
public class ExamPlanController extends BaseController {
    @Autowired
    private IExamPlanService examPlanService;

    /**
     * 查询考试计划列表
     */
    @GetMapping("/list")
    public TableDataInfo list(ExamPlan examPlan) {
        startPage();
        List<ExamPlan> list = examPlanService.selectExamPlanList(examPlan);
        return getDataTable(list);
    }

    /**
     * 导出考试计划列表
     */
    @Log(title = "考试计划", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExamPlan examPlan) {
        List<ExamPlan> list = examPlanService.selectExamPlanList(examPlan);
        ExcelUtil<ExamPlan> util = new ExcelUtil<ExamPlan>(ExamPlan.class);
        util.exportExcel(response, list, "考试计划数据");
    }

    /**
     * 获取考试计划详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(examPlanService.selectExamPlanById(id));
    }

    /**
     * 新增考试计划
     */
    @Log(title = "考试计划", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ExamPlan examPlan) {
        return toAjax(examPlanService.insertExamPlan(examPlan));
    }

    /**
     * 修改考试计划
     */
    @Log(title = "考试计划", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ExamPlan examPlan) {
        return toAjax(examPlanService.updateExamPlan(examPlan));
    }

    /**
     * 修改考试计划 状态
     */
    @PutMapping("editStatus")
    public AjaxResult editStatus(@RequestBody JSONObject object) {
        return toAjax(examPlanService.updateStatus(object.getList("ids",Long.class), object.getLong("status")));
    }

    /**
     * 删除考试计划
     */
    @Log(title = "考试计划", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(examPlanService.deleteExamPlanByIds(ids));
    }
}
