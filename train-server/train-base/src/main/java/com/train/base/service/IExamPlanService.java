package com.train.base.service;

import com.train.base.domain.ExamPlan;

import java.time.LocalDate;
import java.util.List;

/**
 * 考试计划Service接口
 *
 * @author keke
 * @date 2024-03-28
 */
public interface IExamPlanService {
    /**
     * 查询考试计划
     *
     * @param id 考试计划主键
     * @return 考试计划
     */
    public ExamPlan selectExamPlanById(Long id);

    /**
     * 查询考试计划列表
     *
     * @param examPlan 考试计划
     * @return 考试计划集合
     */
    public List<ExamPlan> selectExamPlanList(ExamPlan examPlan);

    /**
     * 新增考试计划
     *
     * @param examPlan 考试计划
     * @return 结果
     */
    public int insertExamPlan(ExamPlan examPlan);

    /**
     * 修改考试计划
     *
     * @param examPlan 考试计划
     * @return 结果
     */
    public int updateExamPlan(ExamPlan examPlan);

    /**
     * 批量删除考试计划
     *
     * @param ids 需要删除的考试计划主键集合
     * @return 结果
     */
    public int deleteExamPlanByIds(Long[] ids);

    /**
     * 删除考试计划信息
     *
     * @param id 考试计划主键
     * @return 结果
     */
    public int deleteExamPlanById(Long id);

    /**
     * 根据考试计划的IDS更新计划的状态
     *
     * @param ids    考试计划的IDS
     * @param status 更新的状态
     * @return
     */
    int updateStatus(List<Long> ids, Long status);

    /**
     * 根据id集合获取计划
     *
     * @param planIds
     * @return
     */
    List<ExamPlan> selectExamPlanByIds(List<Long> planIds);

    /**
     * 获取周期计划需要发布生成试卷的
     *
     * @return
     */
    List<ExamPlan> selectNeedCreatePlan();

    /**
     * 发布计划，将计划生成试卷
     *
     * @param ids
     */
    public void release(List<Long> ids);

    /**
     * 获取某个人某个月的考试计划信息
     *
     * @param month
     * @return
     */
    List<ExamPlan> listByDate(LocalDate month);

    /**
     * 完成计划
     *
     * @param exemPlanId
     * @return
     */
    int finish(Long exemPlanId);

    /**
     * 根据创建时间查询已完成的考试计划
     * @return
     */
    List<Long> selectExamPlanOrderByCreateTime(Integer limitNum);
}
