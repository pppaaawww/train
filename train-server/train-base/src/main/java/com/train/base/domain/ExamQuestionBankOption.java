package com.train.base.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 考题选项 适用于填空、选择题对象 exam_question_bank_option
 * 
 * @author ruoyi
 * @date 2024-03-26
 */
public class ExamQuestionBankOption extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 考题id */
    @Excel(name = "考题id")
    private Long questionId;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    /** 是否删除：0-否 1-是 */
    @Excel(name = "是否删除：0-否 1-是")
    private Long isDel;

    /** 删除者 */
    @Excel(name = "删除者")
    private String delBy;

    /** 删除 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除", width = 30, dateFormat = "yyyy-MM-dd")
    private Date delTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setQuestionId(Long questionId) 
    {
        this.questionId = questionId;
    }

    public Long getQuestionId() 
    {
        return questionId;
    }
    public void setSort(Long sort)
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setIsDel(Long isDel) 
    {
        this.isDel = isDel;
    }

    public Long getIsDel() 
    {
        return isDel;
    }
    public void setDelBy(String delBy) 
    {
        this.delBy = delBy;
    }

    public String getDelBy() 
    {
        return delBy;
    }
    public void setDelTime(Date delTime) 
    {
        this.delTime = delTime;
    }

    public Date getDelTime() 
    {
        return delTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("questionId", getQuestionId())
            .append("sort", getSort())
            .append("content", getContent())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("isDel", getIsDel())
            .append("delBy", getDelBy())
            .append("delTime", getDelTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
