package com.train.base.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.train.base.domain.ExamQuestionBank;
import com.train.base.service.IExamQuestionBankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 题库管理Controller
 *
 * @author ruoyi
 * @date 2024-03-26
 */
@RestController
@RequestMapping("/base/ExamQuestionBank")
public class ExamQuestionBankController extends BaseController {
    @Autowired
    private IExamQuestionBankService examQuestionBankService;

    /**
     * 查询题库管理列表
     */
    @GetMapping("/list")
    public TableDataInfo list(ExamQuestionBank examQuestionBank) {
        startPage();
        List<ExamQuestionBank> list = examQuestionBankService.selectExamQuestionBankList(examQuestionBank);
        return getDataTable(list);
    }

    @Log(title = "题库管理", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    @Transactional
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        try (ExcelReader excelReader = EasyExcel.read(file.getInputStream()).build()) {
            ReadSheet judgeQuestionReadSheet = EasyExcel.readSheet("判断题").registerReadListener(examQuestionBankService.getJudgeQuestionReadListener()).build();
            ReadSheet essayQuestionReadSheet = EasyExcel.readSheet("论述题").registerReadListener(examQuestionBankService.getEssayQuestionReadListener()).build();
            ReadSheet choiceQuestionReadSheet = EasyExcel.readSheet("选择题").registerReadListener(examQuestionBankService.getChoiceQuestionReadListener()).build();
            ReadSheet fillBlankQuestionReadSheet = EasyExcel.readSheet("填空题").registerReadListener(examQuestionBankService.getFillBlankQuestionReadListener()).build();
            excelReader.read(judgeQuestionReadSheet,essayQuestionReadSheet,choiceQuestionReadSheet,choiceQuestionReadSheet,fillBlankQuestionReadSheet);
        }catch (Exception e){
            e.printStackTrace();
            return error(e.getMessage());
        }
        return success("导入成功");
    }

    /**
     * 导出题库管理列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExamQuestionBank examQuestionBank) {
        List<ExamQuestionBank> list = examQuestionBankService.selectExamQuestionBankList(examQuestionBank);
        ExcelUtil<ExamQuestionBank> util = new ExcelUtil<ExamQuestionBank>(ExamQuestionBank.class);
        util.exportExcel(response, list, "题库管理数据");
    }

    /**
     * 获取题库管理详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(examQuestionBankService.selectExamQuestionBankById(id));
    }

    /**
     * 新增题库管理
     */
    @Log(title = "题库管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ExamQuestionBank examQuestionBank) {
        return toAjax(examQuestionBankService.insertExamQuestionBank(examQuestionBank));
    }

    /**
     * 修改题库管理
     */
    @Log(title = "题库管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ExamQuestionBank examQuestionBank) {
        return toAjax(examQuestionBankService.updateExamQuestionBank(examQuestionBank));
    }

    /**
     * 删除题库管理
     */
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(examQuestionBankService.deleteExamQuestionBankByIds(ids));
    }
}
