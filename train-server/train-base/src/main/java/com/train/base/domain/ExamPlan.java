package com.train.base.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 考试计划对象 exam_plan
 *
 * @author keke
 * @date 2024-03-28
 */
@Getter
@Setter
public class ExamPlan extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 考试名称 */
    @Excel(name = "考试名称")
    private String name;

    /** 考试试卷模板id */
    @Excel(name = "考试试卷模板id")
    private Long examTestPaperModuleId;
    /**
     * 考试试卷模板名称
     */
    private String examTestPaperModuleName;

    /** 考试部门id */
    @Excel(name = "考试部门id")
    private Long deptId;

    /** 考试岗位id */
    @Excel(name = "考试岗位id")
    private Long postId;

    /** 考试岗位级别 */
    @Excel(name = "考试岗位级别")
    private String postLevelCode;

    /** 考试类型：0-固定 1-周期 */
    @Excel(name = "考试类型：0-固定 1-周期")
    private Long examType;

    /** 当为周期时的频次（次/天） */
    @Excel(name = "当为周期时的频次", readConverterExp = "次=/天")
    private Long frequency;

    /** 考试计划开始时间 */
    @Excel(name = "考试计划开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private LocalDateTime examStartTime;

    /** 考试计划结束时间 */
    @Excel(name = "考试计划结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private LocalDateTime examEndTime;

    /** 是否可以补考：0-不可用 1-可以 */
    @Excel(name = "是否可以补考：0-不可用 1-可以")
    private Long whetherCanResit;

    /** 考试时长（分钟） */
    @Excel(name = "考试时长", readConverterExp = "分=钟")
    private Long examDuration;

    /** 计划状态：0-待启用；1-启用中；2-计划完成 */
    @Excel(name = "计划状态：0-待启用；1-启用中；2-计划完成")
    private Long statu;

    /** 是否删除：0-否 1-是 */
    @Excel(name = "是否删除：0-否 1-是")
    private Long isDel;

    /** 删除者 */
    @Excel(name = "删除者")
    private String delBy;

    /** 删除 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除", width = 30, dateFormat = "yyyy-MM-dd")
    private Date delTime;


    /** 考试计划关联考场信息 */
    private List<ExamPlanVenue> examPlanVenueList;

}
