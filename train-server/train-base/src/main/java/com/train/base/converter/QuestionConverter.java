package com.train.base.converter;

import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.exception.base.BaseException;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.SysPost;
import com.ruoyi.system.service.ISysDeptService;
import com.ruoyi.system.service.ISysDictDataService;
import com.ruoyi.system.service.ISysPostService;
import com.train.base.domain.ExamQuestionBank;
import com.train.base.domain.ExamSubjectKnowledgePoints;
import com.train.base.entity.excel.*;
import com.ruoyi.common.enums.DictDataTypeEnums;
import com.train.base.enums.QuestionTypeEnums;
import com.train.base.service.IExamSubjectKnowledgePointsService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Mapper(componentModel = "Spring")
public abstract class QuestionConverter {
    @Autowired
    private ISysDeptService iSysDeptService;
    @Autowired
    private ISysDictDataService iSysDictDataService;
    @Autowired
    private ISysPostService iSysPostService;
    @Autowired
    private IExamSubjectKnowledgePointsService iExamSubjectKnowledgePointsService;

    @Mapping(target = "id", expression = "java(com.ruoyi.common.utils.SnowflakeIdWorker.build().nextId())")
    public abstract JudgeQuestionExcel judgeQuestionExcelConverter(BaseQuestionExcel excel);

    @Mapping(target = "id", expression = "java(com.ruoyi.common.utils.SnowflakeIdWorker.build().nextId())")
    public abstract ChoiceQuestionExcel choiceQuestionExcelConverter(BaseQuestionExcel excel);

    public ChoiceQuestionExcel choiceQuestionExcelConverter(BaseQuestionExcel excel, Map<Integer, String> data, Map<Integer, String> head) {
        ChoiceQuestionExcel choiceQuestionExcel = choiceQuestionExcelConverter(excel);
        List<String> alternativeAnswer = new ArrayList<>();
        head.forEach((k, v) -> {
            if (v.contains("备选答案")) {
                alternativeAnswer.add(data.get(k));
            }
        });
        choiceQuestionExcel.setAlternativeAnswer(alternativeAnswer);
        return choiceQuestionExcel;
    }

    @Mapping(target = "id", expression = "java(com.ruoyi.common.utils.SnowflakeIdWorker.build().nextId())")
    public abstract EssayQuestionExcel essayQuestionExcelConverter(BaseQuestionExcel excel);

    @Mapping(target = "id", expression = "java(com.ruoyi.common.utils.SnowflakeIdWorker.build().nextId())")
    public abstract FillBlankQuestionExcel fillBlankQuestionExcelConverter(BaseQuestionExcel excel);

    public FillBlankQuestionExcel fillBlankQuestionExcelConverter(BaseQuestionExcel excel, Map<Integer, String> data, Map<Integer, String> head) {
        FillBlankQuestionExcel fillBlankQuestionExcel = fillBlankQuestionExcelConverter(excel);
        List<String> answer = new ArrayList<>();
        head.forEach((k, v) -> {
            if (v.contains("填空")) {
                answer.add(data.get(k));
            }
        });
        fillBlankQuestionExcel.setAnswer(answer);
        return fillBlankQuestionExcel;
    }

    public List<ExamQuestionBank> fillBlankQuestionExcelsToPos(List<FillBlankQuestionExcel> excels) {
        if (CollectionUtils.isEmpty(excels)) {
            return Collections.emptyList();
        }
        List<ExamQuestionBank> examQuestionBanks = new ArrayList<>(excels.size());
        for (FillBlankQuestionExcel excel : excels) {
            ExamQuestionBank bank = toExamQuestionBank(excel);
            bank.setQuestionType(QuestionTypeEnums.FillBlankQuestionExcel.getCode());
            examQuestionBanks.add(bank);
        }
        return examQuestionBanks;
    }

    public List<ExamQuestionBank> choiceQuestionExcelsToPos(List<ChoiceQuestionExcel> excels) {
        if (CollectionUtils.isEmpty(excels)) {
            return Collections.emptyList();
        }
        List<ExamQuestionBank> examQuestionBanks = new ArrayList<>(excels.size());
        for (ChoiceQuestionExcel excel : excels) {
            ExamQuestionBank bank = toExamQuestionBank(excel);
            bank.setQuestionType(QuestionTypeEnums.ChoiceQuestion.getCode());
            String rightKey = excel.getRightKey();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < rightKey.length(); i++) {
                char c = rightKey.charAt(i);
                int key = c - 'A';
                sb.append(key + ",");
            }
            sb.deleteCharAt(sb.length() - 1);
            bank.setRightKey(sb.toString());
            examQuestionBanks.add(bank);
        }
        return examQuestionBanks;
    }

    public List<ExamQuestionBank> essayQuestionExcelsToPos(List<EssayQuestionExcel> excels) {
        if (CollectionUtils.isEmpty(excels)) {
            return Collections.emptyList();
        }
        List<ExamQuestionBank> examQuestionBanks = new ArrayList<>(excels.size());
        for (EssayQuestionExcel excel : excels) {
            ExamQuestionBank bank = toExamQuestionBank(excel);
            bank.setQuestionType(QuestionTypeEnums.EssayQuestion.getCode());
            examQuestionBanks.add(bank);
        }
        return examQuestionBanks;
    }

    public List<ExamQuestionBank> judgeQuestionExcelsToPos(List<JudgeQuestionExcel> excels) {
        if (CollectionUtils.isEmpty(excels)) {
            return Collections.emptyList();
        }
        List<ExamQuestionBank> examQuestionBanks = new ArrayList<>(excels.size());
        for (JudgeQuestionExcel excel : excels) {
            ExamQuestionBank bank = toExamQuestionBank(excel);
            bank.setQuestionType(QuestionTypeEnums.JudgeQuestion.getCode());
            examQuestionBanks.add(bank);
        }
        return examQuestionBanks;
    }

    private ExamQuestionBank toExamQuestionBank(BaseQuestionExcel excel) {
        ExamQuestionBank bank = new ExamQuestionBank();
        bank.setId(excel.getId());
        bank.setCode(excel.getCode());
        SysDept sysDept = iSysDeptService.selectDeptByName(excel.getUnit());
        if (Objects.nonNull(sysDept)) {
            bank.setDeptId(sysDept.getDeptId());
        } else {
            throw new BaseException("base", excel.getCode() + ":" + excel.getUnit() + "单位信息未进行配置");
        }
        bank.setSyllabusCode(iSysDictDataService.selectDictValue(DictDataTypeEnums.SYLLABUS.getCode(), excel.getSyllabus()));
        SysPost sysPost = iSysPostService.selectPostByName(excel.getPost());
        if (Objects.nonNull(sysPost)) {
            bank.setPostId(sysPost.getPostId());
        } else {
            throw new BaseException("base", excel.getPost() + "该岗位信息未进行配置");
        }
        ExamSubjectKnowledgePoints subject = iExamSubjectKnowledgePointsService.selectSubject(sysPost.getPostId(), excel.getSubject());
        if (Objects.nonNull(subject)) {
            bank.setSubjectId(subject.getId());
        } else {
            throw new BaseException("base", sysPost.getPostName() + "岗位【" + excel.getSubject() + "】科目信息未进行配置");
        }
        ExamSubjectKnowledgePoints knowledgePoint = iExamSubjectKnowledgePointsService.selectKnowledgePoint(subject.getId(), excel.getKnowledgePoint());
        if (Objects.nonNull(knowledgePoint)) {
            bank.setKnowledgePointsId(knowledgePoint.getId());
        } else {
            throw new BaseException("base", subject.getName() + "科目下【" + excel.getKnowledgePoint() + "】知识点信息未进行配置");
        }
        bank.setContent(excel.getContent());
        bank.setRightKey(excel.getRightKey());
        bank.setPostLevelCode(iSysDictDataService.selectDictValue(DictDataTypeEnums.POST_LEVEL.getCode(), excel.getPostLevel()));
        bank.setAnalysis(excel.getAnalysis());
        bank.setCreateBy(SecurityUtils.getLoginUser().getUsername());
        bank.setIsDel(0L);
        return bank;
    }

}
