package com.train.base.entity.bo;

import com.train.base.domain.*;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

/**
 * 每日记录
 */
@Data
public class DailyRecord {
    /**
     * 具体日期
     */
    private LocalDate date;
    /**
     * 当天训练记录
     */
    public SpecialTrainRecord specialTrainRecord;
    /**
     * 当天模拟记录
     */

    private SimulationTrainRecord simulationTrainRecord;
    /**
     * 试卷记录
     */
    private ExamRecord examRecord;
    /**
     * 当天考试计划记录
     */

    private ExamPlanRecord examPlanRecord;

    /**
     * 专项训练记录
     */
    @Data
    public static class SpecialTrainRecord {
        /**
         * 训练内容
         */
        private List<TrainRecord> records;
        /**
         * 训练数量
         */
        private Integer num = 0;
        /**
         * 正确数量
         */
        private Integer rightNum = 0;
    }

    /**
     * 模拟训练记录
     */
    @Data
    public static class SimulationTrainRecord {
        private List<TrainTestPaper> papers;
    }

    /**
     * 考试记录
     */
    @Data
    public static class ExamRecord {
        private List<ExamTestPaper> papers;
    }

    /**
     * 考试计划
     */
    @Data
    public static class ExamPlanRecord {
        /**
         * 计划详情
         */
        private List<PlanDetail> details;

        /**
         * 计划详情
         */
        @Data
        public static class PlanDetail {
            /**
             * 计划
             */
            private ExamPlan examPlan;
            /**
             * 考场
             */
            private ExamPlanVenue venue;
        }
    }

}
