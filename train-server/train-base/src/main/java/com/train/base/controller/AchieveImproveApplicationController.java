package com.train.base.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.train.base.domain.AchieveImproveApplication;
import com.train.base.service.IAchieveImproveApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 成就申请Controller
 *
 * @author keke
 * @date 2024-05-14
 */
@RestController
@RequestMapping("/base/AchieveImproveApplication")
public class AchieveImproveApplicationController extends BaseController {
    @Autowired
    private IAchieveImproveApplicationService achieveImproveApplicationService;

    /**
     * 查询成就申请列表
     */
        @GetMapping("/list")
    public TableDataInfo list(AchieveImproveApplication achieveImproveApplication) {
        startPage();
        List<AchieveImproveApplication> list = achieveImproveApplicationService.selectAchieveImproveApplicationList(achieveImproveApplication);
        return getDataTable(list);
    }

    /**
     * 导出成就申请列表
     */
        @Log(title = "成就申请", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AchieveImproveApplication achieveImproveApplication) {
        List<AchieveImproveApplication> list = achieveImproveApplicationService.selectAchieveImproveApplicationList(achieveImproveApplication);
        ExcelUtil<AchieveImproveApplication> util = new ExcelUtil<AchieveImproveApplication>(AchieveImproveApplication.class);
        util.exportExcel(response, list, "成就申请数据");
    }

    /**
     * 获取成就申请详细信息
     */
        @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(achieveImproveApplicationService.selectAchieveImproveApplicationById(id));
    }

    /**
     * 新增成就申请
     */
        @Log(title = "成就申请", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AchieveImproveApplication achieveImproveApplication) {
        return toAjax(achieveImproveApplicationService.insertAchieveImproveApplication(achieveImproveApplication));
    }

    /**
     * 修改成就申请
     */
        @Log(title = "成就申请", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AchieveImproveApplication achieveImproveApplication) {
        return toAjax(achieveImproveApplicationService.updateAchieveImproveApplication(achieveImproveApplication));
    }

    /**
     * 删除成就申请
     */
        @Log(title = "成就申请", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(achieveImproveApplicationService.deleteAchieveImproveApplicationByIds(ids));
    }

    /**
     * 成就申请审核
     */
    @PutMapping("review")
    public AjaxResult review(@RequestBody AchieveImproveApplication achieveImproveApplication) {
        return toAjax(achieveImproveApplicationService.review(achieveImproveApplication));
    }
}
