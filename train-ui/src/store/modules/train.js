import {listDept} from '@/api/system/dept'
import {listPost} from '@/api/system/post'
const state = {
  postList: [],
  deptList: [],
  postMap: {},
  deptMap: {},


}

const mutations = {
  SET_POSTS: (state, posts) => {
    state.postList = posts
    let map = {}
    posts.forEach(post => {
      map[post.postId] = post.postName
    })
    state.postMap = map
  },
  SET_DEPTS:(state, depts) => {
    state.deptList = depts
    let map = {}
    depts.forEach(dept => {
      map[dept.deptId] = dept.deptName
    })
    state.deptMap = map
  }
}

const actions = {
  init({commit}){
    listDept().then(res => {
      commit('SET_DEPTS', res.data)
    })
    listPost().then(res => {
      commit('SET_POSTS', res.rows)
    })
  }

}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
