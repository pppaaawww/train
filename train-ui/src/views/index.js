import userAvatar from "./system/user/profile/userAvatar";
import userInfo from "./system/user/profile/userInfo";
import resetPwd from "./system/user/profile/resetPwd";
import {getPostDetail, getUserProfile, listDaily} from "@/api/system/user";
import {getTrainNumByMonth, getTrainSubjectAnalysis} from "@/api/train/Analysis.js";
import {listCurrentUser} from "@/api/train/base/ExamTestPaper";
import {postListCurrentUser} from "@/api/train/base/PostImproveApplication";
import {specialTrainProgress} from "@/api/train/base/Train.js";
import * as echarts from 'echarts';
import {parseTime} from "@/utils/common";
import {Post_Apply_State} from "@/views/train/Enums";
import {listAchieveByUserId} from "@/api/system/SysAchieve";
export default {
  name: 'Index',
  components: {userAvatar, userInfo, resetPwd},
  dicts: ['post_level', 'post_ability'],
  data() {
    return {
      user: {},
      roleGroup: {},
      postGroup: {},
      activeTab: "0",
      width: "98%",
      height: "350px",
      activeName: null,
      postDetails: [],
      ability: null,
      level: null,
      specialProgress: [],
      lineChart: {},
      lineChartData: {},
      abilityAnalysisRadarChart: null,
      abilityAnalysisRadarData: null,
      // 成绩单
      gradeRecord: [],
      gradeParams: {
        pageOn: 0,
        pageSize: 10,
        orderBy: "create_time"
      },
      postRecord: [],
      postParams: {
        pageOn: 0,
        pageSize: 10,
        orderBy: "create_time"
      },
      postLevel: {
        0: 123,
        1: SVGDefsElement
      },
      postAbility: {},
      selectedDate: new Date(),
      dailyData: [],
      // 成就列表
      achieveList: [],
      options: {
        Post_Apply_State: Post_Apply_State
      }
    }
  },
  created() {
    this.getUser();
    this.getPostDetail();
    this.getTrainNumByMonth();
    this.getTrainSubjectAnalysis();
    this.getAchievement()
  },
  mounted() {
    this.$nextTick(() => {

    })
  },
  watch: {
    selectedDate: {
      handler(newDate,oldDate) {
        let pattern = '{y}-{m}'
        // 如果不是同月的,那么需要获取该月信息
        if(parseTime(oldDate,pattern) !== parseTime(newDate,pattern)){
          this.listDaily()
        }
      },
      immediate: true
    },
    activeName() {
      this.getSpecialTrainProgress();
    }
  },
  methods: {
    // 获取成就
    getAchievement(){
      listAchieveByUserId().then(res => {
        this.achieveList = res.userAchieveVo || []
      })
    },
    /**
     * 获取底部数量折线图数量
     */
    getTrainNumByMonth() {
      getTrainNumByMonth().then(res => {
        this.lineChartData = res.data
        this.initLine();
      })
    },
    initLine() {
      this.lineChart = echarts.init(this.$refs.TainNumlineChart);
      var xAxisData = [];
      var seriesData = [];
      this.lineChartData.forEach(e => {
        xAxisData.push(e.date);
        seriesData.push(e.num);
      })
      var option = {
        title: {
          text: '训练情况'
        },
        label: {
          show: true,
          position: 'top',
          formatter: function ({value}) {
            return value||''
          }
        },
        tooltip: {
          trigger: 'axis'
        },
        legend: {
          data: ['专项训练数量']
        },
        grid: {
          left: '3%',
          right: '4%',
          bottom: '3%',
          containLabel: true
        },
        toolbox: {
          feature: {
            saveAsImage: {}
          }
        },
        xAxis: {
          type: 'category',
          boundaryGap: false,
          data: xAxisData,
        },
        yAxis: {
          type: 'value'
        },
        series: [
          {
            name: '专项训练数量',
            type: 'line',
            stack: 'Total',
            data: seriesData,
          },
        ]
      };
      this.lineChart.setOption(option);
    },

    onDictReady(dict) {
      dict.type.post_level.forEach(element => {
        this.postLevel[element.value] = element.label;
      });
      dict.type.post_ability.forEach(element => {
        this.postAbility[element.value] = element.label;
      });
    },
    postLoad() {
      this.postParams = this.postParams.pageOn + 1
      postListCurrentUser(this.postParams).then(res => {
        this.postRecord = res.rows;
      })
    },
    load() {
      this.gradeParams = this.gradeParams.pageOn + 1
      listCurrentUser(this.gradeParams).then(res => {
        this.gradeRecord = res.rows;
      })
    },
    /**
     * 获取雷达图数据
     */
    getTrainSubjectAnalysis() {
      getTrainSubjectAnalysis().then(res => {
        this.abilityAnalysisRadarData = res.data
        this.initRadarChart()
      })
    },

    /**
     * 日历数据获取接口
     */
    listDaily() {
      listDaily(
        parseTime(this.selectedDate, '{y}-{m}-{d}')
      ).then(res => {
        this.dailyData = res.data
      })
    },
    /**
     * 初始化能力雷达图
     */
    initRadarChart() {
      this.abilityAnalysisRadarChart = echarts.init(this.$refs.abilityAnalysisRadarChart);
      var indicator = [];
      var seriesDataAll = [], seriesDataRight = [];
      let max = 0;
      this.abilityAnalysisRadarData.forEach(e=>{
        if(e.rightNum > max){
          max = e.num;
        }
      })
      this.abilityAnalysisRadarData.forEach(e => {
        indicator.push({name: e.subjectName, max: max})
        seriesDataAll.push(e.num)
        seriesDataRight.push(e.rightNum)
      })
      var option = {
        title: {
          text: '能力分析'
        },
        legend: {
          data: ['训练总数量', '正确总数量'],
          right: '10%'
        },
        radar: {
          // shape: 'circle',
          indicator: indicator
        },
        tooltip: {
          trigger: 'item'
        },
        series: [
          {
            name: 'Budget vs spending',
            type: 'radar',
            data: [
              {
                value: seriesDataAll,
                name: '训练总数量'
              },
              {
                value: seriesDataRight,
                name: '正确总数量'
              }
            ]
          }
        ]
      };
      this.abilityAnalysisRadarChart.setOption(option);
    },
    getPostDetail() {
      getPostDetail().then(response => {
        this.postDetails = response.data;
        this.activeName = this.postDetails[0].postId
      })
    },
    getSpecialTrainProgress() {
      var param = {postId: this.activeName, postLevelCode: 0};
      specialTrainProgress(param).then(res => {
        this.specialProgress = res.data;
      })
    },
    getUser() {
      getUserProfile().then(response => {
        this.user = response.data;
        this.roleGroup = response.roleGroup;
        this.postGroup = response.postGroup;
      });
    },

  },
}
