import {listExamTestPaper, receive} from "@/api/train/base/ExamTestPaper";
import {EXAM_TEST_PAPER_STATE} from '@/views/train/Enums'
import {mapGetters} from "vuex";
import {listExamPlan} from "@/api/train/base/ExamPlan";
import {listExamTestPaperModule} from "@/api/train/base/ExamTestPaperModule";
import {listUser} from "@/api/system/user";
import mixins from "@/views/train/mixins";
export default {
  name: "ExamTestPaper",
  dicts: ['post_level'],
  mixins: [mixins],
  computed:{
    ...mapGetters(['currentUserId'])
  },
  data() {
    return {
      // 遮罩层
      loading: true,
      // 选中数组
      ids: [],
      // 非单个禁用
      single: true,
      // 非多个禁用
      multiple: true,
      // 显示搜索条件
      showSearch: true,
      // 总条数
      total: 0,
      // 试卷管理表格数据
      ExamTestPaperList: [],
      // 弹出层标题
      title: "",
      // 是否显示弹出层
      open: false,
      // 查询参数
      queryParams: {
        pageNum: 1,
        pageSize: 10,
        state: null,
        exemPlanId: null,
        name: null,
        testPaperModuleId: null,
        userId: null,
        questionNum: null,
        rightAnswerNum: null,
        notDoneNum: null,
        vagueNum: null,
        submitTime: null,
        deptId: null,
        postId: null,
        postLevelCode: null,
        grade: null,
        gainGrade: null,
      },
      options:{
        EXAM_TEST_PAPER_STATE:EXAM_TEST_PAPER_STATE,
        userList: [],
        examPlanList: [],
        testPaperModuleList: [],
      }
    };
  },
  methods: {
    // 领取试题
    receive(row){
      receive({id:row.id}).then(e=>{
        this.$message.success('领取成功')
        this.getList()
      })
    },
    // 答题
    handleReply(row){
      this.$router.push(`/train2/ExamTestPaperDetail/${row.id}`)
    },
    // 判卷
    handleGrade(row){
      this.$router.push(`/train2/ExamTestPaperDetail/${row.id}`)
    },
    handleView(row){
      this.$router.push(`/train2/ExamTestPaperDetail/${row.id}`)
    },
    /** 查询试卷管理列表 */
    getList() {
      this.loading = true;
      listExamTestPaper(this.doSort(this.queryParams,'createTime' )).then(response => {
        this.ExamTestPaperList = response.rows;
        this.total = response.total;
        this.loading = false;
      });
    },
    /** 搜索按钮操作 */
    handleQuery() {
      this.queryParams.pageNum = 1;
      this.getList();
    },
    /** 重置按钮操作 */
    resetQuery() {
      this.resetForm("queryForm");
      this.handleQuery();
    },
    // 多选框选中数据
    handleSelectionChange(selection) {
      this.ids = selection.map(item => item.id)
      this.single = selection.length!==1
      this.multiple = !selection.length
    },
    /** 导出按钮操作 */
    handleExport() {
      this.download('train/ExamTestPaper/export', {
        ...this.queryParams
      }, `ExamTestPaper_${new Date().getTime()}.xlsx`)
    },
    init(){
      // 试卷计划列表
      listExamPlan().then(res => {
        this.options.examPlanList = res.rows
      })
      // 试卷模板列表
      listExamTestPaperModule().then(res => {
        this.options.testPaperModuleList = res.rows
      })
      // 人员列表
      listUser().then(res => {
        this.options.userList = res.rows
      })
      this.getList()

    },
  },
  // 通过路由守卫，请求数据接口
  beforeRouteEnter (to, from, next) {
    next(vm => {
      vm.getList(); 
    });
  },
  created() {
    this.init();
  },
};
