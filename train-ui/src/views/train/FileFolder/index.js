import { addFileFolder, delFileFolder, getFileFolder, listFileFolder, updateFileFolder } from "@/api/train/base/FileFolder";
import { listFile, uploadFile, delFile } from "@/api/train/base/FileInfo";
import mixins from "@/views/train/mixins";
export default {
  name: "FileFolder",
  mixins: [mixins],
  data() {
    return {
      // 遮罩层
      loading: true,
      // 选中数组
      ids: [],
      // 非单个禁用
      single: true,
      // 非多个禁用
      multiple: true,
      // 显示搜索条件
      showSearch: true,
      // 总条数
      total: 0,
      // 资源文件夹表格数据
      FileFolderList: [],
      // 弹出层标题
      title: "",
      // 是否显示弹出层
      open: false,
      // 文件列表弹出层标题
      fileTitle: "",
      // 文件列表弹出层是否展示
      fileOpen: false,
      fileInfoList: [],
      fileInfoTotal: 0,
      // 查询参数
      queryParams: {
        pageNum: 1,
        pageSize: 10,
        name: null,
      },
      fileInfoQueryParams: {
        pageNum: 1,
        pageSize: 10,
        fielName: null,
        fileType: null,
      },
      // 表单参数
      form: {},
      // 表单校验
      rules: {
        name: [
          { required: true, message: "名称不能为空", trigger: "blur" }
        ],
        createTime: [
          { required: true, message: "用户创建时间不能为空", trigger: "blur" }
        ],
      },
      fileIds: null,
    };
  },
  methods: {
    /** 查询资源文件夹列表 */
    getList() {
      this.loading = true;
      listFileFolder(this.doSort(this.queryParams,'createTime' )).then(response => {
        this.FileFolderList = response.rows;
        this.total = response.total;
        this.loading = false;
      });
    },
    // 取消按钮
    cancel() {
      this.open = false;
      this.reset();
    },
    // 表单重置
    reset() {
      this.form = {
        id: null,
        name: null,
        remark: null,
        createTime: null,
        createBy: null,
        updateTime: null,
        updateBy: null,
        isDel: null,
        delBy: null,
        delTime: null
      };
      this.resetForm("form");
    },
    /** 搜索按钮操作 */
    handleQuery() {
      this.queryParams.pageNum = 1;
      this.getList();
    },
    /** 重置按钮操作 */
    resetQuery() {
      this.resetForm("queryForm");
      this.handleQuery();
    },
    // 多选框选中数据
    handleSelectionChange(selection) {
      this.ids = selection.map(item => item.id)
      this.single = selection.length !== 1
      this.multiple = !selection.length
    },
    /** 新增按钮操作 */
    handleAdd() {
      this.reset();
      this.open = true;
      this.title = "添加资源文件夹";
    },
    /** 修改按钮操作 */
    handleUpdate(row) {
      this.reset();
      const id = row.id || this.ids
      getFileFolder(id).then(response => {
        this.form = response.data;
        this.open = true;
        this.title = "修改资源文件夹";
      });
    },
    /** 提交按钮 */
    submitForm() {
      this.$refs["form"].validate(valid => {
        if (valid) {
          if (this.form.id != null) {
            updateFileFolder(this.form).then(response => {
              this.$modal.msgSuccess("修改成功");
              this.open = false;
              this.getList();
            });
          } else {
            addFileFolder(this.form).then(response => {
              this.$modal.msgSuccess("新增成功");
              this.open = false;
              this.getList();
            });
          }
        }
      });
    },
    /** 删除按钮操作 */
    handleDelete(row) {
      const ids = row.id || this.ids;
      this.$modal.confirm('确认删除？').then(function () {
        return delFileFolder(ids);
      }).then(() => {
        this.getList();
        this.$modal.msgSuccess("删除成功");
      }).catch(() => { });
    },
    /** 导出按钮操作 */
    handleExport() {
      this.download('base/FileFolder/export', {
        ...this.queryParams
      }, `FileFolder_${new Date().getTime()}.xlsx`)
    },
    init() {
      this.getList()
    },
    // 展示文件信息
    showFileInfo(row) {
      this.fileTitle = "文件信息列表";
      this.fileOpen = true;
      this.fileInfoQueryParams = {
        relId: row.id,
        isDel: 0,
      };
      this.getFileInfoList();
    },
    getFileInfoList() {
      listFile(this.fileInfoQueryParams).then(response => {
        this.fileInfoList = response.rows;
        this.fileInfoTotal = response.total;
        this.loading = false;
      });
    },
    fileInfoHandleQuery() {
      this.fileInfoQueryParams.pageNum = 1;
      this.getFileInfoList();
    },
    fileInfoResetQuery() {
      this.fileInfoQueryParams.fileName = null;
      this.fileInfoQueryParams.fileType = null;
      this.fileInfoHandleQuery();
    },
    //上传文件
    uploadFile(obj) {
      uploadFile(obj.file, this.fileInfoQueryParams.relId).then(response => {
        this.$modal.msgSuccess("上传成功");
        this.getFileInfoList();
      }).catch(() => {this.$modal.msgError("上传失败"); });;
    },
    handleSelectionChangeFile(selection) {
      this.fileIds = selection.map(item => item.id)
      this.single = selection.length !== 1
      this.multiple = !selection.length
    },
    // 删除文件
    handleFileDelete(row) {
      const ids = row.id || this.fileIds;
      this.$modal.confirm('确认删除？').then(function () {
        return delFile(ids);
      }).then(() => {
        this.getFileInfoList();
        this.$modal.msgSuccess("删除成功");
      }).catch(() => { });
    },
    /** 文件预览 */
    filePreview(filePath) {
      this.fileOpen = false;
      this.$router.push(`/train/filePreview/${encodeURIComponent(filePath)}`)
    }
  },
  created() {
    this.init();
    console.log(this)
  },
};
