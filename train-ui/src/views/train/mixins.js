/**
 * 该文件用于一些通用方法
 */
import "@riophae/vue-treeselect/dist/vue-treeselect.css";
import {listPost} from '@/api/system/post.js'
import {listDept} from '@/api/system/dept'
import {listExamSubjectKnowledgePoints} from "@/api/train/base/ExamSubjectKnowledgePoints";
import {KNOWLEDGE_POINTS_TYPE} from "@/views/train/Enums";

export default {
  data() {
    return {}
  },
  dicts: ['post_level', 'syllabus', 'exam_subject_knowledge'],
  methods: {
    /**
     * 获取分页时排序处理
     * @param {Object} queryParams - 查询参数
     * @param {String}  [=createTime] paramsToSort - 需要排序的字段,默认创建时间
     * @param {Boolean} [=false] isAsc - 是否是正序,默认逆序
     * @return {Object} 处理结果
     */
    doSort(queryParams, paramsToSort='createTime', isAsc = false){
      return {
        ...queryParams,
        orderByColumn: paramsToSort,
        isAsc: isAsc ? 'asc' : 'desc'
      }
    },
    // 获取部门
    getDepts() {
      return new Promise(resolve => {
        listDept().then(res => {
          let map = {}
          let list = []
          list = res.data
          res.data.forEach(e => {
            map[e.deptId] = e.deptName
          })
          resolve({
            map,
            list
          })
        })
      })
    },
    // 获取岗位
    getPosts() {
      return new Promise(resolve => {
        listPost().then(res => {
          let map = {}
          let list = []
          res.rows.forEach(post => {
            map[post.postId] = post.postName
          })
          list = res.rows
          resolve({map, list})
        })
      })
    },
    getPostSubjectPointTree() {
      // 获取科目+知识点,获取岗位
      return new Promise(resolve => {
        Promise.all([listExamSubjectKnowledgePoints(), listPost()]).then(([pointRes, postRes]) => {
          let points = pointRes.rows || [], posts = postRes.rows || []
          // 首先添加统一的类型,用于区分其类别
          posts.forEach(post => {
            post.queryType = 1
          })
          points.forEach(point => {
            if (point.type === KNOWLEDGE_POINTS_TYPE.enums.SUBJECT.value) {
              point.queryType = 2
            } else if (point.type === KNOWLEDGE_POINTS_TYPE.enums.KNOWLEDGE_POINTS.value) {
              point.queryType = 3
            }
            point.parentId = point.subjectId
          })

          // 构建成树
          let treeData = this.handleTree(points)
          posts.forEach(post => {
            post.children = []
            post.name = post.postName
            post.id = post.postId
            treeData.forEach(tree => {
              if (tree.postId === post.postId) {
                post.children.push(tree)
              }
            })
          })
          resolve(posts)
        })
      })
    }
  }
}
