import { listPostImproveApplication, getPostImproveApplication, delPostImproveApplication, addPostImproveApplication, updatePostImproveApplication }
  from "@/api/train/base/PostImproveApplication.js";
import {getPostDetail} from "@/api/system/user";
import { Post_Apply_Type, Post_Apply_State } from '@/views/train/Enums';
import { mapGetters } from "vuex";
import mixins from "@/views/train/mixins";
export default {
  name: "PostImproveApplication",
  dicts: ['post_level', 'post_ability'],
  mixins: [mixins],
  computed: {
    // ...mapGetters(['postList'])
  },
  data() {
    return {
      // 遮罩层
      loading: true,
      // 选中数组
      ids: [],
      // 非单个禁用
      single: true,
      // 非多个禁用
      multiple: true,
      // 显示搜索条件
      showSearch: true,
      // 总条数
      total: 0,
      // 岗位提升申请表格数据
      PostImproveApplicationList: [],
      // 弹出层标题
      title: "",
      // 是否显示弹出层
      open: false,
      // 查询参数
      queryParams: {
        pageNum: 1,
        pageSize: 10,
        oldCode: null,
        toCode: null,
        type: null,
        state: null,
        reviewer: null,
        testPaperId: null,
        content: null,
        result: null,
        isDel: null,
        delBy: null,
        delTime: null,
      },
      // 表单参数
      form: {},
      // 表单校验
      rules: {
        type: [
          { required: true, message: "类型不能为空", trigger: "change" }
        ],
      },
      Post_Apply_Type: Post_Apply_Type,
      Post_Apply_State:Post_Apply_State,
      postList: null,
    };
  },
  methods: {
    /** 查询岗位提升申请列表 */
    getList() {
      this.loading = true;
      listPostImproveApplication(this.doSort(this.queryParams,'createTime' )).then(response => {
        this.PostImproveApplicationList = response.rows;
        this.total = response.total;
        this.loading = false;
      });
    },
    // 取消按钮
    cancel() {
      this.open = false;
      this.reset();
    },
    // 表单重置
    reset() {
      this.form = {
        id: null,
        oldCode: null,
        toCode: null,
        type: null,
        state: null,
        createBy: null,
        createTime: null,
        remark: null,
        isDel: null,
        delBy: null,
        delTime: null,
        updateTime: null
      };
      this.resetForm("form");
    },
    /** 搜索按钮操作 */
    handleQuery() {
      this.queryParams.pageNum = 1;
      this.getList();
    },
    /** 重置按钮操作 */
    resetQuery() {
      this.resetForm("queryForm");
      this.handleQuery();
    },
    // 多选框选中数据
    handleSelectionChange(selection) {
      this.ids = selection.map(item => item.id)
      this.single = selection.length !== 1
      this.multiple = !selection.length
    },
    /** 新增按钮操作 */
    handleAdd() {
      this.getPostDetail();
      this.reset();
      this.open = true;
      this.title = "添加岗位提升申请";
    },
    /** 修改按钮操作 */
    handleUpdate(row) {
      this.reset();
      const id = row.id || this.ids
      getPostImproveApplication(id).then(response => {
        this.form = response.data;
        this.form.type = parseInt(this.form.type)
        this.form.state = parseInt(this.form.state)
        this.open = true;
        this.title = "修改岗位提升申请";
      });
    },
    /** 提交按钮 */
    submitForm() {
      console.log(this.form)
      this.$refs["form"].validate(valid => {
        if (valid) {
          if (this.form.id != null) {
            updatePostImproveApplication(this.form).then(response => {
              this.$modal.msgSuccess("修改成功");
              this.open = false;
              this.getList();
            });
          } else {
            addPostImproveApplication(this.form).then(response => {
              this.$modal.msgSuccess("新增成功");
              this.open = false;
              this.getList();
            });
          }
        }
      });
    },
    /** 删除按钮操作 */
    handleDelete(row) {
      const ids = row.id || this.ids;
      this.$modal.confirm('确认删除？').then(function () {
        return delPostImproveApplication(ids);
      }).then(() => {
        this.getList();
        this.$modal.msgSuccess("删除成功");
      }).catch(() => { });
    },
    /** 导出按钮操作 */
    handleExport() {
      this.download('train/PostImproveApplication/export', {
        ...this.queryParams
      }, `PostImproveApplication_${new Date().getTime()}.xlsx`)
    },
    init() {
      this.getList();
    },
    getPostDetail() {
      getPostDetail().then(response => {
        this.postList = response.data;
      })
    },
  },
  created() {
    this.init();
  },
};
