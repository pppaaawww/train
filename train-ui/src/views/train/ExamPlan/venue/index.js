import { listExamPlanVenue, getExamPlanVenue, delExamPlanVenue, addExamPlanVenue, updateExamPlanVenue } from "@/api/train/base/ExamPlanVenue.js";
import {getByCondition} from "@/api/system/user.js"
import mixins from "@/views/train/mixins";
export default {
  name: "ExamPlanVenue",
  mixins: [mixins],
  data() {
    return {
      // 遮罩层
      loading: true,
      // 选中数组
      ids: [],
      // 子表选中数据
      checkedExamPlanVenueUser: [],
      // 非单个禁用
      single: true,
      // 非多个禁用
      multiple: true,
      // 显示搜索条件
      showSearch: true,
      // 总条数
      total: 0,
      // 考试计划场次表格数据
      ExamPlanVenueList: [],
      // 考试场次人员表格数据
      examPlanVenueUserList: [],
      // 弹出层标题
      title: "",
      // 是否显示弹出层
      open: false,
      show:false,
      titleExam: "",
      // 查询参数
      queryParams: {
        pageNum: 1,
        pageSize: 10,
        examPlanId: null,
        examStartTime: null,
        examEndTime: null,
        examPlace: null,
        isDel: null,
        delBy: null,
        delTime: null,
      },
      // 表单参数
      form: {},
      // 表单校验
      rules: {
        examStartTime: [
          { required: true, message: "考试计划开始时间不能为空", trigger: "blur" }
        ],
        examEndTime: [
          { required: true, message: "考试计划结束时间不能为空", trigger: "blur" }
        ],
        examPlace: [
          { required: true, message: "考试地点不能为空", trigger: "blur" }
        ],
      },
      userForm: {},
      userList: [],
      userIds: [],
    };
  },
  methods: {
    /** 查询考试计划场次列表 */
    getList() {
      this.loading = true;
      listExamPlanVenue(this.doSort(this.queryParams,'createTime' )).then(response => {
        this.ExamPlanVenueList = response.rows;
        this.total = response.total;
        this.loading = false;
      });
    },
    // 取消按钮
    cancel() {
      this.open = false;
      this.reset();
    },
    // 表单重置
    reset() {
      this.form = {
        id: null,
        examPlanId: null,
        examStartTime: null,
        examEndTime: null,
        examPlace: null,
        createTime: null,
        isDel: null,
        delBy: null,
        delTime: null,
        updateBy: null,
        updateTime: null,
        remark: null
      };
      this.examPlanVenueUserList = [];
      this.resetForm("form");
    },
    /** 搜索按钮操作 */
    handleQuery() {
      this.queryParams.pageNum = 1;
      this.getList();
    },
    /** 重置按钮操作 */
    resetQuery() {
      this.resetForm("queryForm");
      this.handleQuery();
    },
    // 多选框选中数据
    handleSelectionChange(selection) {
      this.ids = selection.map(item => item.id)
      this.single = selection.length!==1
      this.multiple = !selection.length
    },
    /** 新增按钮操作 */
    handleAdd() {
      this.reset();
      this.open = true;
      this.title = "添加考试计划场次";
    },
    showExamplanVenue(row) {
      this.handleAdd();
      this.form.examPlanId = row.id;
      this.userForm.postId = row.postId;
      this.userForm.deptId = row.deptId;
      this.userForm.postLevelCode = row.postLevelCode;
      getByCondition(this.userForm).then(response => {
        this.userList = response.data;
      });
    },
    /** 修改按钮操作 */
    handleUpdate(row) {
      this.reset();
      this.userForm.postId = row.postId;
      this.userForm.deptId = row.deptId;
      this.userForm.postLevelCode = row.postLevelCode;
      getByCondition(this.userForm).then(response => {
        this.userList = response.data;
      });
      const id = row.id
      getExamPlanVenue(id).then(response => {
        this.form = response.data;
        this.examPlanVenueUserList = response.data.examPlanVenueUserList;
        this.open = true;
        this.title = "修改考试计划场次";
      });

    },
    /** 提交按钮 */
    submitForm() {
      this.$refs["form"].validate(valid => {
        if (valid) {
          this.form.examPlanVenueUserList = this.examPlanVenueUserList;
          if (this.form.id != null) {
            updateExamPlanVenue(this.form).then(response => {
              this.$modal.msgSuccess("修改成功");
              this.open = false;
              this.$emit("finishParent");
            });
          } else {
            addExamPlanVenue(this.form).then(response => {
              this.$modal.msgSuccess("新增成功");
              this.open = false;
              this.$emit("finishParent");
            });
          }
        }
      });
    },
    /** 删除按钮操作 */
    handleDelete(row) {
      const ids = row.id || this.ids;
      this.$modal.confirm('确认删除？').then(function() {
        return delExamPlanVenue(ids);
      }).then(() => {
        this.getList();
        this.$modal.msgSuccess("删除成功");
      }).catch(() => {});
    },
	/** 考试场次人员序号 */
    rowExamPlanVenueUserIndex({ row, rowIndex }) {
      row.index = rowIndex + 1;
    },
    /** 考试场次人员添加按钮操作 */
    handleAddExamPlanVenueUser() {
      let obj = {};
      obj.examPlanId = "";
      obj.userId = "";
      obj.remark = "";
      obj.isDel = 0;
      this.examPlanVenueUserList.push(obj);
    },
    /** 考试场次人员删除按钮操作 */
    handleDeleteExamPlanVenueUser() {
      if (this.checkedExamPlanVenueUser.length == 0) {
        this.$modal.msgError("请先选择要删除的考试场次人员数据");
      } else {
        const examPlanVenueUserList = this.examPlanVenueUserList;
        const checkedExamPlanVenueUser = this.checkedExamPlanVenueUser;
        this.examPlanVenueUserList = examPlanVenueUserList.filter(function(item) {
          return checkedExamPlanVenueUser.indexOf(item.index) == -1
        });
      }
    },
    /** 复选框选中数据 */
    handleExamPlanVenueUserSelectionChange(selection) {
      this.checkedExamPlanVenueUser = selection.map(item => item.index)
    },
    /** 导出按钮操作 */
    handleExport() {
      this.download('train/ExamPlanVenue/export', {
        ...this.queryParams
      }, `ExamPlanVenue_${new Date().getTime()}.xlsx`)
    },
    init(){
      this.getList()
    },
  },
  created() {
    this.init();
  },
};
