import { listExamPlan, updateExamPlanStatus, getExamPlan, delExamPlan, addExamPlan, updateExamPlan } from "@/api/train/base/ExamPlan.js";
import { listExamTestPaperModule } from "@/api/train/base/ExamTestPaperModule.js";
import { delExamPlanVenue } from "@/api/train/base/ExamPlanVenue.js";
import {PLAN_TYPE,YES_NO, PLAN_STATUS} from '@/views/train/Enums';
import {listDept} from '@/api/system/dept';
import {listPost} from '@/api/system/post.js'
import exam_plan_venue from '@/views/train/ExamPlan/venue/index.vue';
import mixins from "@/views/train/mixins";
export default {
  components: { exam_plan_venue },
  name: "ExamPlan",
  dicts: ['syllabus', 'post_level'],
  mixins: [mixins],
  data() {
    return {
      // 遮罩层
      loading: true,
      // 选中数组
      ids: [],
      // 子表选中数据
      checkedExamPlanVenue: [],
      // 非单个禁用
      single: true,
      // 非多个禁用
      multiple: true,
      // 显示搜索条件
      showSearch: true,
      // 总条数
      total: 0,
      // 考试计划表格数据
      ExamPlanList: [],
      // 考试计划场次表格数据
      examPlanVenueList: [],
      // 弹出层标题
      title: "",
      // 是否显示弹出层
      open: false,
      // 查询参数
      queryParams: {
        pageNum: 1,
        pageSize: 10,
        name: null,
        examTestPaperModuleId: null,
        deptId: null,
        postId: null,
        postLevelCode: null,
        examType: null,
        frequency: null,
        examStartTime: null,
        examEndTime: null,
        whetherCanResit: null,
        examDuration: null,
        statu: null,
      },
      // 表单参数
      form: {},
      // 表单校验
      rules: {
        name: [
          { required: true, message: "考试名称不能为空", trigger: "blur" }
        ],
        examTestPaperModuleId: [
          { required: true, message: "试卷模板不能为空", trigger: "change" }
        ],
        postLevelCode: [
          { required: true, message: "级别不能为空", trigger: "change" }
        ],
        examType: [
          { required: true, message: "类型不能为空", trigger: "change" }
        ],
        frequency: [
          { required: true, message: "频次不能为空", trigger: "blur" }
        ],
        examStartTime: [
          { required: true, message: "开始时间不能为空", trigger: "blur" }
        ],
        examEndTime: [
          { required: true, message: "结束时间不能为空", trigger: "blur" }
        ],
        whetherCanResit: [
          { required: true, message: "可以补考不能为空", trigger: "change" }
        ],
        examDuration: [
          { required: true, message: "考试时长不能为空", trigger: "blur" }
        ],
        statu: [
          { required: true, message: "状态不能为空", trigger: "blur" }
        ],
      },
      // 模板集合
      examTestPaperModules:[],
      PLAN_TYPE:PLAN_TYPE,
      YES_NO:YES_NO,
      PLAN_STATUS:PLAN_STATUS,
      planType:0,
      options: {
        deptMap: {},
        postMap: {},
      },
    };

  },
  methods: {
    /**
     * 获取部门
     */
    getDept() {
      listDept().then(res => {
        res.data.forEach(e => {
          this.options.deptMap[e.deptId] = e.deptName
        })
      })
    },
    getPost() {
      listPost().then(res => {
        res.rows.forEach(e => {
          this.options.postMap[e.postId] = e.postName
        })
      })
    },
    /** 查询考试计划列表 */
    getList() {
      this.loading = true;
      listExamPlan(this.doSort(this.queryParams,'createTime' )).then(response => {
        this.ExamPlanList = response.rows;
        this.total = response.total;
        this.loading = false;
      });
    },
    /** 修改计划类型时调用方法 */
    examTypeChange(){
      this.planType = this.form.examType;
    },
    deptChange(){
      this.form.examTestPaperModuleId = null;
      this.examTestPaperModules = [];
    },
    postChange(){
      this.form.examTestPaperModuleId = null;
      this.examTestPaperModules = [];
    },
    postLevelChange(){
      this.form.examTestPaperModuleId = null;
      this.examTestPaperModules = [];
    },
    /** 下拉框出现/隐藏时触发 */
    moduleVisibleChange(isVisible){
      if(isVisible){
        listExamTestPaperModule({deptId:this.form.deptId,postId:this.form.postId,postLevelCode:this.form.postLevelCode,isDel:0}).then(response => {
          this.examTestPaperModules = response.rows;
        })
      }
    },
    // 取消按钮
    cancel() {
      this.open = false;
      this.reset();
    },
    // 表单重置
    reset() {
      this.form = {
        id: null,
        name: null,
        examTestPaperModuleId: null,
        deptId: null,
        postId: null,
        postLevelCode: null,
        examType: '0',
        frequency: null,
        examStartTime: null,
        examEndTime: null,
        whetherCanResit: null,
        examDuration: null,
        statu: null,
        remark: null
      };
      this.examPlanVenueList = [];
      this.resetForm("form");
    },
    /** 搜索按钮操作 */
    handleQuery() {
      this.queryParams.pageNum = 1;
      this.getList();
    },
    /** 重置按钮操作 */
    resetQuery() {
      this.resetForm("queryForm");
      this.handleQuery();
    },
    // 多选框选中数据
    handleSelectionChange(selection) {
      this.ids = selection.map(item => item.id)
      this.single = selection.length!==1
      this.multiple = !selection.length
    },
    /** 新增按钮操作 */
    handleAdd() {
      this.reset();
      this.open = true;
      this.title = "添加考试计划";
    },
    /** 修改按钮操作 */
    handleUpdate(row) {
      this.reset();
      const id = row.id || this.ids
      getExamPlan(id).then(response => {
        this.form = response.data;
        this.examPlanVenueList = response.data.examPlanVenueList;
        this.open = true;
        this.title = "修改考试计划";
      });
    },
    /** 提交按钮 */
    submitForm() {
      this.$refs["form"].validate(valid => {
        if (valid) {
          this.form.examPlanVenueList = this.examPlanVenueList;
          if (this.form.id != null) {
            updateExamPlan(this.form).then(response => {
              this.$modal.msgSuccess("修改成功");
              this.open = false;
              this.getList();
            });
          } else {
            addExamPlan(this.form).then(response => {
              this.$modal.msgSuccess("新增成功");
              this.open = false;
              this.getList();
            });
          }
        }
      });
    },
    /** 删除按钮操作 */
    handleDelete(row) {
      const ids = row.id || this.ids;
      this.$modal.confirm('确认删除？').then(function() {
        return delExamPlan(ids);
      }).then(() => {
        this.getList();
        this.$modal.msgSuccess("删除成功");
      }).catch(() => {});
    },
    /** 考试计划场次序号 */
    rowExamPlanVenueIndex({ row, rowIndex }) {
      row.index = rowIndex + 1;
    },
    /** 考试计划场次添加按钮操作 */
    handleAddExamPlanVenue() {
      let obj = {};
      obj.examStartTime = "";
      obj.examEndTime = "";
      obj.examPlace = "";
      obj.remark = "";
      this.examPlanVenueList.push(obj);
    },
    /** 考试计划场次删除按钮操作 */
    handleDeleteExamPlanVenue() {
      if (this.checkedExamPlanVenue.length == 0) {
        this.$modal.msgError("请先选择要删除的考试计划场次数据");
      } else {
        const examPlanVenueList = this.examPlanVenueList;
        const checkedExamPlanVenue = this.checkedExamPlanVenue;
        this.examPlanVenueList = examPlanVenueList.filter(function(item) {
          return checkedExamPlanVenue.indexOf(item.index) == -1
        });
      }
    },
    /** 复选框选中数据 */
    handleExamPlanVenueSelectionChange(selection) {
      this.checkedExamPlanVenue = selection.map(item => item.index)
    },
    /** 导出按钮操作 */
    handleExport() {
      this.download('train/ExamPlan/export', {
        ...this.queryParams
      }, `ExamPlan_${new Date().getTime()}.xlsx`)
    },
    init(){
      this.getDept();
      this.getPost();
      this.getList();
    },
    showExamplanVenue(row) {
      this.$refs["dialog"].showExamplanVenue(row);
    },
    /** 更新考试计划的状态 */
    updateStatu() {
      console.log(this.ids)
      updateExamPlanStatus(this.ids, 1).then(res => {
        this.$modal.msgSuccess("修改成功");
      })
    },
    /** 修改按钮操作 */
    handleVenueUpdate(row) {
      this.$refs["dialog"].handleUpdate(row);
    },
    /** 删除按钮操作 */
    handleVenueDelete(row) {
      const ids = row.id;
      this.$modal.confirm('确认删除？').then(function() {
        return delExamPlanVenue(ids);
      }).then(() => {
        this.getList();
        this.$modal.msgSuccess("删除成功");
      }).catch(() => {});
    },
  },
  created() {
    this.init();
  },
};
