import {EnumsBuilder} from "@/utils/Enums";
// 知识点类型
export const KNOWLEDGE_POINTS_TYPE = EnumsBuilder({
  SUBJECT: {label: '科目', value: '0'},
  KNOWLEDGE_POINTS: {label: '知识点', value: '1'},
})

// 考题类型
export const QUESTION_TYPE = EnumsBuilder({
  JudgeQuestion: {label: "判断题", value: '1'},
  FillBlankQuestionExcel: {label: "填空题", value: '2'},
  EssayQuestion: {label: "论述题", value: '4'},
  ChoiceQuestion: {label: "选择题", value: '5'},
})
// 试卷类型
export const TEST_PAPER_TYPE = EnumsBuilder({
  RANDOM_PAPER: {label: "随机试卷", value: '0'},
  MANDATORY_PAPER: {label: "标准试卷", value: '1'},
})

// 判断题的答案
export const JUDGE_QUESTION_RIGHT_KEY = EnumsBuilder({
  RIGHT: {label: "正确", value: '正确'},
  WRONG: {label: "错误", value: '错误'},
})

// 试卷的使用类型
export const USER_TYPE = EnumsBuilder({
  TRAIN: {value: '0', label: "模拟"},
  EXAM: {value: '1', label: "考试"}
})

// 计划类型
export const PLAN_TYPE = EnumsBuilder({
  FIXED: {value: '0', label: "固定计划"},
  CYCLE: {value: '1', label: "周期计划"}
})

// 计划状态  0-待启用；1-启用中；2-计划完成
export const PLAN_STATUS = EnumsBuilder({
  WAIT: {value: '0', label: "待启用"},
  ENABLE: {value: '1', label: "启用中"},
  COMPLETE: {value: '2', label: "计划完成"}
})

// 是否
export const YES_NO = EnumsBuilder({
  NO: {value: '0', label: "否"},
  YES: {value: '1', label: "是"}
})
// 回答题目的结果,对应result字段
export const RESULT_TYPE = EnumsBuilder({
  ERROR: {value: '0', label: "错误"},
  RIGHT: {value: '2', label: "正确"}
})

// 试卷状态
export const EXAM_TEST_PAPER_STATE = EnumsBuilder({
  WAIT_RECEIVE: {value: 0, label: "待领取"},
  WAIT_SUBMIT: {value: 1, label: "待回填"},
  WAIT_GRADE: {value: 2, label: "待判卷"},
  FINISH: {value: 3, label: "已完成"}
})
// 选择题选项
export const Choice_Question_Option = EnumsBuilder({
  A: {value: '0', label: 'A'},
  B: {value: '1', label: 'B'},
  C: {value: '2', label: 'C'},
  D: {value: '3', label: 'D'},
  E: {value: '4', label: 'E'},
  F: {value: '5', label: 'F'},
})

// 岗位申请类型
export const Post_Apply_Type = EnumsBuilder({
  POST_LEVEL_CODE: {value: 0, label: '岗位级别'},
  POST_ABLITY: {value: 1, label: '岗位能力'},
})
// 岗位申请审核状态
export const Post_Apply_State = EnumsBuilder({
  WAIT: {value: 0, label: '待审核'},
  PASS: {value: 1, label: '通过'},
  REJECT: {value: 2, label: '驳回'},
})
// 成就申请审核状态
export const Achieve_Apply_State = EnumsBuilder({
  WAIT: {value: '0', label: '待审核'},
  PASS: {value: '1', label: '通过'},
  REJECT: {value: '2', label: '驳回'},
})
// 模拟测试状态
export const TRAIN_TEST_PAPER_STATE = EnumsBuilder({
  WAIT_SUBMIT: {value: 0, label: "待回填"},
  FINISH: {value: 1, label: "已完成"}
})
