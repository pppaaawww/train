import {EXAM_TEST_PAPER_STATE, TRAIN_TEST_PAPER_STATE, USER_TYPE} from '@/views/train/Enums'
import {listExamPlan} from "@/api/train/base/ExamPlan";
import {listExamTestPaperModule} from "@/api/train/base/ExamTestPaperModule";
import {listUser} from "@/api/system/user";
import {createTrainPaper, queryUserTrainPaper} from "@/api/train/base/Train";
import testPaperModule from './testPaperModule'
import mixins from "@/views/train/mixins";
export default {
  name: "SimulationTrain",
  dicts: ['post_level'],
  mixins: [mixins],
  components:{
    testPaperModule
  },
  props: {
    simulationTrainVisible: {
      type: Boolean
    }
  },
  data() {
    return {
      visible: false,
      // 遮罩层
      loading: true,
      // 显示搜索条件
      showSearch: true,
      // 总条数
      total: 0,
      // 试卷管理表格数据
      ExamTestPaperList: [],
      // 弹出层标题
      title: "",
      // 是否显示弹出层
      open: false,
      // 查询参数
      queryParams: {
        pageNum: 1,
        pageSize: 10,
        state: null,
        exemPlanId: null,
        name: null,
        testPaperModuleId: null,
        userId: null,
        questionNum: null,
        rightAnswerNum: null,
        notDoneNum: null,
        vagueNum: null,
        submitTime: null,
        deptId: null,
        postId: null,
        postLevelCode: null,
        grade: null,
        gainGrade: null,
      },
      options:{
        TRAIN_TEST_PAPER_STATE:TRAIN_TEST_PAPER_STATE,
        userList: [],
        testPaperModuleList: [],
        USER_TYPE: USER_TYPE
      }
    };
  },
  methods: {
    outTrain() {
      this.$emit('update:simulationTrainVisible', false)
    },
    handleView(row){
      this.$router.push(`/train1/simulationDetail/${row.id}`)
    },
    startTest(row){
      this.$router.push(`/train1/simulationDetail/${row.id}`)
    },
    /** 查询试卷管理列表 */
    getList() {
      this.loading = true;
      queryUserTrainPaper(this.doSort(this.queryParams,'createTime' )).then(response => {
        this.ExamTestPaperList = response.rows;
        this.total = response.total;
        this.loading = false;
      });
    },
    /** 搜索按钮操作 */
    handleQuery() {
      this.queryParams.pageNum = 1;
      this.getList();
    },
    /** 重置按钮操作 */
    resetQuery() {
      this.resetForm("queryForm");
      this.handleQuery();
    },
    /** 导出按钮操作 */
    handleExport() {
      this.download('train/ExamTestPaper/export', {
        ...this.queryParams
      }, `ExamTestPaper_${new Date().getTime()}.xlsx`)
    },
    init(){
      // 试卷模板列表
      listExamTestPaperModule( {userType: this.options.USER_TYPE.enums.TRAIN.value}).then(res => {
        this.options.testPaperModuleList = res.rows
      })
      // 人员列表
      listUser().then(res => {
        this.options.userList = res.rows
      })
      this.getList()

    },
    showModule(){
      this.visible = true
      this.$refs.testPaperModule && this.$refs.testPaperModule.resetQuery()
    }
  },
  created() {
    this.init();
  },
}
