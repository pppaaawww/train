import {getPostSubjectTree, listSpecialRecode} from "@/api/train/base/Train";
import question from "./question";
import {RESULT_TYPE, YES_NO} from "@/views/train/Enums";

import mixins from "@/views/train/mixins";

export default {
  name: "SpecialTrain",
  mixins: [mixins],
  components: {
    question
  },
  props: {
    specialTrainVisible: {
      type: Boolean
    }
  },
  data() {
    return {
      postId: null,
      subjectId: null,
      knowledgePointId: null,
      // 显示搜索条件
      showSearch: true,
      // 总条数
      total: 0,
      // 非单个禁用
      single: true,
      // 非多个禁用
      multiple: true,
      loading: false,
      specialRecordList: [],
      // 选中数组
      ids: [],
      // 是否查看试题
      questionVisible: false,
      // 是查看试题还是
      isTraining: '',
      postSubjectTree: [],
      // 查询参数
      queryParams: {
        pageNum: 1,
        pageSize: 10
      },
      options: {
        RESULT_TYPE: RESULT_TYPE,
        YES_NO: YES_NO,
      }
    }
  },

  methods: {
    getList() {
      this.loading = true;
      listSpecialRecode(this.doSort(this.queryParams, 'createTime')).then(response => {
        this.specialRecordList = response.rows;
        this.total = response.total;
        this.loading = false;
      });
    },
    /** 搜索按钮操作 */
    handleQuery() {
      this.queryParams.pageNum = 1;
      listSpecialRecode(this.doSort(this.queryParams, 'createTime')).then(response => {
        this.specialRecordList = response.rows;
        this.total = response.total;
        this.loading = false;
      });
    },
    /** 重置按钮操作 */
    resetQuery() {
      this.resetForm("queryForm");
      this.handleQuery();
    },
    // 多选框选中数据
    handleSelectionChange(selection) {
      this.ids = selection.map(item => item.id)
      this.single = selection.length !== 1
      this.multiple = !selection.length
    },
    // 开始训练
    handleTrain(postId, subjectId, knowledgePointId) {
      this.postId = postId;
      this.subjectId = subjectId;
      this.knowledgePointId = knowledgePointId;


      this.isTraining = true
      this.questionVisible = true
      this.$nextTick(_ => {
        this.$refs.question.questionFetch()
      })
    },
    // 查看模拟试题
    handleView(row) {
      this.isTraining = false
      this.questionVisible = true
      row.bank.resultContent = row.content
      row.bank.result = row.result
      row.bank.type = row.bank.questionType
      row.bank.resultContent = row.content
      this.$nextTick(_ => {
        this.$refs.question.questionView(row.bank)
      })

    },
    // 从试题页面返回
    handleBack() {
      this.questionVisible = false
      this.getList()
    },
    /**
     * 退出训练
     */
    outTrain() {
      this.$emit('update:specialTrainVisible', false)
    }
  },
  created() {
    this.getList();
    getPostSubjectTree().then(res => {
      this.postSubjectTree = res.data
    })
  },
}
