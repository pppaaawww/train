import specialTrain from "./special/specialTrain.vue"
import simulationTrain from "./simulation/simulationTrain.vue"
import mixins from "@/views/train/mixins";
export default {
    name: "Train",
    mixins: [mixins],
    components:{
      specialTrain,
      simulationTrain
    },
    data() {
      return {
        specialTrainVisible:false,
        simulationTrainVisible:false,
      }
    },

    methods: {
      // 点击进行专项训练
      chooseSpecialTrain(){
        this.specialTrainVisible = true;
      },
      // 点击进行模拟训练
      chooseSimulationTrain(){
        this.simulationTrainVisible = true;
      }
    },
    created() {
    },
}
