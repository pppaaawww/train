import { listExamSubjectKnowledgePoints, getExamSubjectKnowledgePoints, delExamSubjectKnowledgePoints, addExamSubjectKnowledgePoints, updateExamSubjectKnowledgePoints } from "@/api/train/base/ExamSubjectKnowledgePoints";
import { listPost } from "@/api/system/post.js";
import  KnowledgeDialog from "./KnowledgeDialog.vue"
import {KNOWLEDGE_POINTS_TYPE} from '@/views/train/Enums'
import mixins from "@/views/train/mixins";
import {getToken} from "@/utils/auth";
export default {
  components:{KnowledgeDialog},
  name: "ExamSubjectKnowledgePoints",
  mixins: [mixins],
  data() {
    return {
      // 遮罩层
      loading: true,
      // 选中数组
      ids: [],
      // 非单个禁用
      single: true,
      // 非多个禁用
      multiple: true,
      // 显示搜索条件
      showSearch: true,
      // 总条数
      total: 0,
      // 科目管理表格数据
      ExamSubjectKnowledgePointsList: [],
      // 弹出层标题
      title: "",
      // 是否显示弹出层
      open: false,
      // 查询参数
      queryParams: {
        pageNum: 1,
        pageSize: 10,
        name: null,
        type: "0",
      },
      // 表单参数
      form: {},
      // 表单校验
      rules: {
        name: [
          { required: true, message: "名称不能为空", trigger: "blur" }
        ],
        type: [
          { required: true, message: "类型不能为空", trigger: "change" }
        ],
        postId: [
          { required: true, message: "岗位不能为空", trigger: "change" }
        ],
      },
      // 岗位列表
      postList: [],
      KNOWLEDGE_POINTS_TYPE: KNOWLEDGE_POINTS_TYPE,
      // 上传
      upload: {
        title: '',
        open: false,
        isUploading: false,
        // 设置上传的请求头部
        headers: {Authorization: "Bearer " + getToken()},
        url: process.env.VUE_APP_BASE_API + "/base/ExamSubjectKnowledgePoints/importData"
      },
    };
  },
  created() {
    this.getList();
    this.getPostList();
  },
  methods: {
    typeFormat(type){
      return (KNOWLEDGE_POINTS_TYPE.list.find(e => e.value === type)|| {}).label
    },
    /** 查询岗位信息 */
    getPostList() {
      listPost({}).then(response => {
        this.postList = response.rows;
      });
    },
    /** 查询科目管理列表 */
    getList() {
      this.loading = true;
      listExamSubjectKnowledgePoints(this.doSort(this.queryParams,'createTime' )).then(response => {
        this.ExamSubjectKnowledgePointsList = response.rows;
        this.total = response.total;
        this.loading = false;
      });
    },
    // 取消按钮
    cancel() {
      this.open = false;
      this.reset();
    },
    // 表单重置
    reset() {
      this.form = {
        id: null,
        name: null,
        type: null,
        subjectId: null,
        postId: null,
        createBy: null,
        createTime: null,
        isDel: null,
        delBy: null,
        delTime: null,
        updateBy: null,
        updateTime: null,
        remark: null
      };
      this.resetForm("form");
    },
    /** 搜索按钮操作 */
    handleQuery() {
      this.queryParams.pageNum = 1;
      this.getList();
    },
    /** 重置按钮操作 */
    resetQuery() {
      this.resetForm("queryForm");
      this.handleQuery();
    },
    // 多选框选中数据
    handleSelectionChange(selection) {
      this.ids = selection.map(item => item.id)
      this.single = selection.length !== 1
      this.multiple = !selection.length
    },
    /** 新增按钮操作 */
    handleAdd() {
      this.reset();
      this.open = true;
      this.title = "添加科目";
      this.form.type = "0";
    },
    /** 修改按钮操作 */
    handleUpdate(row) {
      this.reset();
      const id = row.id || this.ids
      getExamSubjectKnowledgePoints(id).then(response => {
        this.form = response.data;
        this.open = true;
        this.title = "修改科目";
      });
    },
    /** 添加知识点 */
    addKnowledge(row){
      this.$refs["dialog"].showDialog(row);
    },
    /** 提交按钮 */
    submitForm() {
      this.$refs["form"].validate(valid => {
        if (valid) {
          if (this.form.id != null) {
            updateExamSubjectKnowledgePoints(this.form).then(response => {
              this.$modal.msgSuccess("修改成功");
              this.open = false;
              this.getList();
            });
          } else {
            addExamSubjectKnowledgePoints(this.form).then(response => {
              this.$modal.msgSuccess("新增成功");
              this.open = false;
              this.getList();
            });
          }
        }
      });
    },
    /** 删除按钮操作 */
    handleDelete(row) {
      const ids = row.id || this.ids;
      this.$modal.confirm('确认删除？').then(function () {
        return delExamSubjectKnowledgePoints(ids);
      }).then(() => {
        this.getList();
        this.$modal.msgSuccess("删除成功");
      }).catch(() => { });
    },
    /** 导出按钮操作 */
    handleExport() {
      this.download('base/ExamSubjectKnowledgePoints/export', {
        ...this.queryParams
      }, `ExamSubjectKnowledgePoints_${new Date().getTime()}.xlsx`)
    },
    // 导入
    handleImport() {
      this.upload.title = "考题导入";
      this.upload.open = true;
    },
    // 文件上传中处理
    handleFileUploadProgress(event, file, fileList) {
      this.upload.isUploading = true;
    },
    // 文件上传成功处理
    handleFileSuccess(response, file, fileList) {
      this.upload.open = false;
      this.upload.isUploading = false;
      this.$refs.upload.clearFiles();
      this.$alert("<div style='overflow: auto;overflow-x: hidden;max-height: 70vh;padding: 10px 20px 0;'>" + response.msg + "</div>", "导入结果", {dangerouslyUseHTMLString: true});
      this.getList();
    },
    handleFileError() {
      this.$message.error('导入失败')
    },
    // 提交上传文件
    submitFileForm() {
      this.$refs.upload.submit();
    },
  }
};
