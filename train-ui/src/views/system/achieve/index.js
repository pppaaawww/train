import {addSysAchieve, delSysAchieve, getSysAchieve, listSysAchieve, updateSysAchieve} from "@/api/system/SysAchieve";
import mixins from "@/views/train/mixins";

export default {
  name: "SysAchieve",
  mixins: [mixins],
  data() {
    const picValidator = (rule, value, callback) => {
      if (Array.isArray(value) && value.length) {
        callback();
      } else {
        callback(new Error("请上传图片!"));
      }
    };
    return {
      // 遮罩层
      loading: true,
      // 选中数组
      ids: [],
      // 非单个禁用
      single: true,
      // 非多个禁用
      multiple: true,
      // 显示搜索条件
      showSearch: true,
      // 总条数
      total: 0,
      // 等级管理表格数据
      SysAchieveList: [],
      // 弹出层标题
      title: "",
      // 是否显示弹出层
      open: false,
      // 查询参数
      queryParams: {
        pageNum: 1,
        pageSize: 10,
        name: null,
        code: null,
      },
      // 表单参数
      form: {},
      // 表单校验
      rules: {
        name: [
          {required: true, message: "创建时间不能为空", trigger: "blur"}
        ],
        code: [
          {required: true, message: "创建时间不能为空", trigger: "blur"}
        ],
        file: [
          {required: true, validator: picValidator, trigger: "blur"}
        ],
      }
    };
  },
  methods: {
    /** 查询等级管理列表 */
    getList() {
      this.loading = true;
      listSysAchieve(this.doSort(this.queryParams)).then(response => {
        this.SysAchieveList = response.rows;
        this.total = response.total;
        this.loading = false;
      });
    },
    // 取消按钮
    cancel() {
      this.open = false;
      this.reset();
    },
    // 表单重置
    reset() {
      this.form = {
        id: null,
        name: null,
        code: null,
        file: [],
        createTime: null,
        createBy: null,
        updateTime: null,
        updateBy: null,
        delBy: null,
        delTime: null,
        remark: null
      };
      // this.resetForm("form");
    },
    /** 搜索按钮操作 */
    handleQuery() {
      this.queryParams.pageNum = 1;
      this.getList();
    },
    /** 重置按钮操作 */
    resetQuery() {
      this.resetForm("queryForm");
      this.handleQuery();
    },
    // 多选框选中数据
    handleSelectionChange(selection) {
      this.ids = selection.map(item => item.id)
      this.single = selection.length !== 1
      this.multiple = !selection.length
    },
    // 从后端取到数据后进行加工
    dataParse(dataToParse) {
      try {
        let data = {
          id: dataToParse.id,
          name: dataToParse.name,
          code: dataToParse.code,
          file: dataToParse.fileInfo ? [dataToParse.fileInfo]:[]
        }
        return data
      } catch (err) {
        this.$message.warning('表单格式有误!')
        throw new Error(err)
      }
    },
    // 数据发送到后端前的加工
    dataFormat(dataToFormat) {
      try {
        let data = {
          id: dataToFormat.id,
          name: dataToFormat.name,
          code: dataToFormat.code,
          fileId: dataToFormat.file[0].id,
        }
        return data
      } catch (err) {
        this.$message.warning('表单格式有误!')
        throw new Error(err)
      }

    },
    /** 新增按钮操作 */
    handleAdd() {
      this.reset();
      this.open = true;
      this.title = "添加等级管理";
    },
    /** 修改按钮操作 */
    handleUpdate(row) {
      this.reset();
      const id = row.id || this.ids
      getSysAchieve(id).then(response => {
        this.form = this.dataParse(response.data);
        this.open = true;
        this.title = "修改等级管理";
      });
    },
    /** 提交按钮 */
    submitForm() {
      this.$refs["form"].validate(valid => {
        if (valid) {
          let data = this.dataFormat(this.form)
          if (this.form.id != null) {
            updateSysAchieve(data).then(response => {
              this.$modal.msgSuccess("修改成功");
              this.open = false;
              this.getList();
            });
          } else {
            addSysAchieve(data).then(response => {
              this.$modal.msgSuccess("新增成功");
              this.open = false;
              this.getList();
            });
          }
        }
      });
    },
    /** 删除按钮操作 */
    handleDelete(row) {
      const ids = row.id || this.ids;
      this.$modal.confirm('是否确认删除等级管理编号为"' + ids + '"的数据项？').then(function () {
        return delSysAchieve(ids);
      }).then(() => {
        this.getList();
        this.$modal.msgSuccess("删除成功");
      }).catch(() => {
      });
    },
    /** 导出按钮操作 */
    handleExport() {
      this.download('base/SysAchieve/export', {
        ...this.queryParams
      }, `SysAchieve_${new Date().getTime()}.xlsx`)
    },
    init() {
      this.getList()
    },
  },
  created() {
    this.init();
  },
};
