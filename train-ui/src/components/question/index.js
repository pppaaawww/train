import {
    getSpecialQuestion,
    answerSpecialQuestion
} from "@/api/train/base/Train";
import { QUESTION_TYPE,RESULT_TYPE } from '@/views/train/Enums'
export default {
    name: "Question",
    props: {
        subjectId: {
            type: String,
        },
        pointId: {
            type: String,
        },
        postId: {
            type: String,
        },
    },
    components: {

    },
    data() {
        return {
            QUESTION_TYPE: QUESTION_TYPE,
            RESULT_TYPE:RESULT_TYPE,
            question: {},
            rightKey: null,
        }
    },

    methods: {
        init() {
            getSpecialQuestion({ subjectId: this.subjectId, knowledgePointId: this.pointId, postId: this.postId }).then(e => {
                this.question = e.data
            });
        },
        submit(){
            var result = null;
            if(this.question.type === this.QUESTION_TYPE.enums.JudgeQuestion.value){
                if(this.question.rightKey === this.rightKey){
                    result = this.RESULT_TYPE.enums.RIGHT.value;
                }else{
                    result = this.RESULT_TYPE.enums.ERROR.value;
                }
            }
            if(this.question.type === this.QUESTION_TYPE.enums.FillBlankQuestionExcel.value){

            }
            if(this.question.type === this.QUESTION_TYPE.enums.ChoiceQuestion.value){
                if(this.question.rightKey === this.rightKey){
                    result = this.RESULT_TYPE.enums.RIGHT.value;
                }else{
                    result = this.RESULT_TYPE.enums.ERROR.value;
                }
            }
            answerSpecialQuestion({id:this.question.recordId,content:this.rightKey,result:result})
        },
    },
    created() {
    },
}