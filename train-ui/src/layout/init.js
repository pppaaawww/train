import store from "@/store";
import Vue from "vue";
import deptSelector from "@/components/deptSelector";
import postSelector from "@/components/postSelector";
import PostSubjectPointCascader from "@/components/PostSubjectPointCascader";
import select from '@/components/select'
export function init(){
  // 一些初始化后的操作
  store.dispatch('train/init')
  Vue.component('deptSelector',deptSelector)
  Vue.component('postSelector',postSelector)
  Vue.component('ElSelect', select)
  Vue.component('PostSubjectPointCascader',PostSubjectPointCascader)
  Vue.filter('postFilter',function (value){
    return store.state.train.postMap[value] || '-'
  })
  Vue.filter('deptFilter',function (value){
    return store.state.train.deptMap[value] || '-'
  })
}
