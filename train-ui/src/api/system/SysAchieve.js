import request from '@/utils/request'

// 查询等级管理列表
export function listSysAchieve(query) {
  return request({
    url: '/base/SysAchieve/list',
    method: 'get',
    params: query
  })
}

// 查询等级管理详细
export function getSysAchieve(id) {
  return request({
    url: '/base/SysAchieve/' + id,
    method: 'get'
  })
}

// 新增等级管理
export function addSysAchieve(data) {
  return request({
    url: '/base/SysAchieve',
    method: 'post',
    data: data
  })
}

// 修改等级管理
export function updateSysAchieve(data) {
  return request({
    url: '/base/SysAchieve',
    method: 'put',
    data: data
  })
}

// 删除等级管理
export function delSysAchieve(id) {
  return request({
    url: '/base/SysAchieve/' + id,
    method: 'delete'
  })
}
// 当前用户的成就
export function listAchieveByUserId() {
  return request({
    url: '/system/user/listAchieveByUserId',
    method: 'get',
  })
}
