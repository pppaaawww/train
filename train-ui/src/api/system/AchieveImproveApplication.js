import request from '@/utils/request'

// 查询等级申请列表
export function listAchieveImproveApplication(query) {
  return request({
    url: '/base/AchieveImproveApplication/list',
    method: 'get',
    params: query
  })
}

// 查询等级申请详细
export function getAchieveImproveApplication(id) {
  return request({
    url: '/base/AchieveImproveApplication/' + id,
    method: 'get'
  })
}

// 新增等级申请
export function addAchieveImproveApplication(data) {
  return request({
    url: '/base/AchieveImproveApplication',
    method: 'post',
    data: data
  })
}

// 修改等级申请
export function updateAchieveImproveApplication(data) {
  return request({
    url: '/base/AchieveImproveApplication',
    method: 'put',
    data: data
  })
}

// 删除等级申请
export function delAchieveImproveApplication(id) {
  return request({
    url: '/base/AchieveImproveApplication/' + id,
    method: 'delete'
  })
}
