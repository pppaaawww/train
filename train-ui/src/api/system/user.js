import request from '@/utils/request'
import { parseStrEmpty } from "@/utils/common";

// 查询用户列表
export function listUser(query) {
  return request({
    url: '/system/user/list',
    method: 'get',
    params: query
  })
}

// 查询用户详细
export function getUser(userId) {
  return request({
    url: '/system/user/' + parseStrEmpty(userId),
    method: 'get'
  })
}

// 新增用户
export function addUser(data) {
  return request({
    url: '/system/user',
    method: 'post',
    data: data
  })
}

// 修改用户
export function updateUser(data) {
  return request({
    url: '/system/user',
    method: 'put',
    data: data
  })
}

// 删除用户
export function delUser(userId) {
  return request({
    url: '/system/user/' + userId,
    method: 'delete'
  })
}

// 用户密码重置
export function resetUserPwd(userId, password) {
  const data = {
    userId,
    password
  }
  return request({
    url: '/system/user/resetPwd',
    method: 'put',
    data: data
  })
}

// 用户状态修改
export function changeUserStatus(userId, status) {
  const data = {
    userId,
    status
  }
  return request({
    url: '/system/user/changeStatus',
    method: 'put',
    data: data
  })
}

// 查询用户个人信息
export function getUserProfile() {
  return request({
    url: '/system/user/profile',
    method: 'get'
  })
}

// 修改用户个人信息
export function updateUserProfile(data) {
  return request({
    url: '/system/user/profile',
    method: 'put',
    data: data
  })
}

// 用户密码重置
export function updateUserPwd(oldPassword, newPassword) {
  const data = {
    oldPassword,
    newPassword
  }
  return request({
    url: '/system/user/profile/updatePwd',
    method: 'put',
    params: data
  })
}

// 用户头像上传
export function uploadAvatar(data) {
  return request({
    url: '/system/user/profile/avatar',
    method: 'post',
    data: data
  })
}

// 查询授权角色
export function getAuthRole(userId) {
  return request({
    url: '/system/user/authRole/' + userId,
    method: 'get'
  })
}

// 保存授权角色
export function updateAuthRole(data) {
  return request({
    url: '/system/user/authRole',
    method: 'put',
    params: data
  })
}

// 查询部门下拉树结构
export function deptTreeSelect() {
  return request({
    url: '/system/user/deptTree',
    method: 'get'
  })
}
// 根据部门id，岗位id，岗位等级查询用户信息
export function getByCondition(query) {
  return request({
    url:'/system/user/getByCondition',
    method:'get',
    params: query
  })
}

export function getPostDetail() {
  return request({
    url:'/system/user/getPostDetail',
    method:'get',
  })
}

export function listDaily(month) {
  return request({
    url:'/system/user/listDaily',
    method:'get',
    params:{month:month}
  })
}

/**
 * 获取岗位分析(三个饼状图)
 * @returns
 */
export function getPostAnalysis() {
  return request({
    url:'/system/user/getPostAnalysis',
    method:'get',
  })
}


/**
 * 根据岗位获取用户列表
 * @param {Object} params - 参数
 * @param {String} params.postId - 岗位ID
 * @return {AxiosPromise}
 */
export function listByPost(params) {
  return request({
    url:'/system/user/listByPost',
    method:'get',
    params: params
  })
}
