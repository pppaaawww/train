import request from '@/utils/request'

// 查询题库管理列表
export function listExamQuestionBank(query) {
  return request({
    url: '/base/ExamQuestionBank/list',
    method: 'get',
    params: query
  })
}

// 查询题库管理详细
export function getExamQuestionBank(id) {
  return request({
    url: '/base/ExamQuestionBank/' + id,
    method: 'get'
  })
}

// 新增题库管理
export function addExamQuestionBank(data) {
  return request({
    url: '/base/ExamQuestionBank',
    method: 'post',
    data: data
  })
}

// 修改题库管理
export function updateExamQuestionBank(data) {
  return request({
    url: '/base/ExamQuestionBank',
    method: 'put',
    data: data
  })
}

// 删除题库管理
export function delExamQuestionBank(id) {
  return request({
    url: '/base/ExamQuestionBank/' + id,
    method: 'delete'
  })
}
