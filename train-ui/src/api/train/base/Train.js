import request from '@/utils/request'

// 获取用户岗位-科目-知识点树
export function getPostSubjectTree(query) {
  return request({
    url: '/system/user/getPostSubjectTree',
    method: 'get',
    params: query
  })
}

/**
 * 根据用户信息随机获取训练试题
 * @param rule
 * @return
 */
export function getSpecialQuestion(query) {
  return request({
    url: '/train/getSpecialQuestion',
    method: 'get',
    params: query
  })
}

/**
 * 获取专项训练的题目
 * @param rule 规则
 * @return
 */
export function listSpecial(query) {
  return request({
    url: '/train/listSpecial',
    method: 'get',
    params: query
  })
}


/**
 * 提交试卷
 * @param answer
 * @return
 */
export function submitPaper(query) {
  return request({
    url: '/train/submitPaper',
    method: 'post',
    data: query
  })
}

/**
 * 记录训练记录(针对专项训练是调用)
 */
export function addRecord(data) {
  return request({
    url: '/train/addRecord',
    method: 'post',
    params: data
  })
}


/**
 * 回答专项训练题目
 */
export function answerSpecialQuestion(data) {
  return request({
    url: '/train/answerSpecialQuestion',
    method: 'put',
    data: data
  })
}


/**
 * 获取专项训练分页
 */
export function listSpecialRecode(query) {
  return request({
    url: '/train/listSpecialRecode',
    method: 'get',
    params: query
  })
}

/**
 * 获取某个岗位下以科目为维度的训练进度
 * @param query
 * @return {AxiosPromise}
 */
export function specialTrainProgress(query) {
  return request({
    url: '/train/specialTrainProgress',
    method: 'get',
    params: query
  })
}
/**
 * 获取个人模拟试卷列表
 * @param query
 * @return {AxiosPromise}
 */
export function queryUserTrainPaper(query) {
  return request({
    url: `/train/queryUserTrainPaper`,
    method: 'get',
    params: query
  })
}


/**
 * 获取某个模拟试卷详情
 * @param query
 * @return {AxiosPromise}
 */
export function getPaperDetail(id) {
  return request({
    url: `/train/getPaperDetail?id=${id}`,
    method: 'get',
  })
}
/**
 * 生成训练试卷
 * @param query
 * @return {AxiosPromise}
 */
export function createTrainPaper(id) {
  return request({
    url: '/train/createTrainPaper',
    method: 'post',
    data: id
  })
}





