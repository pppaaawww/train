import request from '@/utils/request'

// 查询科目管理列表
export function listExamSubjectKnowledgePoints(query) {
  return request({
    url: '/base/ExamSubjectKnowledgePoints/list',
    method: 'get',
    params: query
  })
}

// 查询科目管理详细
export function getExamSubjectKnowledgePoints(id) {
  return request({
    url: '/base/ExamSubjectKnowledgePoints/' + id,
    method: 'get'
  })
}

// 新增科目管理
export function addExamSubjectKnowledgePoints(data) {
  return request({
    url: '/base/ExamSubjectKnowledgePoints',
    method: 'post',
    data: data
  })
}

// 修改科目管理
export function updateExamSubjectKnowledgePoints(data) {
  return request({
    url: '/base/ExamSubjectKnowledgePoints',
    method: 'put',
    data: data
  })
}

// 删除科目管理
export function delExamSubjectKnowledgePoints(id) {
  return request({
    url: '/base/ExamSubjectKnowledgePoints/' + id,
    method: 'delete'
  })
}
