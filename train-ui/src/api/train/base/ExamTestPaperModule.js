import request from '@/utils/request'

// 查询试卷模板列表
export function listExamTestPaperModule(query) {
  return request({
    url: '/train/ExamTestPaperModule/list',
    method: 'get',
    params: query
  })
}

// 查询试卷模板详细
export function getExamTestPaperModule(id) {
  return request({
    url: '/train/ExamTestPaperModule/' + id,
    method: 'get'
  })
}

// 新增试卷模板
export function addExamTestPaperModule(data) {
  return request({
    url: '/train/ExamTestPaperModule',
    method: 'post',
    data: data
  })
}

// 修改试卷模板
export function updateExamTestPaperModule(data) {
  return request({
    url: '/train/ExamTestPaperModule',
    method: 'put',
    data: data
  })
}

// 删除试卷模板
export function delExamTestPaperModule(id) {
  return request({
    url: '/train/ExamTestPaperModule/' + id,
    method: 'delete'
  })
}
