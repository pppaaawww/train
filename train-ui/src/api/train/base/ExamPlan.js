import request from '@/utils/request'

// 查询考试计划列表
export function listExamPlan(query) {
  return request({
    url: '/train/ExamPlan/list',
    method: 'get',
    params: query
  })
}

// 查询考试计划详细
export function getExamPlan(id) {
  return request({
    url: '/train/ExamPlan/' + id,
    method: 'get'
  })
}

// 新增考试计划
export function addExamPlan(data) {
  return request({
    url: '/train/ExamPlan',
    method: 'post',
    data: data
  })
}

// 修改考试计划
export function updateExamPlan(data) {
  return request({
    url: '/train/ExamPlan',
    method: 'put',
    data: data
  })
}
// 修改考试计划 状态
export function updateExamPlanStatus(ids, status) {
  return request({
    url: '/train/ExamPlan/editStatus',
    method: 'put',
    data:{ids:ids,status:status}
  })
}

// 删除考试计划
export function delExamPlan(id) {
  return request({
    url: '/train/ExamPlan/' + id,
    method: 'delete'
  })
}
