import request from '@/utils/request'

// 查询试卷管理列表
export function listExamTestPaper(query) {
  return request({
    url: '/train/ExamTestPaper/list',
    method: 'get',
    params: query
  })
}

// 查询试卷管理详细
export function getExamTestPaper(id) {
  return request({
    url: '/train/ExamTestPaper/' + id,
    method: 'get'
  })
}

// 新增试卷管理
export function addExamTestPaper(data) {
  return request({
    url: '/train/ExamTestPaper',
    method: 'post',
    data: data
  })
}

// 修改试卷管理
export function updateExamTestPaper(data) {
  return request({
    url: '/train/ExamTestPaper',
    method: 'put',
    data: data
  })
}

// 删除试卷管理
export function delExamTestPaper(id) {
  return request({
    url: '/train/ExamTestPaper/' + id,
    method: 'delete'
  })
}
// 获取试题信息
export function getDetail(params) {
  return request({
    url: '/train/ExamTestPaper/getDetail',
    params:params,
    method: 'get'
  })
}

// 领取试卷
export function receive(data) {
  return request({
    url: '/train/ExamTestPaper/receive',
    method: 'put',
    data: data
  })
}
// 提交试卷
export function submitPaper(data) {
  return request({
    url: '/train/ExamTestPaper/submitPaper',
    method: 'post',
    data: data
  })
}
// 阅卷
export function gradingPaper(data){
  return request({
    url: '/train/ExamTestPaper/gradingPaper',
    method: 'post',
    data: data
  })
}

// 获取当前用户的成绩单，分页
export function listCurrentUser(params){
  return request({
    url: '/train/ExamTestPaper/listCurrentUser',
    method: 'get',
    params:params
  })
}

