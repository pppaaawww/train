import request from '@/utils/request'

// 查询考试计划场次列表
export function listExamPlanVenue(query) {
  return request({
    url: '/train/ExamPlanVenue/list',
    method: 'get',
    params: query
  })
}

// 查询考试计划场次详细
export function getExamPlanVenue(id) {
  return request({
    url: '/train/ExamPlanVenue/' + id,
    method: 'get'
  })
}

// 新增考试计划场次
export function addExamPlanVenue(data) {
  return request({
    url: '/train/ExamPlanVenue',
    method: 'post',
    data: data
  })
}

// 修改考试计划场次
export function updateExamPlanVenue(data) {
  return request({
    url: '/train/ExamPlanVenue',
    method: 'put',
    data: data
  })
}

// 删除考试计划场次
export function delExamPlanVenue(id) {
  return request({
    url: '/train/ExamPlanVenue/' + id,
    method: 'delete'
  })
}
