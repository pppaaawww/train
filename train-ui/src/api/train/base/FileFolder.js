import request from '@/utils/request'

// 查询资源文件夹列表
export function listFileFolder(query) {
  return request({
    url: '/FileFolder/list',
    method: 'get',
    params: query
  })
}

// 查询资源文件夹详细
export function getFileFolder(id) {
  return request({
    url: '/FileFolder/' + id,
    method: 'get'
  })
}

// 新增资源文件夹
export function addFileFolder(data) {
  return request({
    url: '/FileFolder',
    method: 'post',
    data: data
  })
}

// 修改资源文件夹
export function updateFileFolder(data) {
  return request({
    url: '/FileFolder',
    method: 'put',
    data: data
  })
}

// 删除资源文件夹
export function delFileFolder(id) {
  return request({
    url: '/FileFolder/' + id,
    method: 'delete'
  })
}
