import request from '@/utils/request'

// 查询岗位提升申请列表
export function listPostImproveApplication(query) {
  return request({
    url: '/train/PostImproveApplication/list',
    method: 'get',
    params: query
  })
}

// 查询岗位提升申请详细
export function getPostImproveApplication(id) {
  return request({
    url: '/train/PostImproveApplication/' + id,
    method: 'get'
  })
}

// 新增岗位提升申请
export function addPostImproveApplication(data) {
  return request({
    url: '/train/PostImproveApplication',
    method: 'post',
    data: data
  })
}

// 修改岗位提升申请
export function updatePostImproveApplication(data) {
  return request({
    url: '/train/PostImproveApplication',
    method: 'put',
    data: data
  })
}

// 删除岗位提升申请
export function delPostImproveApplication(id) {
  return request({
    url: '/train/PostImproveApplication/' + id,
    method: 'delete'
  })
}

// 删除岗位提升申请
export function postListCurrentUser(query) {
  return request({
    url: '/train/PostImproveApplication/listCurrentUser',
    method: 'get',
    params: query
  })
}

