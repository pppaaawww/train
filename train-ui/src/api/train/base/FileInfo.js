import request from '@/utils/request'

// 查询资源文件列表
export function listFile(query) {
  return request({
    url: '/file/list',
    method: 'get',
    params: query
  })
}

export function uploadFile(file, relId) {
  const formData = new FormData();
  formData.append('file', file);
  return request({
    url: '/file/upload?relId=' + relId,
    headers: {
      "Content-Type": "multipart/form-data",
    },
    method: 'post',
    data: formData
  })
}

// 查询资源文件详细
export function getFile(id) {
  return request({
    url: '/file/' + id,
    method: 'get'
  })
}

// 删除资源文件
export function delFile(id) {
  return request({
    url: '/file/' + id,
    method: 'delete'
  })
}
