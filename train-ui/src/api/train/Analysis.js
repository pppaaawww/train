import request from "@/utils/request";

/**
 * 获取某个月专项训练题目数量
 * @param month
 * @return {AxiosPromise}
 */
export function getTrainNumByMonth(params) {
  return request({
    url: '/train/getTrainNumByMonth',
    method: 'get',
    params: params,
  })
}

/**
 * 获取某个人的可以答题的科目的分析  雷达图
 * @param month
 * @return {AxiosPromise}
 */
export function getTrainSubjectAnalysis(params) {
  return request({
    url: '/train/getTrainSubjectAnalysis',
    method: 'get',
    params: params,

  })
}

/**
 * 一段时间训练题目数量及错题数量
 * @param params {Object} - 查询参数
 * @param params.userId {number}
 * @param params.startTime {Date}
 * @param params.endTime {Date}
 * @return {AxiosPromise}
 */
export function getTrainLineChart(params){
  return request({
    url: '/analysis/getTrainLineChart',
    method: 'get',
    params: params,
  })
}


/**
 * 人员训练数量（滚动排行）【数量、错题数量、错误率、当前岗位、当前岗位能力、当前岗位级别】
 * @param params {Object} - 查询参数
 * @param params.pageSize {number}
 * @param params.pageNum {number}
 * @return {AxiosPromise}
 */
export function userRanking(params){
  return request({
    url: '/train/userRanking',
    method: 'get',
    params: params,
  })
}

/**
 * 近三次考试排名（走马灯，前十名）
 * @return {AxiosPromise}
 */
export function examRanking(){
  return request({
    url: '/train/ExamTestPaper/examRanking',
    method: 'get',
  })
}

/**
 * 近十次考试通过率【折线图】
 * @return {AxiosPromise}
 */
export function passingRate(){
  return request({
    url: '/train/ExamTestPaper/passingRate',
    method: 'get',
  })
}

/**
 * 历史考试第一名
 * @return {AxiosPromise}
 */
export function historyExamFirst(){
  return request({
    url: '/train/ExamTestPaper/historyExamFirst',
    method: 'get',
  })
}

/**
 * 考试错题【知识点-科目】饼状图占比
 * @return {AxiosPromise}
 */
export function paperErrorProportion(){
  return request({
    url: '/train/ExamTestPaper/errorProportion',
    method: 'get',
  })
}


/**
 * 常用模板
 * @return {AxiosPromise}
 */
export function commonTemplates(){
  return request({
    url: '/train/commonTemplates',
    method: 'get',
  })
}

/**
 * 训练错误数量占比饼状图
 * @return {AxiosPromise}
 */
export function trainErrorProportion(){
  return request({
    url: '/train/errorProportion',
    method: 'get',
  })
}



/**
 * 模拟试卷列表
 * @param params {Object} - 查询参数
 * @param params.pageSize {number}
 * @param params.pageNum {number}
 * @return {AxiosPromise}
 */
export function trainPaperList(params){
  return request({
    url: '/train/paperList',
    method: 'get',
    params: params,
  })
}




/**
 * 统计分析-训练-旭日图
 * @param params {Object} - 查询参数
 * @param params.userId {number} - 用户ID
 * @param params.startDate {Date} - 开始时间
 * @param params.endDate {Date} - 结束时间
 * @return {AxiosPromise}
 */
export function getAnalysisData(params){
  return request({
    url: '/analysis/getAnalysisData',
    method: 'get',
    params: params,
  })
}
