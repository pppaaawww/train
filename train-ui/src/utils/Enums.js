export class Enums {
  constructor(params) {
    this.enums = params
    let map = {}, list = []
    Object.values(params).forEach(e => {
      map[e.value] = e.label
      list.push(e)
    })
    this.map = map
    this.list = list
  }
}
export function EnumsBuilder(params){
  return new Enums(params)
}
